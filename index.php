<?php
    /**
     * Remap Pos Main Rest API
     * @author Robet Atiq Maulana Rifiq
     * JUN 2019
     */

    require_once('./vendor/autoload.php');
    require_once('./vendor/autoload.php');

    use Pos\Pos;

    $pos = new Pos();
    $pos->run();
?>