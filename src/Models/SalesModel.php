<?php


namespace Pos\Models;

use Pos\Systems\Connection;
use Pos\Models\Adapters\Sales;

class SalesModel
{

    /**
     * @var Connection
     */
    private $db;

    public function __construct()
    {
        $this->db = new Connection();
    }

    /**
     * @param $params
     * @return int
     */
    public function insertSales($params, $secondaryId)
    {
        $query = "SELECT s.sales_id AS salesId FROM `pmr_t_sales` s
            WHERE s.secondary_id = " . $secondaryId;
        $this->db->query($query, $params);
        $result = $this->db->fetch();

        if (isset($result->salesId) == true) {
            return $result->salesId;
        } else {

            $query = "INSERT INTO `pmr_t_sales` (`user_id`, `secondary_id`,  `operator_id`, `shop_id`,  `customer_paid`, `remaining_payment`, `discount_percent`, `discount_potongan`, `total`, `total_after_discount`, `date`, `time`, `status`)
                VALUES (:userId, :secondaryId, :operatorId, :shopId , :customerPaid, :remainingPayment, :discountPercent, :discountPotongan, :total, :totalAfterDiscount,  :date, :time, :status)";

            $this->db->query($query, $params);

            return $this->db->lastInsertId();
        }
    }

    /**
     * Perbedaaan sama insert sales hanya di pengecekan tanggal dan shopId
     * @param $params
     * @return int
     */
    public function insertSalesFromBot($params, $date, $time, $shopId, $secondaryId)
    {
        $query = "SELECT s.sales_id AS salesId FROM `pmr_t_sales` s
            WHERE s.date = '" . $date . "' AND s.shop_id = " . $shopId . " AND s.time = '" . $time . "'" . " AND s.secondary_id = '" . $secondaryId . "'";
        $this->db->query($query, $params);
        $result = $this->db->fetch();

        if (isset($result->salesId) == true) {
            return $result->salesId;
        } else {

            $query = "INSERT INTO `pmr_t_sales` (`user_id`, `secondary_id`,  `operator_id`, `shop_id`,  `customer_paid`, `remaining_payment`, `discount_percent`, `discount_potongan`, `total`, `total_after_discount`, `date`, `time`, `status`)
                VALUES (:userId, :secondaryId, :operatorId, :shopId , :customerPaid, :remainingPayment, :discountPercent, :discountPotongan, :total, :totalAfterDiscount,  :date, :time, :status)";

            $this->db->query($query, $params);

            return $this->db->lastInsertId();
        }
    }

    /**
     * @param $params
     */
    public function insertSalesItem($params, $commodityId, $secondaryId, $stock, $shopId)
    {
        $query = "SELECT s.secondary_id AS secondaryId FROM `pmr_t_sales_detail` s
            WHERE s.secondary_id = " . $secondaryId;
        $this->db->query($query);
        $result = $this->db->fetch();

        if (isset($result->secondaryId) == false) {
            $query = "INSERT INTO `pmr_t_sales_detail`(`sales_id`, `secondary_id`, `secondary_trans_id`, `commodity_id`, `amount`, `price`, `old_price`, `discount_percent`, `discount_potongan`)
            VALUES (:salesId, :secondaryId, :secondaryTransId, :commodityId,  :amount, :price, :oldPrice, :discountPercent, :discountPotongan)";

            $this->db->query($query, $params);

            $query = "SELECT s.stock_product FROM `pmr_t_post_commodities_shop` s
            WHERE s.commodity_id = " . $commodityId . " AND s.shop_id = " . $shopId;
            $this->db->query($query);
            $result = $this->db->fetch();

            if (isset($result->stock_product) == true) {
                $finalStockProduct = $result->stock_product - $stock;
                $this->db->query("UPDATE `pmr_t_post_commodities_shop` 
                        SET `stock_product` = " . $finalStockProduct . "
                         WHERE commodity_id = " . $commodityId . " AND shop_id = " . $shopId);
            }
        }
    }

    /**
     * @param $params
     */
    public function insertSalesItemBot($params, $commodityId, $salesId, $stock, $shopId, $secondaryId)
    {
        $query = "SELECT s.secondary_id AS secondaryId FROM `pmr_t_sales_detail` s
            WHERE s.sales_id = " . $salesId . " AND s.commodity_id = " . $commodityId . " AND s.secondary_id = " . $secondaryId;
        $this->db->query($query);
        $result = $this->db->fetch();

        if (isset($result->secondaryId) == false) {
            $query = "INSERT INTO `pmr_t_sales_detail`(`sales_id`, `secondary_id`, `secondary_trans_id`, `commodity_id`, `amount`, `price`, `old_price`, `discount_percent`, `discount_potongan`)
            VALUES (:salesId, :secondaryId, :secondaryTransId, :commodityId,  :amount, :price, :oldPrice, :discountPercent, :discountPotongan)";

            $this->db->query($query, $params);

            $query = "SELECT s.stock_product FROM `pmr_t_post_commodities_shop` s
            WHERE s.commodity_id = " . $commodityId . " AND s.shop_id = " . $shopId;
            $this->db->query($query);
            $result = $this->db->fetch();

            if (isset($result->stock_product) == true) {
                $finalStockProduct = $result->stock_product - $stock;
                $this->db->query("UPDATE `pmr_t_post_commodities_shop` 
                        SET `stock_product` = " . $finalStockProduct . "
                         WHERE commodity_id = " . $commodityId . " AND shop_id = " . $shopId);
            }
        }
    }

    /**
     * @param $params
     * @return object
     */
    public function getSales($params)
    {

        $query = "SELECT 
            `sales_id` AS `salesId`,
            `secondary_id` AS `secondaryId`,
            `user_id` AS `userId`,
            `operator_id` AS `operatorId`,
            `shop_id` AS `shopId`,
            `date`, `time`, `status`,
            `customer_paid` AS `customerPaid`,
            `remaining_payment` AS `remaining_payment`,
            `discount_percent` AS `discountPercent`,
            `discount_potongan` AS `discountPotongan`,
            `total` AS `total`,
            `total_after_discount` AS `totalAfterDiscount`
            FROM `pmr_t_sales`
            WHERE `sales_id` = :salesId";
        $this->db->query($query, $params);

        return $this->db->fetch();
    }

    /**
     * @param $params
     * @return object
     */
    public function getSalesItem($params)
    {

        $query = "SELECT 
            s.sales_id AS `salesId`,
            s.commodity_id AS `commodityId`,
            s.secondary_id AS `secondaryId`,
            s.secondary_trans_id AS `secondaryTransId`,
            c.name,
            c.images,
            s.amount, s.price,
            s.old_price AS `oldPrice`,
            s.discount_potongan AS discountPotongan,
            s.discount_percent AS discountPercent
            FROM `pmr_t_sales_detail` s, `pmr_t_post_commodities` c
            WHERE s.sales_id = :salesId AND s.commodity_id = c.commodity_id";
        $this->db->query($query, $params);

        return $this->db->fetchAll(new Sales());
    }

    /**
     * @param $params
     */
    public function removeSalesItem($params)
    {

        $this->db->query("DELETE FROM `pmr_t_sales_detail` WHERE `sales_id` = :salesId", $params);
    }


    /**
     * @param $params
     */
    public function removeSalesItemByCommodityId($params)
    {
        $this->db->query("DELETE FROM `pmr_t_sales_detail` WHERE `sales_id` = :salesId AND `commodity_id` = :commodityId", $params);
    }

    /**
     * @param $params
     */
    public function removeSalesData($params)
    {

        $query = "
            DELETE FROM `pmr_t_sales` WHERE `sales_id` = :salesId;
            DELETE FROM `pmr_t_sales_detail` WHERE `sales_id` = :salesId;";

        $this->db->query($query, $params);
    }

    /**
     * @param $params
     * @return object
     */
    public function getUserSales($params)
    {

        $query = "SELECT 
            `sales_id` AS `salesId`,
            `user_id` AS `userId`,
            `secondary_id` AS `secondaryId`,
            `operator_id` AS `operatorId`,
            `shop_id` AS `shopId`,
            `date`, `time`, `status`,
             `customer_paid` AS `customerPaid`,
             `remaining_payment` AS `remaining_payment`,
             `discount_percent` AS `discountPercent`,
            `discount_potongan` AS `discountPotongan`,
            `total` AS `total`,
            `total_after_discount` AS `totalAfterDiscount`
            FROM `pmr_t_sales`
            WHERE `user_id` = :userId";

        $this->db->query($query, $params);

        return $this->db->fetchAll();
    }

    /**
     * @param $params
     * @return object
     */
    public function getShopSales($params)
    {

        $query = "SELECT 
            `sales_id` AS `salesId`,
            `user_id` AS `userId`,
            `secondary_id` AS `secondaryId`,
            `operator_id` AS `operatorId`,
            `shop_id` AS `shopId`,
            `date`, `time`, `status`,
             `customer_paid` AS `customerPaid`,
             `remaining_payment` AS `remaining_payment`,
             `discount_percent` AS `discountPercent`,
            `discount_potongan` AS `discountPotongan`,
            `total` AS `total`,
            `total_after_discount` AS `totalAfterDiscount`
            FROM `pmr_t_sales`
            WHERE `shop_id` = :shopId
            ORDER BY `sales_id` DESC";

        $this->db->query($query, $params);

        return $this->db->fetchAll();
    }

    /**
     * @param $params
     * @return object
     */
    public function getShopSalesLimit($params, $offset, $limit)
    {

        $query = "SELECT 
            `sales_id` AS `salesId`,
            `user_id` AS `userId`,
            `secondary_id` AS `secondaryId`,
            `operator_id` AS `operatorId`,
            `shop_id` AS `shopId`,
            `date`, `time`, `status`,
             `customer_paid` AS `customerPaid`,
             `remaining_payment` AS `remaining_payment`,
             `discount_percent` AS `discountPercent`,
            `discount_potongan` AS `discountPotongan`,
            `total` AS `total`,
            `total_after_discount` AS `totalAfterDiscount`
            FROM `pmr_t_sales`
            WHERE `shop_id` = :shopId
            LIMIT " . $offset . "," . $limit;
        $this->db->query($query, $params);

        return $this->db->fetchAll();
    }


    public function updateStatus($params)
    {

        $query = "UPDATE `pmr_t_sales` SET `status` = :status WHERE `sales_id` = :salesId";
        $this->db->query($query, $params);
    }

    public function getMaxCountByShop($params)
    {
        $query = "SELECT COUNT(*) AS max FROM `pmr_t_sales` WHERE `shop_id` = :shopId";
        $this->db->query($query, $params);
        return $this->db->fetch();
    }

    public function getShopHasReport($date)
    {
        $query = "SELECT DISTINCT shop_id AS shopId FROM `pmr_t_sales` WHERE `date` = '" . $date . "'";
        $this->db->query($query);
        $result = $this->db->fetchAll();
        return $result;
    }

    public function getShopYear($tgl, $id = null)
    {
        $query = "SELECT 
            COUNT(*) as `total_transaksi`,
            SUM(total_after_discount) as `total`
            FROM `pmr_t_sales`
            WHERE `date` = '" . $tgl . "'" . ($id !== null ? "AND pmr_t_sales.shop_id = $id" : null);

        $this->db->query($query);

        return $this->db->fetch();
    }

    public function getTotalSales($tgl, $id = null)
    {
        $query = "SELECT 
            SUM(pmr_t_sales_detail.amount) as `total_barang_terjual`
            FROM `pmr_t_sales_detail`
            INNER JOIN `pmr_t_sales` ON pmr_t_sales_detail.sales_id = pmr_t_sales.sales_id
            WHERE pmr_t_sales.date = '" . $tgl . "'" . ($id !== null ? "AND pmr_t_sales.shop_id = $id" : null);

        $this->db->query($query);

        return $this->db->fetch();
    }

    public function getRekapSales($tgl)
    {
        $query = "SELECT
        pmr_t_post_commodities.name as `nama`,
        pmr_t_sales_detail.amount as `jumlah`
        FROM `pmr_t_sales_detail`
        INNER JOIN `pmr_t_post_commodities` ON pmr_t_sales_detail.commodity_id = pmr_t_post_commodities.commodity_id
        INNER JOIN `pmr_t_sales` ON pmr_t_sales_detail.sales_id = pmr_t_sales.sales_id
        WHERE pmr_t_sales.date = '" . $tgl . "'";

        $this->db->query($query);
        return $this->db->fetchAlla();
    }

    public function getRekapSalesMonth($bln, $tahun)
    {
        $query = "SELECT
        pmr_t_post_commodities.name as `nama`,
        pmr_t_sales_detail.amount as `jumlah`
        FROM `pmr_t_sales_detail`
        INNER JOIN `pmr_t_post_commodities` ON pmr_t_sales_detail.commodity_id = pmr_t_post_commodities.commodity_id
        INNER JOIN `pmr_t_sales` ON pmr_t_sales_detail.sales_id = pmr_t_sales.sales_id
        WHERE MONTH(pmr_t_sales.date) = " . $bln . " AND YEAR (pmr_t_sales.date) = " . $tahun;

        $this->db->query($query);
        return $this->db->fetchAlla();
    }

    public function getRekapSalesYear($thn)
    {
        $query = "SELECT
        pmr_t_post_commodities.name as `nama`,
        pmr_t_sales_detail.amount as `jumlah`
        FROM `pmr_t_sales_detail`
        INNER JOIN `pmr_t_post_commodities` ON pmr_t_sales_detail.commodity_id = pmr_t_post_commodities.commodity_id
        INNER JOIN `pmr_t_sales` ON pmr_t_sales_detail.sales_id = pmr_t_sales.sales_id
        WHERE YEAR(pmr_t_sales.date) = " . $thn;

        $this->db->query($query);
        return $this->db->fetchAlla();
    }

    public function getRekapTime($tgl)
    {
        $query = "SELECT
        `time`
        FROM `pmr_t_sales`
        WHERE `date` = '" . $tgl . "'";

        $this->db->query($query);
        return $this->db->fetchAlla();
    }

    public function getRekapTimeMonth($bln)
    {
        $query = "SELECT
        `time`
        FROM `pmr_t_sales`
        WHERE MONTH(date) = " . $bln;

        $this->db->query($query);
        return $this->db->fetchAlla();
    }

    public function getRekapTimeYear($thn)
    {
        $query = "SELECT
        `time`
        FROM `pmr_t_sales`
        WHERE YEAR(date) = " . $thn;

        $this->db->query($query);
        return $this->db->fetchAlla();
    }

    public function getTotal($bln, $id = null)
    {
        $query = "SELECT
        COUNT(*) as `total_transaksi`,
        SUM(total_after_discount) as `total`
        FROM pmr_t_sales
        WHERE MONTH(date)= " . $bln . " " . ($id !== null ? "AND shop_id = $id" : null);

        $this->db->query($query);
        return $this->db->fetch();
    }

    public function getTotalSalesYear($thn, $id = null)
    {
        $query = "SELECT
        COUNT(*) as `total_transaksi`,
        SUM(total_after_discount) as `total`
        FROM pmr_t_sales
        WHERE YEAR(date)= " . $thn . " " . ($id !== null ? "AND shop_id = $id" : null);

        $this->db->query($query);
        return $this->db->fetch();
    }

    public function getTotalMonth($bln, $id = null)
    {
        $query = "SELECT 
            SUM(pmr_t_sales_detail.amount) as `total_barang_terjual`
            FROM `pmr_t_sales_detail`
            INNER JOIN `pmr_t_sales` ON pmr_t_sales_detail.sales_id = pmr_t_sales.sales_id
            WHERE MONTH(date)= " . $bln . " " . ($id !== null ? "AND shop_id = $id" : null);

        $this->db->query($query);

        return $this->db->fetch();
    }

    public function getTotalYear($thn, $id = null)
    {
        $query = "SELECT 
            SUM(pmr_t_sales_detail.amount) as `total_barang_terjual`
            FROM `pmr_t_sales_detail`
            INNER JOIN `pmr_t_sales` ON pmr_t_sales_detail.sales_id = pmr_t_sales.sales_id
            WHERE YEAR(date)= " . $thn . " " . ($id !== null ? "AND shop_id = $id" : null);

        $this->db->query($query);

        return $this->db->fetch();
    }

    public function getShopHasReportm($bln)
    {
        $query = "SELECT DISTINCT shop_id AS shopId FROM `pmr_t_sales` WHERE MONTH(date)=" . $bln;
        $this->db->query($query);
        $result = $this->db->fetchAll();
        return $result;
    }

    public function getShopHasReporty($thn)
    {
        $query = "SELECT DISTINCT shop_id AS shopId FROM `pmr_t_sales` WHERE YEAR(date)=" . $thn;
        $this->db->query($query);
        $result = $this->db->fetchAll();
        return $result;
    }

    public function getShopWeek($mgulalu, $mgu, $id = null)
    {
        $query = "SELECT 
            COUNT(*) as `total_transaksi`,
            SUM(total_after_discount) as `total`
            FROM `pmr_t_sales`
            WHERE `date` >= '" . $mgulalu . "' AND `date` <= '" . $mgu . "'" . ($id !== null ? "AND pmr_t_sales.shop_id = $id" : null);

        $this->db->query($query);

        return $this->db->fetch();
    }

    public function getTotalSalesWeek($mgulalu, $mgu, $id = null)
    {
        $query = "SELECT 
            SUM(pmr_t_sales_detail.amount) as `total_barang_terjual`
            FROM `pmr_t_sales_detail`
            INNER JOIN `pmr_t_sales` ON pmr_t_sales_detail.sales_id = pmr_t_sales.sales_id
            WHERE pmr_t_sales.date >= '" . $mgulalu . "' AND pmr_t_sales.date <= '" . $mgu . "'" . ($id !== null ? "AND pmr_t_sales.shop_id = $id" : null);

        $this->db->query($query);

        return $this->db->fetch();
    }

    public function getTypeSales($nama)
    {
        $query = 'SELECT
        pmr_t_post_commodities.description as `description`,
        pmr_t_sales_detail.price as `price`
        FROM `pmr_t_post_commodities`
        INNER JOIN `pmr_t_sales_detail` ON pmr_t_sales_detail.commodity_id = pmr_t_post_commodities.commodity_id
        WHERE pmr_t_post_commodities.name = "' . $nama . '"';



        $this->db->query($query);

        return $this->db->fetch();
    }

    public function getRekapSalesWeek($mgulalu, $mgu)
    {
        $query = "SELECT
        pmr_t_post_commodities.name as `nama`,
        pmr_t_sales_detail.amount as `jumlah`
        FROM `pmr_t_sales_detail`
        INNER JOIN `pmr_t_post_commodities` ON pmr_t_sales_detail.commodity_id = pmr_t_post_commodities.commodity_id
        INNER JOIN `pmr_t_sales` ON pmr_t_sales_detail.sales_id = pmr_t_sales.sales_id
        WHERE pmr_t_sales.date >= '" . $mgulalu . "' AND pmr_t_sales.date <= '" . $mgu . "'";

        $this->db->query($query);
        return $this->db->fetchAlla();
    }

    public function getSalesMonthByID($bln, $thn, $id = null)
    {
        $query = "SELECT
        pmr_t_post_commodities.name as `nama`,
        pmr_t_sales_detail.amount as `jumlah`,
        pmr_t_sales_detail.price as `price`
        FROM `pmr_t_post_commodities`
        INNER JOIN `pmr_t_sales_detail` ON pmr_t_sales_detail.commodity_id = pmr_t_post_commodities.commodity_id
        INNER JOIN `pmr_t_sales` ON pmr_t_sales_detail.sales_id = pmr_t_sales.sales_id
        WHERE MONTH(pmr_t_sales.date) = " . $bln . " AND YEAR(pmr_t_sales.date) = " . $thn . ($id !== null ? "AND pmr_t_sales.shop_id = $id" : null);


        $this->db->query($query);
        return $this->db->fetchAlla();
    }

    public function getReportByMonth($bln, $thn, $id = null)
    {
        $query = "SELECT
        COUNT(*) as `total_transaksi`,
        SUM(total_after_discount) as `total`
        FROM pmr_t_sales
        WHERE MONTH(pmr_t_sales.date) = " . $bln . " AND YEAR(pmr_t_sales.date) = " . $thn . ($id !== null ? "AND pmr_t_sales.shop_id = $id" : null);

        $this->db->query($query);
        return $this->db->fetch();

    }

    public function getTotalReportByMonth($bln, $thn, $id = null)
    {
        $query = "SELECT 
            SUM(pmr_t_sales_detail.amount) as `total_barang_terjual`
            FROM `pmr_t_sales_detail`
            INNER JOIN `pmr_t_sales` ON pmr_t_sales_detail.sales_id = pmr_t_sales.sales_id
            WHERE MONTH(pmr_t_sales.date) = " . $bln . " AND YEAR(pmr_t_sales.date) = " . $thn . ($id !== null ? "AND pmr_t_sales.shop_id = $id" : null);

        $this->db->query($query);

        return $this->db->fetch();
    }

    public function getRekapTimeWeek($mgulalu, $mgu)
    {
        $query = "SELECT
        `time`
        FROM `pmr_t_sales`
        WHERE pmr_t_sales.date >= '" . $mgulalu . "' AND pmr_t_sales.date <= '" . $mgu . "'";

        $this->db->query($query);
        return $this->db->fetchAlla();
    }

    public function getShopHasReportw($mgulalu, $mguskrng)
    {
        $query = "SELECT DISTINCT shop_id AS shopId FROM `pmr_t_sales` WHERE `date` >= '" . $mgulalu . "' AND `date` <= '" . $mguskrng . "'";
        $this->db->query($query);
        $result = $this->db->fetchAll();
        return $result;
    }

    public function getSalesDayByID($tgl, $id = null)
    {
        $query = "SELECT
        pmr_t_post_commodities.name as `nama`,
        pmr_t_post_commodities.description as `jenis`,
        pmr_t_sales_detail.amount as `jumlah`,
        pmr_t_sales_detail.price as `price`,
        pmr_t_sales.shop_id as `shop_id`,
        pmr_t_sales_detail.commodity_id as `id`
        FROM `pmr_t_post_commodities`
        INNER JOIN `pmr_t_sales_detail` ON pmr_t_sales_detail.commodity_id = pmr_t_post_commodities.commodity_id
        INNER JOIN `pmr_t_sales` ON pmr_t_sales_detail.sales_id = pmr_t_sales.sales_id
        WHERE pmr_t_sales.date = '" . $tgl . "'" . ($id !== null ? " AND pmr_t_sales.shop_id = $id" : null);

        $this->db->query($query);
        return $this->db->fetchAlla();
    }

    public function getReportByDay($tgl, $id = null)
    {
        $query = "SELECT
        COUNT(*) as `total_transaksi`,
        SUM(total_after_discount) as `total`
        FROM pmr_t_sales
        WHERE pmr_t_sales.date = '" . $tgl . "'" . ($id !== null ? " AND pmr_t_sales.shop_id = $id" : null);

        $this->db->query($query);
        return $this->db->fetch();
    }

    public function getTotalReportByDay($tgl, $id = null)
    {
        $query = "SELECT 
            SUM(pmr_t_sales_detail.amount) as `total_barang_terjual`
            FROM `pmr_t_sales_detail`
            INNER JOIN `pmr_t_sales` ON pmr_t_sales_detail.sales_id = pmr_t_sales.sales_id
            WHERE pmr_t_sales.date = '" . $tgl . "'" . ($id !== null ? " AND pmr_t_sales.shop_id = $id" : null);

        $this->db->query($query);

        return $this->db->fetch();
    }

    public function getSalesWeekByID($mgulalu, $mgu, $id = null)
    {
        $query = "SELECT
        pmr_t_post_commodities.name as `nama`,
        pmr_t_sales_detail.amount as `jumlah`,
        pmr_t_sales_detail.price as `price`
        FROM `pmr_t_post_commodities`
        INNER JOIN `pmr_t_sales_detail` ON pmr_t_sales_detail.commodity_id = pmr_t_post_commodities.commodity_id
        INNER JOIN `pmr_t_sales` ON pmr_t_sales_detail.sales_id = pmr_t_sales.sales_id
        WHERE pmr_t_sales.date >= '" . $mgulalu . "' AND pmr_t_sales.date <= '" . $mgu . "'" . ($id !== null ? "AND pmr_t_sales.shop_id = $id" : null);

        $this->db->query($query);
        return $this->db->fetchAlla();
    }

    public function getReportByWeek($mgulalu, $mgu, $id = null)
    {
        $query = "SELECT
        COUNT(*) as `total_transaksi`,
        SUM(total_after_discount) as `total`
        FROM pmr_t_sales
        WHERE pmr_t_sales.date >= '" . $mgulalu . "' AND pmr_t_sales.date <= '" . $mgu . "'" . ($id !== null ? "AND pmr_t_sales.shop_id = $id" : null);

        $this->db->query($query);
        return $this->db->fetch();
    }

    public function getTotalReportByWeek($mgulalu, $mgu, $id = null)
    {
        $query = "SELECT 
            SUM(pmr_t_sales_detail.amount) as `total_barang_terjual`
            FROM `pmr_t_sales_detail`
            INNER JOIN `pmr_t_sales` ON pmr_t_sales_detail.sales_id = pmr_t_sales.sales_id
            WHERE pmr_t_sales.date >= '" . $mgulalu . "' AND pmr_t_sales.date <= '" . $mgu . "'" . ($id !== null ? "AND pmr_t_sales.shop_id = $id" : null);


        $this->db->query($query);
        return $this->db->fetch();
    }

    public function getTotalByCabangd($nama_barang,$tgl,$id){
        $query = 'SELECT
        SUM(pmr_t_sales_detail.amount) as `total`
        FROM `pmr_t_sales_detail`
        INNER JOIN `pmr_t_sales` ON pmr_t_sales_detail.sales_id = pmr_t_sales.sales_id
        INNER JOIN `pmr_t_post_commodities` ON pmr_t_post_commodities.commodity_id = pmr_t_sales_detail.commodity_id
        WHERE pmr_t_post_commodities.name = "'.$nama_barang.'" AND pmr_t_sales.shop_id = '.$id.' AND pmr_t_sales.date = "'.$tgl.'"';


        $this->db->query($query);

        return $this->db->fetch();
    }

    public function getTotalByCabangm($nama_barang,$bln,$id){
        $query = 'SELECT
        SUM(pmr_t_sales_detail.amount) as `total`
        FROM `pmr_t_sales_detail`
        INNER JOIN `pmr_t_sales` ON pmr_t_sales_detail.sales_id = pmr_t_sales.sales_id
        INNER JOIN `pmr_t_post_commodities` ON pmr_t_post_commodities.commodity_id = pmr_t_sales_detail.commodity_id
        WHERE pmr_t_post_commodities.name = "'.$nama_barang.'" AND pmr_t_sales.shop_id = '.$id.' AND MONTH(pmr_t_sales.date) = '.$bln;


        $this->db->query($query);

        return $this->db->fetch();
    }

    public function getTotalByCabangw($nama_barang,$mgulalu,$mguskrng,$id){
        $query = 'SELECT
        SUM(pmr_t_sales_detail.amount) as `total`
        FROM `pmr_t_sales_detail`
        INNER JOIN `pmr_t_sales` ON pmr_t_sales_detail.sales_id = pmr_t_sales.sales_id
        INNER JOIN `pmr_t_post_commodities` ON pmr_t_post_commodities.commodity_id = pmr_t_sales_detail.commodity_id
        WHERE pmr_t_post_commodities.name = "'.$nama_barang.'" AND pmr_t_sales.shop_id = '.$id.' AND pmr_t_sales.date >= "'.$mgulalu.'" AND pmr_t_sales.date <= "'.$mguskrng.'"';


        $this->db->query($query);

        return $this->db->fetch();
    }

    public function getBuy(){
        $query = 'SELECT
        pmr_t_post_commodities.name,
        pmr_t_commodity_profit.commodity_id,
        pmr_t_commodity_profit.price
        FROM pmr_t_commodity_profit
        INNER JOIN pmr_t_post_commodities ON pmr_t_commodity_profit.commodity_id = pmr_t_post_commodities.commodity_id';
//        var_dump($query);die;

        $this->db->query($query);

        return $this->db->fetchAlla();
    }


    public function insertDataProfitFromTable(){

        $dataHaveInsert = [];

        $query = '
            SELECT DISTINCT 
                pmr_t_sales_detail.commodity_id, 
                pmr_t_sales.shop_id, 
                pmr_t_sales_detail.old_price
            FROM pmr_t_sales_detail 
            INNER JOIN pmr_t_sales 
            ON pmr_t_sales_detail.sales_id = pmr_t_sales.sales_id
        ';
        $this->db->query($query);
        $dataAll = $this->db->fetchAlla();
        for ($i = sizeof($dataAll)-1;$i >= 0;$i--){
            $isFound = false;
           foreach ($dataHaveInsert as $dataInsert) {
//               var_dump($dataInsert["commodity_id"]."&".$dataAll[$i]["commodity_id"]." - ".$dataInsert["shop_id"]."&".$dataAll[$i]["shop_id"]);
               if (isset($dataInsert["commodity_id"])
                    && $dataInsert["commodity_id"] == $dataAll[$i]["commodity_id"]) {
                   $isFound = true;
               }
           }
           if ($isFound == false) {
               $dataHaveInsert[] = $dataAll[$i];
           }

        }
//        die;
        foreach ($dataHaveInsert as $dataInsert) {
            $dataInsert["old_price"] = $dataInsert["old_price"] - 2000;
            $query = '
            INSERT INTO pmr_t_commodity_profit (commodity_id, price) 
            VALUES ('.$dataInsert["commodity_id"].','.$dataInsert["old_price"].')
        ';
            $this->db->query($query);
        }
        die;
    }

}
