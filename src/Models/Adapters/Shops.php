<?php
/**
 * Shop Adapters
 * @author Robet Atiq Maulana Rifqi
 * JUNI 2019
 */

namespace Pos\Models\Adapters;

class Shops
{

    // Invoking data
    public function __invoke($shop)
    {

        // Set Default Shops Model
        $model = [
            "shopId" => 0,
            "userId" => 0,
            "name" => "",
            "branch" => "",
            "address" => "",
            "email" => "",
            "fax" => "",
            "city" => "",
            "place" => []
        ];

        // Merging Data
        $this->data = ( object )array_merge(( array )$model, ( array )$shop);

        // Set Place
        $this->_setPlace();

        // Return
        return $this->data;
    }

    // Set Place
    private function _setPlace()
    {

        $this->data->place = [
            "placeId" => $this->data->placeId,
            "placeName" => $this->data->placeName,
            "latitude" => $this->data->latitude,
            "longitude" => $this->data->longitude,
            "city" => $this->data->city,
            "province" => $this->data->province,
            "state" => $this->data->state
        ];

        // Unset PlaceId
        unset($this->data->placeId);
        unset($this->data->placeName);
        unset($this->data->latitude);
        unset($this->data->longitude);
        unset($this->data->city);
        unset($this->data->province);
        unset($this->data->state);
    }
}

?>