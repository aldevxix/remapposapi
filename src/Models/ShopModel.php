<?php
/**
 * Shop Models
 * @author Robet Atiq Maulana Rifqi
 * JUNI 2019
 */

namespace Pos\Models;

use Pos\Models\Adapters\Shops;
use Pos\Systems\Connection;

class ShopModel
{

    /**
     *
     */
    public function __construct()
    {

        $this->db = new Connection();
    }

    /**
     * Action Shop
     *
     * @param object
     * @return object
     */
    public function actionShop($params)
    {

        $this->db->query("CALL `ActionShop`(:shopId, :userId, :name, :address, :placeId, :action)", $params);
        return $this->db->fetchAll(new Shops());
    }

    /**
     * Insert Location
     *
     * @param object
     * @return object
     */
    public function insertLocation($params)
    {

        $this->db->query("SELECT `InsertLocation`(:name, :longitude, :latitude, :city, :province, :state) as placeId", $params);

        return $this->db->fetch();
    }

    /**
     * Search Shop By Place
     *
     * @param object
     * @return object
     */
    public function getByPlace($params)
    {

        $this->db->query("CALL `GetShopByPlace`(:range, :keyword)", $params);

        return $this->db->fetchAll(new Shops());
    }

    /**
     * Search Shop
     *
     * @param object
     * @return object
     */
    public function search($params)
    {

        $this->db->query("SELECT * FROM `pmr_v_shops` WHERE `name` LIKE CONCAT('%', :keyword, '%')", $params);

        return $this->db->fetchAll(new Shops());
    }

    /**
     *  Shop User List
     *
     * @param object
     * @return object
     */
    public function getShopsByUser()
    {
        $this->db->query("SELECT shop_id AS shopId, user_id AS userId, name, branch, address, place_id AS placeId, city, telp, fax, email FROM `pmr_t_shops`");
        $result = $this->db->fetchAll(new Shops());
        return $result;
    }

    public function getShopIdByName($nameShop){
        $this->db->query("SELECT shop_id AS shopId FROM `pmr_t_shops` WHERE name LIKE '%".$nameShop."%'");
        $result = $this->db->fetch();
        return $result;
    }
}

?>