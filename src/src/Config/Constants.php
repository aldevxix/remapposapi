<?php
    /**
     * Constants Value
     * @author Robet Atiq Maulana Rifqi
     * JUN 2019
     */

    namespace Pos\Config;

    interface Constants {

        /**
         * App Description
         */
        const APP = "Remap Pos API v1.0";
        const VERSION = "1";
        const DESC = "Authentication is needed to access api.";

        /**
         * Path Configuration
         */
        const CONTROLLER_PATH = "/Controllers/";
        const STORAGE_PATH = "../../storage/";
        const STORAGE_URI = "storage/";

        /**
         * Status code
         */
        const SUCCESS = 200;
        const FORBIDEN = 403;
        const NOT_FOUND = 404;
        const ERROR = 500;

        /**
         * Encryption configuration
         */
        const ENC_KEY = "pamer";
        const DEF_MAX_PIN = 6;

        /**
         * Maps Configuration
         */
        const MAPS_URL = "https://maps.googleapis.com/maps/api/geocode/json";
        const MAPS_SENSOR = true;
        const MAPS_KEY = "AIzaSyAFUe4bnf6Yr7S97t3YhLR5FVNN3oBR7nA";

        /**
         * Action Configuration
         */
        const ACTION_CREATE = 0;
        const ACTION_READ = 1;
        const ACTION_UPDATE = 2;
        const ACTION_DELETE = 3;

        /**
         * Status Configuration
         */
        const CART_STATUS = 0;
        const BUY_STATUS = 1;


        /**
         * CABANG VAUZA TAMMA
         */
        const CABANG_KAUMAN = "kauman";
        const CABANG_SULFAT = "sulfat";
        const CABANG_SAWOJAJAR = "sawojajar";
        const CABANG_SIGURA_GURA = "sigura";
        const CABANG_GONDANG_LEGI = "gondang";
        const CABANG_LAWANG = "lawang";
        const CABANG_PROBOLINGGO = "probolinggo";
        const CABANG_PAKIS = "pakis";
        const CABANG_PASURUAN = "pasuruan";
        const CABANG_JEMBER = "jember";

        /**
         * Bot Telegram
         */
        //UMUM
        const TOKEN_TELEGRAM = "1003163922:AAETrfhCzcylB_IkW13bw5crfmSdrN3hozc";
        const USERNAME_BOT_TELEGRAM= "@vatcashbot";

        //Jember
        const TOKEN_TELEGRAM_JEMBER = "997696263:AAG-7iOHx2GDAJ7_NKecabgI-iZ_nqBHsec";
        const USERNAME_BOT_TELEGRAM_JEMBER = "@vatcashjemberbot";

        //Sawojajar
        const TOKEN_TELEGRAM_SAWOJAJAR = "812238719:AAFamzIlM3WBI4rt4c_EtKA5S8y5R5rSeVs";
        const USERNAME_BOT_TELEGRAM_SAWOJAJAR = "@vatcashsawojajarbot";

        //Pakis
        const TOKEN_TELEGRAM_PAKIS = "854242580:AAERzsE8SRLWNXHYw-TBereS6x597lCC-1M";
        const USERNAME_BOT_TELEGRAM_PAKIS = "@vatcashpakisbot";

        //KAUMAN
        const TOKEN_TELEGRAM_KAUMAN = "821885173:AAFpB_tbcTpnJnNDcGuJWQ1DmlU6aau0iQE";
        const USERNAME_BOT_TELEGRAM_KAUMAN = "@vatcashkaumanbot";

        //SIGURA
        const TOKEN_TELEGRAM_SIGURA = "1087818490:AAHFxibEpofUVZj41Hsks5ryg7XCpl4mz5Y";
        const USERNAME_BOT_TELEGRAM_SIGURA = "@vatcashsigurabot";

        //SIGURA
        const TOKEN_TELEGRAM_LAWANG = "1102083889:AAEVUvxUgXQlrGdjY0RdPGvre1lft_6Z-ZU";
        const USERNAME_BOT_TELEGRAM_LAWANG = "@vatcashlawangbot";

        //GONDANG LEGI
        const TOKEN_TELEGRAM_GONDANG_LEGI = "1124665977:AAGwgj3QfSuHrJGTAtTHFvssVdu02NbbWXM";
        const USERNAME_BOT_TELEGRAM_GONDANG_LEGI = "@vatcashgondanglegibot";

        //PROBOLINGGO
        const TOKEN_TELEGRAM_PROBOLINGGO = "976537340:AAGZQX5xTtvEV_II5s_3UyXZOE-HOn1Xeqw";
        const USERNAME_BOT_TELEGRAM_PROBOLINGGO = "@vatcashprobolinggobot";

        //PASURUAN
        const TOKEN_TELEGRAM_PASURUAN = "1100502003:AAHWsZb-cFVx9kg88zr6-S60j151ZmTPQbw";
        const USERNAME_BOT_TELEGRAM_PASURUAN = "@vatcashpasuruanbot";

    }
