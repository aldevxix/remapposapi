<?php
    /**
     * Cart Controllers
     * @author Robet Atiq Maulana Rifqi
     * JUL 2019
     */

    use Pos\Config\Constants;
    use Pos\Models\CartModel;
    use Pos\Models\CommodityModel;
    use Pos\Systems\Request;
    use Pos\Systems\Response;

    $this->get( "/{cartId}", Cart::class . ":detail" );
    $this->post( "/user", Cart::class . ":getUserCart" );
    $this->post( "/add", Cart::class . ":add" );
    $this->post( "/edit", Cart::class . ":edit" );
    $this->post( "/remove", Cart::class . ":remove" );
    $this->post( "/buy", Cart::class . ":buy" );

    class Cart implements Constants {

        /**
         * @var CartModel
         * @var CommodityModel
         * @var Request
         * @var Response
         */
        private $cartModel;
        private $commodityModel;
        private $request;
        private $response;

        public function __construct () {

            // Initialize
            $this->cartModel = new CartModel();
            $this->commodityModel = new CommodityModel();
            $this->request = new Request();
            $this->response = new Response();
        }

        // Main Action Cart Item
        private function _actionCartItem ( $action, $data = null, $i = 0 ) {

            // Check Action
            if ( $action == self::ACTION_DELETE || ( $action == self::ACTION_UPDATE && $i == 0 )) {

                $this->cartModel->deleteItem([ ":cartId" => $this->request->get("cartId") ]);
            }

            // Check avaible product
            if ( $this->request->get("commodityId") != "" || $data != null ) {
                
                // Inserting data
                $this->cartModel->addItem([
                    ":cartId"       => $this->request->get("cartId"),
                    ":commodityId"  => $data == null ? $this->request->get("commodityId")[$i] : $data[$i]->commodityId,
                    ":amount"       => $data == null ? $this->request->get("amount")[$i] : $data[$i]->amount,
                    ":subTotal"     => $data == null ? $this->request->get("subTotal")[$i] : $data[$i]->subTotal
                ]);

                // Updating remains item
                $this->commodityModel->updateStock([ 
                    ":commodityId"  => $data[$i]->commodityId,
                    ":display"      => $data[$i]->remains
                ]);

                // Make Loop
                if ( $data != null && $i < sizeof ( $data ) - 1 ) return $this->_actionCartItem($action, $data, $i + 1);
                else if ( $i < sizeof ( $this->request->get("commodityId")) - 1 ) return $this->_actionCartItem($action, null, $i + 1);

                // Return
                return $this->cartModel->getItems([ ":cartId" => $this->request->get("cartId") ]);
            }

            return [];
        }

        // Main Action Cart
        private function _actionCart ( $action, $total = null, $data = null ) {

            // Main Action Cart
            $cart = $this->cartModel->actionCart([
                ":cartId"   => $this->request->get("cartId"),
                ":userId"   => $this->request->get("userId"),
                ":total"    => $total == null ? $this->request->get("total") : $total,
                ":status"   => $this->request->get("status") != "" ? $this->request->get("status") : self::CART_STATUS,
                ":action"   => $action
            ]);

            if ( $action == self::ACTION_CREATE ) $this->request->set( "cartId", $cart->cartId );

            // Performing action cart item
            $cart->item = ( array ) $this->_actionCartItem($action, $data);

            // Set Result
            $result = ( object ) [
                "data"  => $cart,
                "code"  => $cart->code
            ];

            if ( $cart->code == self::NOT_FOUND ) $result->message = "Cart Not Found";

            if ( $cart->code == self::FORBIDEN ) $result->message = "Illegal Access";

            unset ( $result->data->code );

            // Return
            return $result;
        }

        /**
         * Get Detail Cart
         *
         * @link cart/{cartId}
         * @method GET
         * @return object
         */
        public function detail ( $request, $response, $args ) {

            // Getting cart
            $cart = $this->cartModel->get([ ":cartId" => $args['cartId'] ]);

            if ( isset ( $cart->scalar )) return $this->response->publish ( null, "Cart Not Found", self::NOT_FOUND );

            // Getting cart Item
            $cart->item = ( array ) $this->cartModel->getItems([ ":cartId" => $args['cartId'] ]);

            return $this->response->publish( $cart, "Success Get Cart", self::SUCCESS );
        }

        /**
         * Get User Cart
         *
         * @link cart/user
         * @method POST
         * @return object
         */
        public function getUserCart ( $request, $response, $args ) {

            // Parsing request params
            $this->request->parse ( $request );

            $cart = ( array ) $this->cartModel->getUserCart([
                ":userId"   => $this->request->get("userId"),
                ":status"   => $this->request->get("status")
            ]);

            if ( sizeof($cart) == 0 ) return $this->response->publish ( null, "Cart Not Found", self::NOT_FOUND );

            return $this->response->publish( $cart, "Success Get User Cart", self::SUCCESS );
        }

        /**
         * Add Cart
         * 
         * @link cart/add
         * @method POST
         * @return object
         */
        public function add ( $request, $response, $args ) {

            // Parsing request params
            $this->request->parse ( $request );

            // Inserting to cart
            $cart = $this->_actionCart( self::ACTION_CREATE );

            if ( $cart->code != self::SUCCESS ) return $this->response->publish ( null, $cart->message, $cart->code );

            // Return
            return $this->response->publish ( $cart->data, "Success Add Cart", self::SUCCESS );
        }

        /**
         * Edit Cart
         * 
         * @link cart/edit
         * @method POST
         * @return object
         */
        public function edit ( $request, $response, $args ) {

            // Parsing request params
            $this->request->parse ( $request );

            // Edit cart
            $cart = $this->_actionCart(self::ACTION_UPDATE);

            if ( $cart->code != self::SUCCESS ) return $this->response->publish( null, $cart->message, $cart->code );

            // Return
            return $this->response->publish ( $cart->data, "Success Edit Cart", self::SUCCESS );
        }

        /**
         * Removing Cart
         * 
         * @link cart/remove
         * @method POST
         * @return object
         */
        public function remove ( $request, $response, $args ) {

            // Parsing Request params
            $this->request->parse ( $request );

            // Delete cart
            $cart = $this->_actionCart(self::ACTION_DELETE);

            if ( $cart->code != self::SUCCESS ) return $this->response->publish( null, $cart->message, $cart->code );

            // Return
            return $this->response->publish ( null, "Success Delete Cart", self::SUCCESS );
        }

        // Check avaibility
        private function _checkAvaibility ( $price = 0, $data = [], $items = [], $i = 0) {

            if ( $this->request->get("cartId") != "" ) {

                // Get Cart Detail
                if ( $i == 0 ) $items = ( array ) $this->cartModel->getItems([ ":cartId" => $this->request->get("cartId") ]);

                if ( sizeof ( $items ) == 0 ) return ( object ) [ "code" => self::NOT_FOUND, "message" => "Cart item not found" ];

                // Get Cart Item
                $item = ( object ) $items[$i];

                // Get product detail
                $product = $this->commodityModel->getDetail([ ":commodityId" => $item->commodityId ]);
                $stock = ( object ) $product->stock;
                $discount = ( object ) $product->discount;

                if ( $stock->display < $item->amount ) return ( object ) [ "code" => self::ERROR, "message" => "Product is not enough" ];

                // Count discount
                if ( $discount->discount != null || $discount->discount != "" ) {

                    $discount = $discount->minAmount <= $item->amount ? $discount->discount : 0;
                } else $discount = 0;

                $subTotal = ( $product->price * $item->amount ) - $discount;

                // Set Requirements data
                $data[$i] = ( object ) [
                    "commodityId"   => $product->commodityId,
                    "amount"        => $item->amount,
                    "discount"      => $discount,
                    "subTotal"      => $subTotal,
                    "remains"       => $stock->display - $item->amount
                ];

                // Increment total price
                $price += $subTotal;

                // Make Loop
                if ( $i < sizeof ( $items ) - 1 ) return $this->_checkAvaibility ( $price, $data, $items, $i + 1 );
                
            } else {

                // Get product detail
                $product = $this->commodityModel->getDetail([ ":commodityId" => $this->request->get("commodityId")[$i] ]);
                $stock = ( object ) $product->stock;
                $discount = ( object ) $product->discount;

                if ( $stock->display < $this->request->get("amount")[$i] ) return ( object ) [ "code" => self::ERROR, "message" => "Product is not enough" ];

                // Count discount
                if ( $discount->discount != null || $discount->discount != "" ) {

                    $discount = $discount->minAmount <= $this->request->get("amount")[$i] ? $discount->discount : 0;
                } else $discount = 0;

                // Count sub total
                $subTotal = ( $product->price * $this->request->get("amount")[$i] ) - $discount;

                // Set Requirements data
                $data[$i] = ( object ) [
                    "commodityId"   => $this->request->get("commodityId")[$i],
                    "amount"        => $this->request->get("amount")[$i],
                    "discount"      => $discount,
                    "subTotal"      => $subTotal,
                    "remains"       => $stock->display - $this->request->get("amount")[$i]
                ];

                // Increment total price
                $price += $subTotal;

                // Make loop
                if ( $i < sizeof ( $this->request->get("commodityId")) - 1 ) return $this->_checkAvaibility ( $price, $data, null, $i + 1);

                // Unset CommodityId
                $this->request->remove("commodityId");
            }

            // Return
            return ( object ) [
                "code"      => self::SUCCESS,
                "data"      => $data,
                "total"     => $price
            ];
        }

        /**
         * Buy Product
         * 
         * @method POST
         * @link cart/buy
         * @return object
         */
        public function buy ( $request, $response, $args ) {

            // Parsing request params
            $this->request->parse ( $request );

            // Set Default Status
            $this->request->set("status", self::BUY_STATUS);

            // Check Product avaibility
            $avaible = $this->_checkAvaibility();

            if ( $avaible->code != self::SUCCESS ) return $this->response->publish(null, $avaible->message, $avaible->code );

            // Performing cart action
            if ( $this->request->get("cartId") != null ) $cart = $this->_actionCart(self::ACTION_UPDATE, $avaible->total, $avaible->data );
            else $cart = $this->_actionCart(self::ACTION_CREATE, $avaible->total, $avaible->data);

            if ( $cart->code != self::SUCCESS ) return $this->response->publish ( null, $cart->message, $cart->code );

            // Return
            return $this->response->publish ( $cart->data, "Success Buy Product", self::SUCCESS );
        }
    }