<?php
/**
 * Telergam Controllers
 * @author Mohammad Ri
 * JUNI 2019
 */
require 'vendor/autoload.php';

use Pos\Config\Constants;
use Pos\Helpers\Location;
use Pos\Middlewares\Filter;
use Pos\Models\SalesModel;
use Pos\Models\CommodityModel;
use Pos\Models\ShopModel;
use Pos\Systems\Request;
use Pos\Systems\Response;
use PhpOffice\PhpSpreadsheet\Reader\IReader;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use Pos\Helpers\Telegram;
use Pos\Helpers\CalenderTelegram;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Pos\Telegram\TelegramBot;

$this->post("/uploadDocument", Bot::class . ":uploadDocument");
$this->post("/telegram", Bot::class . ":telegram");
$this->post("/telegram/jember", Bot::class . ":telegramJember");
$this->post("/telegram/pakis", Bot::class . ":telegramPakis");
$this->post("/telegram/sawojajar", Bot::class . ":telegramSawojajar");
$this->post("/telegram/kauman", Bot::class . ":telegramKauman");
$this->post("/telegram/sigura", Bot::class . ":telegramSigura");
$this->post("/telegram/lawang", Bot::class . ":telegramLawang");
$this->post("/telegram/gondangLegi", Bot::class . ":telegramGondangLegi");
$this->post("/telegram/probolinggo", Bot::class . ":telegramProbolinggo");
$this->post("/telegram/pasuruan", Bot::class . ":telegramPasuruan");

$this->get("/harian", Bot::class . ":harian");
$this->get("/bulanan", Bot::class . ":bulanan");
$this->get("/mingguan", Bot::class . ":mingguan");
$this->get("/tahunan", Bot::class . ":tahunan");

$this->get("/reportAll", Bot::class . ":reportAll");
$this->get("/report/bulanan", Bot::class . ":reportBulanan");
$this->get("/reportDay", Bot::class . ":reportDay");
$this->get("/reportDayAll", Bot::class . ":reportDayAll");
$this->get("/reportMonth", Bot::class . ":reportMonth");
$this->get("/reportMonthAll", Bot::class . ":reportMonthAll");
$this->get("/reportWeek", Bot::class . ":reportWeek");
$this->get("/reportWeekAll", Bot::class . ":reportWeekAll");

$this->get("/labaHarian", Bot::class . ":labaHarian");
$this->get("/labaMingguan", Bot::class . ":labaMingguan");
$this->get("/labaBulanan", Bot::class . ":labaBulanan");

$this->post("/updateProfit", Bot::class . ":updateHargaPembelian");
$this->post("/updateStokBarang", Bot::class . ":updateStokBarangApi");

$container['upload_directory'] = __DIR__ . '/export';

class Bot implements Constants
{


    public function __construct()
    {
        // Initialize
        $this->salesModel = new SalesModel();
        $this->shopModel = new ShopModel();
        $this->commodityModel = new CommodityModel();
        $this->request = new Request();
        $this->response = new Response();
        $this->calendarTelegram = new CalenderTelegram();
        $this->telegramBot = new TelegramBot();
    }

    public function uploadDocument($request, $response, $args)
    {
        $directory = "/home/develo16/public_html/api_pos/storage/";
        $uploadedFiles = $request->getUploadedFiles();
        if (isset($uploadedFiles['report'])) {
            $uploadedFile = $uploadedFiles['report'];
            $directory = $directory . "/export";
            if ($uploadedFile != null) {
                $filename = $this->moveUploadedFile($directory, $uploadedFile);
                $resultDate = $this->uploadReportFile($directory . "/" . $filename, "155152745");
                if ($resultDate != null) {
                    return $this->response->publish(null, "Success Insert Report ", self::SUCCESS);
                }
            }
        } else if (isset($uploadedFiles['stok'])) {
            $uploadedFile = $uploadedFiles['stok'];
            if ($uploadedFile != null) {
                $filename = $this->moveUploadedFile($directory, $uploadedFile);
                $directory = $directory . "/stok/" . $filename;
                $result = $this->updateStokBarang($directory);
                if ($result != null) {
                    return $this->response->publish(null, "Success Update Stock ", self::SUCCESS);
                }
            }
        }
        return $this->response->publish(null, "File upload not found", self::NOT_FOUND);

    }
    public function uploadReportFile($filePathLocal, $messageBot = null)
    {
        $reader = new Xls();
        $reader->setLoadSheetsOnly(["userList"]);
        $spreadsheet = $reader->load($filePathLocal);

//        $spreadsheet = $reader->load("/home/develo16/public_html/api_pos/storage/export/52296e8f89c165b4.xls");
        $activeWorksheet = $spreadsheet->getSheet(0);

        /**
         * Nama cabang di check di sell A3 jika ada nama disalah satu nama shop maka itu yang dikembalikan
         */
        $userIdData = explode(" ", $activeWorksheet->getCell("A2")->getValue());
        $userId = $userIdData[3];
        $nameCabang = $this->getNamaCabang(strtolower($activeWorksheet->getCell("A3")->getValue()));
        if ($nameCabang == "") {
            return null;
        }
        $shopId = $this->shopModel->getShopIdByName($nameCabang)->shopId;
        $date = $this->getDateTime($activeWorksheet->getCell("A4")->getValue());
        $jumlahPendapatan = $this->getTotalPendapatan($activeWorksheet->getCell("A7")->getValue());
        $totalTransaksi = $this->getTotalTransaksi($activeWorksheet->getCell("A8")->getValue());
        $totalBarangTerjual = $this->getTotalBarangTerjual($activeWorksheet);
        $result = $this->getDataTransaction($userId, $activeWorksheet, $shopId, $date);

        $barangLaris = $this->getBarangLaris($activeWorksheet);
        $jamRamai = $this->getWaktuRamai($activeWorksheet);

        if ($result == true) {
            $message = "[New Report] \nBos, cabang " . $nameCabang . " baru saja upload report untuk tanggal " . $date .
                "\n \n Hasil Ringkasan " .
                "\n Jumlah Pendapatan : " . $this->getTotalPendapatanRupiah($activeWorksheet->getCell("A7")->getValue()) .
                "\n Total Transaksi : " . $totalTransaksi .
                "\n Total Barang Terjual : " . $totalBarangTerjual .
                "\n\n Ringkasan penjualan barang :\n";
            for ($y = 0; $y < sizeof($barangLaris); $y++) {
                $message = $message . ($y + 1) . ". " . $barangLaris[$y];
            }

            $message = $message . "\n\n ringkasan waktu transaksi : \n";
            for ($u = 0; $u < sizeof($jamRamai); $u++) {
                $message = $message . " jam " . $jamRamai[$u];
            }

            $message = $message . "\n\nCabang Yang belum report sampai sekarang per tanggal " . $date . " adalah :";
            $resultShopIdHasReport = (array)$this->salesModel->getShopHasReport($date);
            $resultBranchYetReport = $this->getShopBranchExceptShopId($resultShopIdHasReport);
            for ($i = 0; $i < sizeof($resultBranchYetReport); $i++) {
                $message = $message . "\n" . ($i + 1) . ". " . $resultBranchYetReport[$i];
            }
            $this->sendMessageTelegram($message, $nameCabang);
            return $date;
        }
        return null;
    }


    public function updateHargaPembelian($pathFile)
    {

//        $pathFile = "/home/develo16/public_html/api_pos/storage/bati/bulanan/laba_2_2020.xlsx";
        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($pathFile);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($pathFile);
        $activeWorksheet = $spreadsheet->getActiveSheet();

        $cellColomCommodityId = "D";
        $cellColomHargaPembelian = "I";
        $cellRow = 11;

        $dataPembelian = [];
        for ($x = $cellRow; $x > 0; $x++) {
            $pembelian = [];
            $pembelian["commodityId"] = $activeWorksheet->getCell($cellColomCommodityId . $x)->getValue();
            $pembelian["harga"] = $activeWorksheet->getCell($cellColomHargaPembelian . $x)->getValue();
            $dataPembelian[] = $pembelian;
            if ($activeWorksheet->getCell($cellColomCommodityId . ($x + 1))->getValue() == null) {
                break;
            }
        }

        return $dataPembelian;
    }

    public function updateStokBarangApi($request, $response, $args)
    {
        $this->updateStokBarang(null);
    }

    public function updateStokBarang($pathFile)
    {
//        $pathFile = "/home/develo16/public_html/api_pos/storage/stok/stok_barang.xlsx";
        $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($pathFile);
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($pathFile);
        $activeWorksheet = $spreadsheet->getActiveSheet();

        $cellNamaBarang = "A";
        $cellJenisBarang = "B";
        $cellHargaBarang = "C";
        $cellStartCabang = "D";
        $cellRow = 2;
        $dataStokBarang = [];

        $shops = (array)$this->shopModel->getShopsByUser();

        for ($i = 1; $i <= sizeof($shops); $i++) {
            $shopId = $this->getShpoIdFromName($activeWorksheet->getCell($cellStartCabang . "1")->getValue(), $shops);
            if ($shopId > 0) {
                for ($x = $cellRow; $x > 0; $x++) {
                    $barang = [];
                    $barang["nama"] = $activeWorksheet->getCell($cellNamaBarang . $x)->getValue();
                    $barang["harga"] = $activeWorksheet->getCell($cellHargaBarang . $x)->getValue();
                    $barang["type"] = $activeWorksheet->getCell($cellJenisBarang . $x)->getValue();
                    $barang["stok"] = $activeWorksheet->getCell($cellStartCabang . $x)->getValue();
                    $barang["commodityId"] = $this->commodityModel->getIdByName($barang["nama"], $barang["harga"], $barang["type"]);
                    $this->commodityModel->insertOrUpdateCommodityShop($barang["commodityId"], $shopId, $barang["stok"], 0, 0);
                    if ($activeWorksheet->getCell($cellNamaBarang . ($x + 1))->getValue() == null) {
                        break;
                    }
                }
            }
            $cellStartCabang++;
        }
        return true;
    }

    function getShpoIdFromName($nameShop, $dataAllShop)
    {
        foreach ($dataAllShop as $shop) {
            if (strpos(strtolower($shop->branch), strtolower($nameShop)) !== false) {
                return $shop->shopId;
            }
        }
        return 0;
    }

    function sendMessageTelegram($message, $nameCabang)
    {
        $pakEdoId = "11828955";
        $buDindaId = "370102604";
        $abahWahyudi = "209729319";
        $rizky = "155152745";
        $chico = "677129765";
//            var_dump($message); die;
        $this->send_message($pakEdoId, $message, $nameCabang);
        $this->send_message($buDindaId, $message, $nameCabang);
        $this->send_message($rizky, $message, $nameCabang);
        $this->send_message($abahWahyudi, $message, $nameCabang);
//        $this->send_message($chico, $message, $nameCabang);
    }


    function getNamaCabang($namaCabangOri)
    {
        if (strpos($namaCabangOri, self::CABANG_KAUMAN) !== false) {
            return self::CABANG_KAUMAN;
        } else if (strpos($namaCabangOri, self::CABANG_SAWOJAJAR) !== false) {
            return self::CABANG_SAWOJAJAR;
        } else if (strpos($namaCabangOri, self::CABANG_SIGURA_GURA) !== false) {
            return self::CABANG_SIGURA_GURA;
        } else if (strpos($namaCabangOri, self::CABANG_GONDANG_LEGI) !== false) {
            return self::CABANG_GONDANG_LEGI;
        } else if (strpos($namaCabangOri, self::CABANG_LAWANG) !== false) {
            return self::CABANG_LAWANG;
        } else if (strpos($namaCabangOri, self::CABANG_PAKIS) !== false) {
            return self::CABANG_PAKIS;
        } else if (strpos($namaCabangOri, self::CABANG_JEMBER) !== false) {
            return self::CABANG_JEMBER;
        } else if (strpos($namaCabangOri, self::CABANG_PROBOLINGGO) !== false) {
            return self::CABANG_PROBOLINGGO;
        } else if (strpos($namaCabangOri, self::CABANG_PASURUAN) !== false) {
            return self::CABANG_PASURUAN;
        }
        return "";
    }

    function getDateTime($getDateTimeOri)
    {
        $dataTimeOri = explode(":", $getDateTimeOri);
        return $this->getDateFormatMySQL(trim($dataTimeOri[1]));
    }

    function getTotalPendapatan($getTotalPendapatanOri)
    {
        $getTotalPendapatanOri = explode(":", $getTotalPendapatanOri);
        $totalPendapatan = str_replace('Rp', '', $getTotalPendapatanOri[1]);
        $totalPendapatan = str_replace('.', '', $totalPendapatan);
        return trim($totalPendapatan);
    }

    function getTotalPendapatanRupiah($getTotalPendapatanOri)
    {
        $getTotalPendapatanOri = explode(":", $getTotalPendapatanOri);
        return trim($getTotalPendapatanOri[1]);
    }

    function getTotalTransaksi($getTotalTransaksiOri)
    {
        $getTotalTransaksiOri = explode(":", $getTotalTransaksiOri);
        return trim($getTotalTransaksiOri[1]);
    }

    function getTotalBarangTerjual($activeWorksheet, $i = 12)
    {
        for ($x = $i; $x > 0; $x++) {
            $positionCellCurrent = "A" . $x;
            $dataCell = strtolower($activeWorksheet->getCell($positionCellCurrent)->getValue());

            if (is_numeric($dataCell) !== false) {

                //other data
                $name = trim($activeWorksheet->getCell("C" . $x)->getValue());
                $amount = trim($activeWorksheet->getCell("E" . $x)->getValue());

                for ($l = 1; $l <= $amount; $l++) {
                    $barang[] = $name;
                }

            } else if ($dataCell == null) { // jika ada 2 data kosong maka hentikan
                if ($activeWorksheet->getCell("A" . ($x + 1))->getValue() == null) {
                    break;
                }
            }
        }
        // menghitung banyaknya barang
        $c = array_count_values($barang);
        arsort($c);
        foreach ($c as $x => $c) {
            $type = $this->salesModel->getTypeSales($x);
            $t = $type->description;
            $q[] = "$x ($t) = $c\n";
        }
        $jumlah = array_sum($q);

        return $jumlah;

    }

    function getDataTransaction($userId, $activeWorksheet, $shopId, $date, $i = 12)
    {
        // perulangan jika index + 2 kosong berhenti
        $salesId = 0;
        $secondaryTransId = 0;
        $dataCommodity = ( array )$this->commodityModel->getAllGeneralCommodity();
        $timeTransaction = "00:00:00";

        for ($x = $i; $x > 0; $x++) {
            $positionCellCurrent = "A" . $x;
            $dataCell = strtolower($activeWorksheet->getCell($positionCellCurrent)->getValue());

            if (strpos($dataCell, "transaksi ke") !== false) {
                $orderTransactionCell = explode("|", $dataCell);
                $timeTransactionCell = explode(" ", trim($orderTransactionCell[1]));
                $timeTransaction = trim($timeTransactionCell[2]);
                $secondaryTransId = trim($orderTransactionCell[2]);
            } else if (strpos($dataCell, "no") !== false) {
                if ($secondaryTransId == 0 || $secondaryTransId == null || $secondaryTransId == "") {
                    $secondaryTransId = mt_rand(10000000, 99999999);
                }

                $total = "0";
                // looping unlimited karena tidak diketahui baris total sampai berapa
                for ($a = $x + 1; $a > 0; $a++) {
                    // looping berhenti jika menemui kata teks total
                    if (strpos(strtolower($activeWorksheet->getCell("A" . $a)->getValue()), "total") !== false) {
                        $total = $this->getNumberRupiahValue($activeWorksheet->getCell("I" . $a)->getValue());
                        break;
                    }
                }

                // Insert Sales Transaction
                $salesId = $this->salesModel->insertSalesFromBot([
                    ":userId" => $userId,
                    ":secondaryId" => $secondaryTransId,
                    ":operatorId" => $userId,
                    ":shopId" => $shopId,
                    ":customerPaid" => $total,
                    ":remainingPayment" => 0,
                    ":discountPercent" => 0,
                    ":discountPotongan" => 0,
                    ":total" => $total,
                    ":totalAfterDiscount" => $total,
                    ":date" => $date,
                    ":time" => $timeTransaction,
                    ":status" => 1
                ], $date, $timeTransaction, $shopId, $secondaryTransId);

            } else if (is_numeric($dataCell) !== false) {

                $secondaryId = 0;
                $secondaryId = trim($activeWorksheet->getCell("B" . $x)->getValue());
                if ($secondaryId == 0 || $secondaryId == null || $secondaryId == "") {
                    $secondaryId = mt_rand(10000000, 99999999);
                }

                //other data
                $name = trim($activeWorksheet->getCell("C" . $x)->getValue());
                $type = trim($activeWorksheet->getCell("D" . $x)->getValue());
                $amount = trim($activeWorksheet->getCell("E" . $x)->getValue());
                $price = trim($activeWorksheet->getCell("F" . $x)->getValue());
                $discountPercent = trim($activeWorksheet->getCell("G" . $x)->getValue());
                $discountPotongan = trim($activeWorksheet->getCell("H" . $x)->getValue());
                $commodityId = $this->getCommodityIdFromName($name, $type, $price, $dataCommodity);

                // jumlah total transaksi
                $total = trim($activeWorksheet->getCell("I" . $x)->getValue());
                $total = str_replace('Rp', '', $total);
                $total = str_replace('.', '', $total);

                // Inserting item
                $this->salesModel->insertSalesItemBot([
                    ":salesId" => $salesId,
                    ":secondaryTransId" => $secondaryTransId,
                    ":commodityId" => $commodityId,
                    ":secondaryId" => $secondaryId,
                    ":amount" => $amount,
                    ":price" => $this->getFinalPriceCommodity($price, $discountPercent, $discountPotongan),
                    ":oldPrice" => $this->getNumberRupiahValue($price),
                    ":discountPercent" => $discountPercent,
                    ":discountPotongan" => $discountPotongan
                ], $commodityId, $salesId, $amount, $shopId, $secondaryId);

            } else if (strpos($dataCell, "total") !== false) {
                $salesId = 0;
                $secondaryTransId = 0;
            } else if ($dataCell == null) { // jika ada 2 data kosong maka hentikan
                if ($activeWorksheet->getCell("A" . ($x + 1))->getValue() == null) {
                    break;
                }
            }
        }
        return true;
    }

    function getShopId($shopName)
    {

        return 0;
    }

    function moveUploadedFile($directory, $uploadedFile)
    {
        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        $basename = bin2hex(random_bytes(8));
        $filename = sprintf('%s.%0.8s', $basename, $extension);

        $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

        return $filename;
    }

    function getNumberRupiahValue($valueRupiah)
    {
        $valueRupiah = str_replace('Rp', '', $valueRupiah);
        $valueRupiah = str_replace('.', '', $valueRupiah);
        return trim($valueRupiah);
    }

    function getCommodityIdFromName($commodityName, $commodityType, $commodityPrice, $dataCommodity)
    {
        $commodityId = null;

        for ($i = 0; $i < sizeof($dataCommodity); $i++) {
            if (strtolower($commodityName) == strtolower($dataCommodity[$i]["name"])) {
                $commodityId = $dataCommodity[$i]["commodityId"];
                break;
            }
        }
        if ($commodityId == null) {
            $commodityId = $this->commodityModel->insertCommodity([
                ":userId" => 1,
                ":name" => $commodityName,
                ":description" => $commodityType,
                ":price" => $commodityPrice,
                ":date" => date("Y-m-d"),
                ":time" => date("H:i:s"),
                ":images" => "",
            ]);
        }

        return $commodityId;
    }

    function getFinalPriceCommodity($oldPrice, $discountPercent, $discountPotongan)
    {
        $price = (int)$this->getNumberRupiahValue($oldPrice);
        if ($discountPercent > 0.0) {
            $price = ((double)$discountPercent * $price / 100) + $price;
        } else if ($discountPotongan > 0) {
            $price = $price - (int)$discountPotongan;
        }
        return $price;
    }

    function getDateFormatMySQL($date)
    {
        $dateSplit = explode(" ", $date);
        $month = $this->changeMonthNameToNumber($dateSplit[1]);
        return $dateSplit[2] . "-" . $month . "-" . $dateSplit[0];
    }

    function changeMonthNameToNumber($monthName)
    {
        $monthName = strtolower($monthName);
        switch ($monthName) {
            case "januari":
                return 1;
            case "februari":
                return 2;
            case "maret":
                return 3;
            case "april":
                return 4;
            case "mei":
                return 5;
            case "juni":
                return 6;
            case "juli":
                return 7;
            case "agustus":
                return 8;
            case "september":
                return 9;
            case "oktober":
                return 10;
            case "november":
                return 11;
            case "desember":
                return 12;
        }
        return 0;
    }

    public function telegram($request, $response, $args)
    {
        $this->processTelegram($request, null);
    }

    public function telegramJember($request, $response, $args)
    {
        $this->processTelegram($request, "jember");
    }

    public function telegramPakis($request, $response, $args)
    {
        $this->processTelegram($request, "pakis");
    }

    public function telegramSawojajar($request, $response, $args)
    {
        $this->processTelegram($request, "sawojajar");
    }

    public function telegramKauman($request, $response, $args)
    {
        $this->processTelegram($request, "kauman");
    }

    public function telegramSigura($request, $response, $args)
    {
        $this->processTelegram($request, "sigura");
    }

    public function telegramLawang($request, $response, $args)
    {
        $this->processTelegram($request, "lawang");
    }

    public function telegramGondangLegi($request, $response, $args)
    {
        $this->processTelegram($request, "gondang");
    }

    public function telegramProbolinggo($request, $response, $args)
    {
        $this->processTelegram($request, "probolinggo");
    }

    public function telegramPasuruan($request, $response, $args)
    {
        $this->processTelegram($request, "pasuruan");
    }

    public function processTelegram($request, $cabang)
    {
        $body = $request->getBody();
        $this->addLog($body);
        $message = json_decode($body, true);
        $this->process_message($message, $cabang, $request);
    }

    function process_message($message, $cabang, $request)
    {
        $callback_query = $message["callback_query"];
        if (isset($callback_query)) {
            $data = $callback_query["data"];
            $message = $callback_query["message"];
            $messageId = $callback_query["message"]["message_id"];
            $chatId = $message["chat"]["id"];
            $this->deleteMessage($chatId, $messageId, $cabang);
            $this->processMessageCommand($chatId, $data, $cabang);
        } else {
            $updateid = $message["update_id"];
            $message_data = $message["message"];
            $chatid = $message_data["chat"]["id"];
            $message_id = $message_data["message_id"];
            $response = "";
            if (isset($message_data["text"])) {
                $text = $message_data["text"];
                $this->processMessageCommand($chatid, $text, $cabang);
            } else if (isset($message_data["document"])) {
                $getNameFile = $message_data["document"]["file_name"];
                if (strpos($getNameFile, "export_daily") !== false) {
                    $getPathFile = $this->getPathFileTelegram($message_data["document"]["file_id"], $cabang);
                    $getPathFileLocal = $this->getFileTelegram($getPathFile, $cabang, "report", $getNameFile);
                    $response = "Report berhasil diupload";
                    $this->send_message($chatid, $response, $cabang);
                    $this->send_message($chatid, "Mulai proses syncron report, tunggu sebentar yaah.. :D", $cabang);
                    $resultDate = $this->uploadReportFile($getPathFileLocal);
                    $this->send_message($chatid, "Oke! Report untuk tanggal " . $resultDate . " sudah berhasil di upload di server. Thank you...", $cabang);
                } else if (strpos($getNameFile, "laba") !== false) {
                    $this->addLog("mulai laba");
                    $kindLaba = explode($getNameFile, "_");
                    $kindLaba = explode($kindLaba[1], "_");
                    if (sizeof($kindLaba) == 2) {
                        $kindLaba = "laba_harian";
                    } else if (sizeof($kindLaba) == 1) {
                        $kindLaba = "laba_bulanan";
                    } else {
                        $kindLaba = "laba_tahunan";
                    }
                    $this->addLog($kindLaba);
                    $getPathFile = $this->getPathFileTelegram($message_data["document"]["file_id"], $cabang);
                    $this->addLog($getPathFile);
                    $getPathFileLocal = $this->getFileTelegram($getPathFile, $cabang, $kindLaba, $getNameFile);
                    $this->addLog($getPathFileLocal);
                    $response = "Update harga kulakan berhasil diupload";
                    $this->send_message($chatid, $response, $cabang);
                    $this->send_message($chatid, "Mulai proses syncron harga kulakan, tunggu sebentar yaah.. :D", $cabang);
                    $result = $this->updateHargaPembelian($getPathFileLocal);
                    $this->addLog($result);
                    $this->send_message($chatid, "Oke! Update harga kulakan sudah berhasil di update di server. Thank you...", $cabang);
                } else if (strpos($getNameFile, "stok") !== false) {
                    $this->addLog("mulai stok");
                    $getPathFile = $this->getPathFileTelegram($message_data["document"]["file_id"], $cabang);
                    $this->addLog($getPathFile);
                    $getPathFileLocal = $this->getFileTelegram($getPathFile, $cabang, "laba", $getNameFile);
                    $this->addLog($getPathFileLocal);
                    $response = "Update stok dan data barang berhasil di upload";
                    $this->send_message($chatid, $response, $cabang);
                    $this->send_message($chatid, "Mulai proses syncron data dan stok barang, tunggu sebentar yaah.. :D", $cabang);
                    $result = $this->updateStokBarangApi($getPathFileLocal);
                    $this->addLog($result);
                    $this->send_message($chatid, "Oke! Update data dan stok barang sudah berhasil di update di server. Thank you...", $cabang);
                } else {
                    $this->send_message($chatid, "Sory, file yang anda upload tidak dapat di proses bos! beda format maafkeun..", $cabang);
                    return $updateid;
                }
            }
        }

        return $updateid;
    }

    function processMessageCommand($chatid, $text, $cabang, $messageId = null)
    {
        $markup = null;
        $message = null;
        switch ($text) {
            case "/menu":
                $keyboard = array(
                    'inline_keyboard' => [
                        [['text' => "laporan", 'callback_data' => "/laporan"]],
                        [['text' => "laba", 'callback_data' => "/profit"]]
                    ],
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true
                );
                $message = "Pilih salah satu menu :";
                $markup = json_encode($keyboard, true);
                break;
            case "/profit":
                $keyboard = array(
                    'inline_keyboard' => [
                        [['text' => "Harian", 'callback_data' => "/profit_day"]],
                        [['text' => "Bulanan", 'callback_data' => "/profit_month"]],
                        [['text' => "Tahunan", 'callback_data' => "/profit_year"]]
                    ],
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true
                );
                $message = "Pilih salah satu jenis laba :";
                $markup = json_encode($keyboard, true);
                break;
            case "/laporan":
                $keyboard = array(
                    'inline_keyboard' => [
                        [['text' => "Harian", 'callback_data' => "/report_day"]],
                        [['text' => "Bulanan", 'callback_data' => "/report_month"]],
                        [['text' => "Tahunan", 'callback_data' => "/report_year"]]
                    ],
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true
                );
                $message = "Pilih salah satu jenis laporan :";
                $markup = json_encode($keyboard, true);
                break;
            case "/report_day":
                $keyboard = array(
                    'inline_keyboard' => [
                        [['text' => "File Excel", 'callback_data' => "/report_day_file"]],
                        [['text' => "Teks", 'callback_data' => "/report_day_teks"]],
                    ],
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true
                );
                $message = "Pilih salah satu format laporan harian:";
                $markup = json_encode($keyboard, true);
                break;
            case "/report_month":
                $keyboard = array(
                    'inline_keyboard' => [
                        [['text' => "File Excel", 'callback_data' => "/report_month_file"]],
                        [['text' => "Teks", 'callback_data' => "/report_month_teks"]],
                    ],
                    'resize_keyboard' => true,
                    'one_time_keyboard' => true
                );
                $message = "Pilih salah satu format laporan bulanan:";
                $markup = json_encode($keyboard, true);
                break;
            case "/profit_day":
                $this->send_message($chatid, "Ketikkan format penulisan untuk mendapat file excel laba harian \n\nFormat : \n- file laba harian [cabang] [DD-MM-YYYY] \n- kalau ingin semua cabang tidak perlu dikasihkan cabang \n\nContoh : \n- file laba harian kauman 02-02-2020 \n- file laba harian 02-02-2020", $cabang);
                break;
            case "/profit_month":
                $this->send_message($chatid, "Ketikkan format penulisan untuk mendapat file excel laba bulanan \n\nFormat : \n- file laba bulanan [cabang] [MM-YYYY] \n- kalau ingin semua cabang tidak perlu dikasihkan cabang \n\nContoh : \n- file laba bulanan kauman 02-2020 \n- file laba bulanan 02-2020", $cabang);
                break;
            case "/report_day_file":
                $this->send_message($chatid, "Ketikkan penulisan tanggal report harian untuk mendapat file \n\nFormat : \n- file laporan harian [cabang] [DD-MM-YYYY] \n- kalau ingin semua cabang tidak perlu dikasihkan cabang \n\nContoh : \n- file laporan harian kauman 02-02-2020 \n- file laporan harian 02-02-2020", $cabang);
                break;
            case "/report_day_teks":
                $this->send_message($chatid, "Ketikkan penulisan tanggal report harian untuk mendapat hasil teks \n\nFormat : \n- teks laporan harian [cabang] [DD-MM-YYYY] \n- kalau ingin semua cabang tidak perlu dikasihkan cabang \n\nContoh : \n- teks laporan harian kauman 02-02-2020 \n- teks laporan harian 02-02-2020", $cabang);
                break;
            case "/report_month_file":
                $this->send_message($chatid, "Ketikkan penulisan tanggal report bulanan untuk mendapat file \n\nFormat : \n- file laporan bulanan [cabang] [MM-YYYY] \n- kalau ingin semua cabang tidak perlu dikasihkan cabang \n\nContoh : \n- file laporan bulanan kauman 02-2020 \n- file laporan bulanan 02-2020", $cabang);
                break;
            case "/report_month_teks":
                $this->send_message($chatid, "Ketikkan penulisan tanggal report bulanan untuk mendapat hasil teks \n\nFormat : \n- teks laporan bulanan [cabang] [MM-YYYY] \n- kalau ingin semua cabang tidak perlu dikasihkan cabang \n\nContoh : \n- teks laporan bulanan kauman 02-2020 \n- teks laporan bulanan 02-2020", $cabang);
                break;
            default:
                $this->processMessageTeksComplex($chatid, $text, $cabang, $messageId);
                return;

        }
        if ($messageId == null) {
            $this->send_message($chatid, $message, $cabang, $markup);
        } else {
            $this->editMessageReplyMarkup($chatid, $messageId, $message, $cabang, $markup);
        }
    }


    function processMessageTeksComplex($chatid, $text, $cabang, $messageId = null)
    {
        $this->addLog("process");
        $textSplit = explode(" ", $text);
        $this->addLog($textSplit);
        if (sizeof($textSplit) <= 1) {
            $text = "Sory gaes, perintah belum diketahui, tenang sudah disimpan kok, akan kami balas secepatnya nggih,... pangapunten";
            $this->send_message($chatid, $text, $cabang);
        } else {
            $keywordFileLaporanHarian = array("file", "laporan", "hari", "tanggal");
            $keywordTeksLaporanHarian = array("teks", "laporan", "hari", "tanggal");
            $keywordFileLaporanBulanan = array("file", "laporan", "bulan", "tanggal_bulan");
            $keywordTeksLaporanBulanan = array("teks", "laporan", "bulan", "tanggal_bulan");
            $keywordFileLabaHarian = array("file", "laba", "hari", "tanggal");
            $keywordFileLabaBulanan = array("file", "laba", "bulan", "tanggal_bulan");
            if ($this->checkKeyword($textSplit, $keywordFileLaporanHarian)) {
                $newDate = date("Y-m-d", strtotime($this->getDateFromKeyword($textSplit)));
                $_GET['tanggal'] = $newDate;
                $result = $this->reportDayAll();
                $this->addLog($result);
                $this->sendMessageFile($chatid, "Result", $cabang, $result);
            } else if ($this->checkKeyword($textSplit, $keywordTeksLaporanHarian)) {
                $newDate = date("Y-m-d", strtotime($this->getDateFromKeyword($textSplit)));
                $_GET['tanggal'] = $newDate;
                $result = $this->getHarianAll();
                $this->addLog($result);
                $this->send_message($chatid, $result, $cabang, null);
            } else if ($this->checkKeyword($textSplit, $keywordFileLaporanBulanan)) {
                $_GET['bulan'] = $this->getMonthValueFromMonthKeyword($textSplit);
                $_GET['tahun'] = $this->getYearValueFromMonthKeyword($textSplit);
                $result = $this->reportMonthAll();
                $this->addLog($result);
                $this->sendMessageFile($chatid, "Result", $cabang, $result);
            } else if ($this->checkKeyword($textSplit, $keywordTeksLaporanBulanan)) {
                $_GET['bulan'] = $this->getMonthValueFromMonthKeyword($textSplit);
                $_GET['tahun'] = $this->getYearValueFromMonthKeyword($textSplit);
                $result = $this->getReportBulananAll();
                $this->addLog($result);
                $this->send_message($chatid, $result, $cabang, null);
            } else if ($this->checkKeyword($textSplit, $keywordFileLabaHarian)) {
                $this->send_message($chatid, "Bentar nggih, masih digenarate bosque.. :D", $cabang, null);
                $newDate = date("Y-m-d", strtotime($this->getDateFromKeyword($textSplit)));
                $_GET['tanggal'] = $newDate;
                $result = $this->labaHarian();
                $this->addLog($result);
                $this->sendMessageFile($chatid, "Result", $cabang, $result);
            }
//            else if ($this->checkKeyword($textSplit, $keywordFileLabaBulanan)) {
//                $this->send_message($chatid, "Bentar nggih, masih digenarate bosque.. :D", $cabang, null);
//                $_GET['bulan'] = $this->getMonthValueFromMonthKeyword($textSplit);
//                $_GET['tahun'] = $this->getYearValueFromMonthKeyword($textSplit);
//                $result = $this->labaBulanan();
//                $this->addLog($result);
//                $this->sendMessageFile($chatid, "Result", $cabang, $result);
//            }
            else {
                $text = "Sory gaes, perintah belum diketahui, tenang sudah disimpan kok, akan kami balas secepatnya nggih,... pangapunten";
                $this->send_message($chatid, $text, $cabang, null);
            }
        }
    }

    function checkKeyword($splitText, $keywordList)
    {
        foreach ($keywordList as $keyword) {
            $isFound = false;
            foreach ($splitText as $text) {
                if ($keyword == "tanggal") {
                    if ($this->validateDate($text)) {
                        $isFound = true;
                    }
                } else if ($keyword == "tanggal_bulan") {
                    if ($this->validateMonth($text)) {
                        $isFound = true;
                    }
                } else if (strpos($text, $keyword) !== false) {
                    $isFound = true;
                }
            }
            if ($isFound == false) {
                return false;
            }
        }
        return true;
    }

    function validateDate($date, $format = 'd-m-Y')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    function validateMonth($date, $format = 'm-Y')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    function getDateFromKeyword($splitText)
    {
        foreach ($splitText as $text) {
            if ($this->validateDate($text)) {
                return $text;
            }
        }
        return "";
    }

    function getMonthValueFromMonthKeyword($splitText)
    {
        foreach ($splitText as $text) {
            if ($this->validateMonth($text)) {
                $monthSplit = explode("-", $text);
                return $monthSplit[0];
            }
        }
        return "";
    }

    function getYearValueFromMonthKeyword($splitText)
    {
        foreach ($splitText as $text) {
            if ($this->validateMonth($text)) {
                $monthSplit = explode("-", $text);
                return $monthSplit[1];
            }
        }
        return "";
    }

    function send_message($chatid, $text, $cabang, $replyMarkup = null)
    {
        // jika text lebih dari 4096 karakter dikirim sebanyak array
        $splitMaxCharTelegram = $this->splitLongText($text);
        foreach ($splitMaxCharTelegram as $textSplit) {
            if ($replyMarkup != null) {
                $data = array(
                    'chat_id' => $chatid,
                    'text' => $textSplit,
                    'reply_markup' => $replyMarkup
                );
            } else {
                $data = array(
                    'chat_id' => $chatid,
                    'text' => $textSplit
                );
            }

            $url = $this->request_url('sendMessage', $cabang);
            $result = $this->url_get_contents($url, $data);
            $this->addLog($result);
        }
    }

    function editMessageReplyMarkup($chatid, $messageId, $text, $cabang, $replyMarkup = null)
    {
        $data = array(
            'chat_id' => $chatid,
            'text' => $text,
            'reply_markup' => $replyMarkup,
            'message_id' => $messageId

        );
        $url = $this->request_url('editMessageReplyMarkup', $cabang);
        $result = $this->url_get_contents($url, $data);
        $this->addLog($result);
    }


    function deleteMessage($chatid, $messageId, $cabang)
    {
        $data = array(
            'chat_id' => $chatid,
            'message_id' => $messageId
        );

        $url = $this->request_url('deleteMessage', $cabang);
        $result = $this->url_get_contents($url, $data);
        $this->addLog($result);

    }

    function sendMessageFile($chatid, $text, $cabang, $filePath)
    {
        // jika text lebih dari 4096 karakter dikirim sebanyak array
        $splitMaxCharTelegram = $this->splitLongText($text);
        foreach ($splitMaxCharTelegram as $textSplit) {
            if ($filePath != null) {
                $data = array(
                    'chat_id' => $chatid,
                    'text' => $filePath,
                    'document' => new CURLFile(realpath($filePath))
                );
                $url = $this->request_url('sendDocument', $cabang);
                $result = $this->url_get_contentsFile($url, $data);
                $this->addLog($result);
            }
        }
    }

    function send_reply($chatid, $msgid, $text, $cabang)
    {
        $data = array(
            'chat_id' => $chatid,
            'text' => $text,
            'reply_to_message_id' => $msgid
        );

        $url = $this->request_url('sendMessage', $cabang);
        $result = $this->url_get_contents($url, $data);
        $this->addLog($result);
    }

    function url_get_contents($Url, $data)
    {
        if (!function_exists('curl_init')) {
            $this->addLog('CURL is not installed!');
            die('CURL is not installed!');
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $Url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/x-www-form-urlencoded"));
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    function url_get_contentsFile($Url, $data)
    {
        if (!function_exists('curl_init')) {
            $this->addLog('CURL is not installed!');
            die('CURL is not installed!');
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $Url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    function create_response($text)
    {
        return "definisi " . $text;
    }

    function request_url($method, $cabang)
    {
        return "https://api.telegram.org/bot" . $this->getTelegramTokenFromCabang($cabang) . "/" . $method;
    }

    function addLog($data)
    {
        $myfile = fopen("/home/develo16/public_html/api_pos/storage/log/log.txt", "a") or die("Unable to open file!");
        $txt = "user id date";
        fwrite($myfile, "\n" . $data);
        fclose($myfile);
    }

    function getPathFileTelegram($fileId, $cabang)
    {
        $path = "";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/bot" . $this->getTelegramTokenFromCabang($cabang) . "/getFile?file_id=" . $fileId);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_GET, true);
        $output = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($output, true);
        if ($response["ok"] == true) {
            $path = $response["result"]["file_path"];
        }
        $this->addLog($path);
        return $path;
    }

    function getFileTelegram($filePath, $cabang)
    {
        $directory = "/home/develo16/public_html/api_pos/storage";
        $file = fopen($directory . "/" . $filePath, "w+");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.telegram.org/file/bot" . $this->getTelegramTokenFromCabang($cabang) . "/" . $filePath);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_FILE, $file);;
        $data = curl_exec($ch);
        $error = curl_error($ch);
        curl_close($ch);
        fclose($file);
        return $directory . "/" . $filePath;
    }

    function getShopBranchExceptShopId($shopIds)
    {
        $shops = (array)$this->shopModel->getShopsByUser();
        $data = [];
        for ($i = 0; $i < sizeof($shops); $i++) {
            $isfound = false;
            for ($a = 0; $a < sizeof($shopIds); $a++) {
                if ($shopIds[$a]["shopId"] == $shops[$i]->shopId) {
                    $isfound = true;
                }
            }
            if ($isfound == false) {
                array_push($data, $shops[$i]->branch);
            }
        }
        return $data;
    }

    function getBarangLaris($activeWorksheet, $i = 12)
    {
        for ($x = $i; $x > 0; $x++) {
            $positionCellCurrent = "A" . $x;
            $dataCell = strtolower($activeWorksheet->getCell($positionCellCurrent)->getValue());

            if (is_numeric($dataCell) !== false) {

                //other data
                $name = trim($activeWorksheet->getCell("C" . $x)->getValue());
                $amount = trim($activeWorksheet->getCell("E" . $x)->getValue());

                for ($l = 1; $l <= $amount; $l++) {
                    $barang[] = $name;
                }

            } else if ($dataCell == null) { // jika ada 2 data kosong maka hentikan
                if ($activeWorksheet->getCell("A" . ($x + 1))->getValue() == null) {
                    break;
                }
            }
        }
        // menghitung banyaknya barang
        $c = array_count_values($barang);
        arsort($c);
        foreach ($c as $x => $c) {
            $q[] = "$x = $c\n";
        }

        return $q;
    }

    function getWaktuRamai($activeWorksheet, $i = 12)
    {
        for ($x = $i; $x > 0; $x++) {
            $positionCellCurrent = "A" . $x;
            $dataCell = strtolower($activeWorksheet->getCell($positionCellCurrent)->getValue());

            if (strpos($dataCell, "transaksi ke") !== false) {
                $orderTransactionCell = explode("|", $dataCell);
                $timeTransactionCell = explode(" ", trim($orderTransactionCell[1]));
                $timeTransaction = trim($timeTransactionCell[2]);

                $getJam = explode(":", $timeTransaction);
                $getJamreal = trim($getJam[0]);
                $arJam[] = $getJamreal;

            } else if ($dataCell == null) { // jika ada 2 data kosong maka hentikan
                if ($activeWorksheet->getCell("A" . ($x + 1))->getValue() == null) {
                    break;
                }
            }
        }

        // menghitung banyaknya jam
        $v = array_count_values($arJam);
        arsort($v);
        foreach ($v as $k => $v) {
            $p = $k + 1;
            $w[] = "$k-" . $p . ": $v transaksi\n";
        }

        return $w;
    }

    function getTelegramTokenFromCabang($cabang)
    {
        if (strpos($cabang, self::CABANG_KAUMAN) !== false) {
            return self::TOKEN_TELEGRAM_KAUMAN;
        } else if (strpos($cabang, self::CABANG_SAWOJAJAR) !== false) {
            return self::TOKEN_TELEGRAM_SAWOJAJAR;
        } else if (strpos($cabang, self::CABANG_SIGURA_GURA) !== false) {
            return self::TOKEN_TELEGRAM_SIGURA;
        } else if (strpos($cabang, self::CABANG_GONDANG_LEGI) !== false) {
            return self::TOKEN_TELEGRAM_GONDANG_LEGI;
        } else if (strpos($cabang, self::CABANG_LAWANG) !== false) {
            return self::TOKEN_TELEGRAM_LAWANG;
        } else if (strpos($cabang, self::CABANG_PAKIS) !== false) {
            return self::TOKEN_TELEGRAM_PAKIS;
        } else if (strpos($cabang, self::CABANG_JEMBER) !== false) {
            return self::TOKEN_TELEGRAM_JEMBER;
        } else if (strpos($cabang, self::CABANG_PROBOLINGGO) !== false) {
            return self::TOKEN_TELEGRAM_PROBOLINGGO;
        } else if (strpos($cabang, self::CABANG_PASURUAN) !== false) {
            return self::TOKEN_TELEGRAM_PASURUAN;
        }
        return self::TOKEN_TELEGRAM;
    }

    public
    function harian($request = null, $response = null, $args = null)
    {
        $this->sendMessageTelegram($this->getHarianAll($request, $response, $args), null);
        return $this->response->publish(null, "Success Report Harian", self::SUCCESS);
    }

    public function getHarianAll($request = null, $response = null, $args = null)
    {
        error_reporting(E_WARNING);
        if (isset($_GET['tanggal'])) {
            $tgl = date("Y-m-d", strtotime($_GET['tanggal']));
        } else {
            $tgl = date("Y-m-d");
        }

        $hasil = $this->salesModel->getShopYear($tgl);
        $barjual = $this->salesModel->getTotalSales($tgl);
        $totransaksi = $hasil->total_transaksi;
        $total = $hasil->total;
        $barang_terjual = $barjual->total_barang_terjual;

        $result = "[REKAP HARIAN $tgl]\n\nCabang yang report:";
        $resultShopIdHasReport = (array)$this->salesModel->getShopHasReport($tgl);
        $resultBranchYetReport = $this->getShopBranchExceptShopIda($resultShopIdHasReport);
        for ($i = 0; $i < sizeof($resultBranchYetReport); $i++) {
            $result = $result . "\n" . ($i + 1) . ". " . $resultBranchYetReport[$i];
        }

        $result .= "\n\nRekapitulasi Total Pendapatan" .
            "\nTotal Pendapatan : Rp. " . number_format($total, 0, "", ".") .
            "\nTotal Transaksi : " . $totransaksi .
            "\nTotal Barang Terjual : " . $barang_terjual;

        $id = array_reverse($resultShopIdHasReport);
        asort($id);
        foreach ($id as $di => $id) {
            $k[] = $id;
        }

        $result .= "\n\nRekapitulasi Setiap Cabang";
        for ($a = 0; $a < sizeof($resultBranchYetReport); $a++) {
            $transaksi = $this->salesModel->getShopYear($tgl, $k[$a]["shopId"]);
            $pendapatan = $this->salesModel->getTotalSales($tgl, $k[$a]["shopId"]);

            $result = $result . "\n" . ($a + 1) . ". " . $resultBranchYetReport[$a];
            $result .= "\nPendapatan : Rp. " . number_format($transaksi->total, 0, "", ".") .
                "\nTotal Transaksi : $transaksi->total_transaksi" .
                "\nTotal Barang Terjual : $pendapatan->total_barang_terjual\n";
        }

        $rekap_barang = $this->salesModel->getRekapSales($tgl);
        for ($i = 0; $i < sizeof($rekap_barang); $i++) {
            for ($j = 0; $j < $rekap_barang[$i]["jumlah"]; $j++) {
                $totbar[] = $rekap_barang[$i]["nama"];
            }
        }

        $c = array_count_values($totbar);
        arsort($c);
        foreach ($c as $x => $c) {
            $q[] = "$x = $c\n";
        }

        $result .= "\nRekapitulasi Barang Terjual\n";
        for ($y = 0; $y < sizeof($q); $y++) {
            $result = $result . ($y + 1) . ". " . $q[$y];
        }

        $rekap_waktu = $this->salesModel->getRekapTime($tgl);
        for ($l = 0; $l < sizeof($rekap_waktu); $l++) {
            $getJam = explode(":", $rekap_waktu[$l]["time"]);
            $getJamreal = trim($getJam[0]);
            $arJam[] = $getJamreal;
        }

        $v = array_count_values($arJam);
        arsort($v);
        foreach ($v as $k => $v) {
            $p = $k + 1;
            $w[] = "$k-" . $p . ": $v transaksi\n";
        }

        $result .= "\n\nRekapitulasi Waktu Transaksi\n";
        for ($y = 0; $y < sizeof($w); $y++) {
            $result = $result . "Jam " . $w[$y];
        }
        return $result;
    }

    public
    function bulanan($request = null, $response = null, $args = null)
    {
        $this->sendMessageTelegram($this->getReportBulananAll($request, $response, $args), null);
        return $this->response->publish(null, "Success Report Bulanan", self::SUCCESS);
    }

    public function getReportBulananAll($request = null, $response = null, $args = null)
    {
        error_reporting(E_WARNING);
        if (isset($_GET['bulan']) || isset($_GET['tahun'])) {
            $bln = $_GET['bulan'];
            $tahun = $_GET['tahun'];
        } else {
            $bln = date('n');
            $tahun = date("Y");
            if ($bln = 1) {
                $bln = 12;
                $tahun = $tahun - 1;
            } else {
                $bln = $bln - 1;
            }
        }

        $result = "[REKAP BULAN $bln - $tahun]\n\n";

        $total = $this->salesModel->getTotal($bln);
        $barang = $this->salesModel->getTotalMonth($bln);
        $resultShopIdHasReport = (array)$this->salesModel->getShopHasReportm($bln);
        $resultBranchYetReport = $this->getShopBranchExceptShopIda($resultShopIdHasReport);
        $result = $result . "Total pendapatan semua cabang : Rp. " . number_format($total->total, 0, "", ".") .
            "\nTotal transaksi semua cabang : " . number_format($total->total_transaksi, 0, "", ".") .
            "\nTotal barang terjual setiap cabang : " . number_format($barang->total_barang_terjual, 0, "", ".") . "\n\n";

        $id = array_reverse($resultShopIdHasReport);
        asort($id);
        foreach ($id as $di => $id) {
            $k[] = $id;
        }

        for ($a = 0; $a < sizeof($resultBranchYetReport); $a++) {
            $pendapatan = $this->salesModel->getTotal($bln, $k[$a]["shopId"]);
            $barjual = $this->salesModel->getTotalMonth($bln, $k[$a]["shopId"]);

            $result = $result . "\n" . ($a + 1) . ". Cabang " . $resultBranchYetReport[$a];
            $result .= "\n Total pendapatan : Rp. " . number_format($pendapatan->total, 0, "", ".") .
                "\n Total transaksi : " . number_format($pendapatan->total_transaksi, 0, "", ".") .
                "\n Total barang terjual : " . number_format($barjual->total_barang_terjual, 0, "", ".") . "\n";
        }

        $rekap_barang = $this->salesModel->getRekapSalesMonth($bln, $tahun);

        for ($i = 0; $i < sizeof($rekap_barang); $i++) {
            for ($j = 0; $j < $rekap_barang[$i]["jumlah"]; $j++) {
                $totbar[] = $rekap_barang[$i]["nama"];
            }
        }

        $c = array_count_values($totbar);
        arsort($c);
        foreach ($c as $x => $c) {
            $q[] = "$x = $c\n";
        }

        $result .= "\n\nRekapitulasi transaksi\n\n";
        for ($y = 0; $y < sizeof($q); $y++) {
            $result = $result . ($y + 1) . ". " . $q[$y];
        }

        $rekap_waktu = $this->salesModel->getRekapTimeMonth($bln);
        for ($l = 0; $l < sizeof($rekap_waktu); $l++) {
            $getJam = explode(":", $rekap_waktu[$l]["time"]);
            $getJamreal = trim($getJam[0]);
            $arJam[] = $getJamreal;
        }

        $v = array_count_values($arJam);
        arsort($v);
        foreach ($v as $k => $v) {
            $p = $k + 1;
            $w[] = "$k-" . $p . ": $v transaksi\n";
        }

        $result .= "\n\nRekapitulasi waktu transaksi \n";
        for ($y = 0; $y < sizeof($w); $y++) {
            $result = $result . "Jam " . $w[$y];
        }
        return $result;
    }

    function getShopBranchExceptShopIda($shopIds)
    {
        $shops = (array)$this->shopModel->getShopsByUser();
        $data = [];
        for ($i = 0; $i < sizeof($shops); $i++) {
            $isfound = true;
            for ($a = 0; $a < sizeof($shopIds); $a++) {
                if ($shopIds[$a]["shopId"] == $shops[$i]->shopId) {
                    $isfound = false;
                }
            }
            if ($isfound == false) {
                array_push($data, $shops[$i]->branch);
            }
        }
        return $data;
    }

    function splitLongText($longString)
    {
        $output = [];
        $words = explode(chr(10), $longString);
        $maxLineLength = 4096;

        $currentLength = 0;
        $index = 0;

        foreach ($words as $word) {
            // +1 because the word will receive back the space in the end that it loses in explode()
            $wordLength = strlen($word) + 1;

            if (($currentLength + $wordLength) <= $maxLineLength) {
                $output[$index] .= $word . "\n";
                $currentLength += $wordLength;
            } else {
                $index += 1;
                $currentLength = $wordLength;
                $output[$index] = $word;
            }
        }
        return $output;
    }

    function mingguan()
    {
        error_reporting(E_WARNING);
        $mgu = $_GET['hari'];
        $mguskrng = date('Y-m-d', strtotime($mgu));
        $mgulalu = date('Y-m-d', strtotime('-6 day', strtotime($mgu)));
        $result = "[REKAP MINGGUAN  - TANGGAL " . $mgulalu . " s/d " . $mguskrng . "\n]";

        $hasil = $this->salesModel->getShopWeek($mgulalu, $mguskrng);
        $barjual = $this->salesModel->getTotalSalesWeek($mgulalu, $mguskrng);
        $totransaksi = $hasil->total_transaksi;
        $total = $hasil->total;
        $barang_terjual = $barjual->total_barang_terjual;
        $resultShopIdHasReport = (array)$this->salesModel->getShopHasReportw($mgulalu, $mguskrng);
        $resultBranchYetReport = $this->getShopBranchExceptShopIda($resultShopIdHasReport);

        $result .= "\nTotal pendapatan semua cabang : Rp. " . number_format($total, 0, "", ".") .
            "\nTotal transaksi semua cabang : " . number_format($totransaksi, 0, "", ".") .
            "\nTotal barang terjual setiap cabang : " . $barang_terjual . "\n\n";

        $id = array_reverse($resultShopIdHasReport);
        asort($id);
        foreach ($id as $di => $id) {
            $k[] = $id;
        }

        for ($a = 0; $a < sizeof($resultBranchYetReport); $a++) {
            $pendapatan = $this->salesModel->getShopWeek($mgulalu, $mguskrng, $k[$a]["shopId"]);
            $barjual = $this->salesModel->getTotalSalesWeek($mgulalu, $mguskrng, $k[$a]["shopId"]);

            $result = $result . "\n" . ($a + 1) . ". Cabang " . $resultBranchYetReport[$a];
            $result .= "\n Total pendapatan : Rp. " . number_format($pendapatan->total, 0, "", ".") .
                "\n Total transaksi : " . number_format($pendapatan->total_transaksi, 0, "", ".") .
                "\n Total barang terjual : " . $barjual->total_barang_terjual . "\n";
        }

        $rekap_barang = $this->salesModel->getRekapSalesWeek($mgulalu, $mguskrng);
        for ($i = 0; $i < sizeof($rekap_barang); $i++) {
            for ($j = 0; $j < $rekap_barang[$i]["jumlah"]; $j++) {
                $totbar[] = $rekap_barang[$i]["nama"];
            }
        }

        $c = array_count_values($totbar);
        arsort($c);
        foreach ($c as $x => $c) {
            $q[] = "$x = $c\n";
        }

        $result .= "\n\nRekapitulasi transaksi\n\n";
        for ($y = 0; $y < sizeof($q); $y++) {
            $result = $result . ($y + 1) . ". " . $q[$y];
        }

        $rekap_waktu = $this->salesModel->getRekapTimeWeek($mgulalu, $mguskrng);
        for ($l = 0; $l < sizeof($rekap_waktu); $l++) {
            $getJam = explode(":", $rekap_waktu[$l]["time"]);
            $getJamreal = trim($getJam[0]);
            $arJam[] = $getJamreal;
        }

        $v = array_count_values($arJam);
        arsort($v);
        foreach ($v as $k => $v) {
            $p = $k + 1;
            $w[] = "$k-" . $p . ": $v transaksi\n";
        }

        $result .= "Rekapitulasi waktu transaksi \n";
        for ($y = 0; $y < sizeof($w); $y++) {
            $result = $result . "Jam " . $w[$y];
        }
        $this->sendMessageTelegram($result, null);
        return $this->response->publish(null, "Success Report Mingguan", self::SUCCESS);
    }


    function changeMonthNumberToName($monthNumber)
    {
        switch ($monthNumber) {
            case 1:
                return "Januari";
            case 2:
                return "Februari";
            case 3:
                return "Maret";
            case 4:
                return "April";
            case 5:
                return "Mei";
            case 6:
                return "Juni";
            case 7:
                return "Juli";
            case 8:
                return "Agustus";
            case 9:
                return "September";
            case 10:
                return "Oktober";
            case 11:
                return "November";
            case 12:
                return "Desember";
        }
        return "";
    }

    function changeCabangIDToName($id)
    {
        switch ($id) {
            case 9:
                return "kauman";
            case 10:
                return "sigura";
            case 11:
                return "sawojajar";
            case 12:
                return "pakis";
            case 13:
                return "lawang";
            case 14:
                return "gondanglegi";
            case 15:
                return "probolinggo";
            case 16:
                return "pasuruan";
            case 17:
                return "jember";
        }
        return "";
    }

    public
    function reportDay($request, $response, $args)
    {
        $cabang = $_GET['cabang'];
        $tgl = date("Y-m-d", strtotime($_GET['tanggal']));
        if ($cabang == null || $tgl == null) {
            echo "cabang atau tanggal tidak boleh kosong";
            die;
        }

        $rekap_barang = $this->salesModel->getSalesDayByID($tgl, $cabang);
//        var_dump($rekap_barang);die;
        for ($i = 0; $i < sizeof($rekap_barang); $i++) {
            for ($j = 0; $j < $rekap_barang[$i]["jumlah"]; $j++) {
                $totbar[] = $rekap_barang[$i]["nama"] . "=" . $rekap_barang[$i]["price"];
            }
        }

        $c = array_count_values($totbar);
        arsort($c);
        foreach ($c as $x => $c) {
            $a = explode('=', $x);
            $type = $this->salesModel->getTypeSales($a[0]);
            $t = $type->description;
            $h = $type->price;
            $q[] = "$x ($t) = $c\n";
            $nabarang[] = $x;
            $tybarang[] = $t;
            $jumbarang[] = $c;
            $harga[] = $h;
        }

        $pendapatan = $this->salesModel->getReportByDay($tgl, $cabang);
        $total = $this->salesModel->getTotalReportByDay($tgl, $cabang);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A2', 'LAPORAN REKAPITULASI HARIAN UNTUK CABANG ' . strtoupper($this->changeCabangIDToName($cabang)));
        $sheet->setCellValue('A4', 'Tanggal : ' . $tgl);
        $sheet->setCellValue('A6', 'TOTAL PENDAPATAN :' . number_format($pendapatan->total, 0, ',', '.'));
        $sheet->setCellValue('A7', 'TOTAL TRANSAKSI : ' . $pendapatan->total_transaksi);
        $sheet->setCellValue('A8', 'TOTAL BARANG : ' . $total->total_barang_terjual);

        $sheet->setCellValue('A11', 'No');
        $sheet->setCellValue('B11', 'Nama Barang');
        $sheet->setCellValue('C11', 'Tipe Barang');
        $sheet->setCellValue('D11', 'Jumlah');
        $sheet->setCellValue('E11', 'Harga');
        $sheet->setCellValue('F11', 'Total');

        for ($n = 0; $n < sizeof($q); $n++) {
            $barang = explode("=", $nabarang[$n]);
            $harga = number_format(($barang[1]), 0, ',', '.');
            $total_harga = number_format($barang[1] * $jumbarang[$n], 0, ',', '.');
//            echo "\n".$barang[1]." Menjadi => ".$harga."\n";
//            var_dump($harga);die;

            $sheet->setCellValue('A' . (12 + $n), ($n + 1));
            $sheet->setCellValue('B' . (12 + $n), $barang[0]);
            $sheet->setCellValue('C' . (12 + $n), $tybarang[$n]);
            $sheet->setCellValue('D' . (12 + $n), $jumbarang[$n]);
            $sheet->setCellValue('E' . (12 + $n), " " . $harga);
            $sheet->setCellValue('F' . (12 + $n), " " . $total_harga);
        }

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(11);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(7);
        $writer = new Xlsx($spreadsheet);
        $writer->save('storage/rekap/cabang/' . $this->changeCabangIDToName($cabang) . '/harian/report_harian_' . $this->changeCabangIDToName($cabang) . '_' . $tgl . '.xlsx');

        echo '/home/develo16/public_html/api_pos/storage/rekap/cabang/' . $this->changeCabangIDToName($cabang) . '/harian/report_harian_' . $this->changeCabangIDToName($cabang) . '_' . $tgl . '.xlsx';
    }

    public function reportDayAll($request = null, $response = null, $args = null)
    {
        $tgl = date("Y-m-d", strtotime($_GET['tanggal']));

        if ($tgl == null) {
            echo "tanggal tidak boleh kosong";
            die;
        }

        $rekap_barang = $this->salesModel->getSalesDayByID($tgl);
//        var_dump($rekap_barang);die;
        for ($i = 0; $i < sizeof($rekap_barang); $i++) {
            for ($j = 0; $j < $rekap_barang[$i]["jumlah"]; $j++) {
                $totbar[] = $rekap_barang[$i]["nama"] . "=" . $rekap_barang[$i]["price"];
            }
        }

        $c = array_count_values($totbar);
        arsort($c);
        foreach ($c as $x => $c) {
            $a = explode('=', $x);
            $type = $this->salesModel->getTypeSales($a[0]);
            $t = $type->description;
            $h = $type->price;
            $q[] = "$x ($t) = $c\n";
            $nabarang[] = $x;
            $tybarang[] = $t;
            $jumbarang[] = $c;
            $harga[] = $h;
        }

        $pendapatan = $this->salesModel->getReportByDay($tgl);
        $total = $this->salesModel->getTotalReportByDay($tgl);

        $resultShopIdHasReport = (array)$this->salesModel->getShopHasReport($tgl);
        $resultBranchYetReport = $this->getShopBranchExceptShopIda($resultShopIdHasReport);

        $id = array_reverse($resultShopIdHasReport);
        asort($id);
        foreach ($id as $di => $id) {
            $k[] = $id;
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A2', 'LAPORAN REKAPITULASI HARIAN SEMUA CABANG ');
        $sheet->setCellValue('A4', 'Tanggal : ' . $tgl);
        $sheet->setCellValue('A6', 'TOTAL PENDAPATAN :' . number_format($pendapatan->total, 0, ',', '.'));
        $sheet->setCellValue('A7', 'TOTAL TRANSAKSI : ' . $pendapatan->total_transaksi);
        $sheet->setCellValue('A8', 'TOTAL BARANG : ' . $total->total_barang_terjual);

        $sheet->setCellValue('A11', 'No');
        $sheet->setCellValue('B11', 'Nama Barang');
        $sheet->setCellValue('C11', 'Tipe Barang');
        $sheet->setCellValue('D11', 'Jumlah');
        $sheet->setCellValue('E11', 'Harga');
        $sheet->setCellValue('F11', 'Total');

        for ($n = 0; $n < sizeof($q); $n++) {
            $barang = explode("=", $nabarang[$n]);
            $harga = number_format(($barang[1]), 0, ',', '.');
            $total_harga = number_format($barang[1] * $jumbarang[$n], 0, ',', '.');

            $sheet->setCellValue('A' . (12 + $n), ($n + 1));
            $sheet->setCellValue('B' . (12 + $n), $barang[0]);
            $sheet->setCellValue('C' . (12 + $n), $tybarang[$n]);
            $sheet->setCellValue('D' . (12 + $n), $jumbarang[$n]);
            $sheet->setCellValue('E' . (12 + $n), " " . $harga);
            $sheet->setCellValue('F' . (12 + $n), " " . $total_harga);
        }

        $cell = 'H';
        for ($m = 0; $m < sizeof($resultBranchYetReport); $m++) {
            $sheet->setCellValue($cell . '11', $resultBranchYetReport[$m]);
            $sheet->getColumnDimension($cell)->setWidth(9.5);

            for ($l = 0; $l < sizeof($q); $l++) {
                $nama_barang = $sheet->getCell('B' . (12 + $l))->getValue();
                $barang_cabang = $this->salesModel->getTotalByCabangd($nama_barang, $tgl, $k[$m]["shopId"]);
//            var_dump($k[$m]["shopId"]);die;
                $sheet->setCellValue($cell . (12 + $l), ($barang_cabang->total != null ? $barang_cabang->total : '0'));
            }

            $cell++;
        }

        $sheet->getColumnDimension('A')->setWidth(5);
        $sheet->getColumnDimension('B')->setWidth(30);
        $sheet->getColumnDimension('C')->setWidth(11);
        $sheet->getColumnDimension('D')->setWidth(7);
        $writer = new Xlsx($spreadsheet);

        $writer->save('storage/rekap/all/harian/report_harian_all_' . $tgl . '.xlsx');

        return ("/home/develo16/public_html/api_pos/storage/rekap/all/harian/report_harian_all_" . $tgl . ".xlsx");

    }

    public
    function reportBulanan($request, $response, $args)
    {
        $cabang = $_GET['cabang'];
        $bln = $_GET['bulan'];
        $thn = $_GET['tahun'];
        if ($cabang == null || $bln == null || $thn == null) {
            echo "cabang, bulan, atau tahun tidak boleh kosong";
            die;
        }
//        var_dump("ASEEK INI REPORT");

//        echo "INI REPORT UNTUK ID CABANG :".$this->changeCabangIDToName($cabang).
//            "\nbulan : ".$this->changeMonthNumberToName($bln).
//            "\ntahun : ".$thn;
        $rekap_barang = $this->salesModel->getSalesMonthByID($bln, $thn, $cabang);
//        var_dump($rekap_barang);die;
        for ($i = 0; $i < sizeof($rekap_barang); $i++) {
            for ($j = 0; $j < $rekap_barang[$i]["jumlah"]; $j++) {
                $totbar[] = $rekap_barang[$i]["nama"] . "=" . $rekap_barang[$i]["price"];
            }
        }

        $c = array_count_values($totbar);
        arsort($c);
        foreach ($c as $x => $c) {
            $a = explode('=', $x);
            $type = $this->salesModel->getTypeSales($a[0]);
            $t = $type->description;
            $h = $type->price;
            $q[] = "$x ($t) = $c\n";
            $nabarang[] = $x;
            $tybarang[] = $t;
            $jumbarang[] = $c;
            $harga[] = $h;
        }

        $pendapatan = $this->salesModel->getReportByMonth($bln, $thn, $cabang);
        $total = $this->salesModel->getTotalReportByMonth($bln, $thn, $cabang);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A2', 'LAPORAN REKAPITULASI BULANAN UNTUK CABANG ' . strtoupper($this->changeCabangIDToName($cabang)));
        $sheet->setCellValue('A4', 'Bulan : ' . $this->changeMonthNumberToName($bln));
        $sheet->setCellValue('A5', 'tahun : ' . $thn);
        $sheet->setCellValue('A7', 'TOTAL PENDAPATAN :' . number_format($pendapatan->total, 0, ',', '.'));
        $sheet->setCellValue('A8', 'TOTAL TRANSAKSI : ' . $pendapatan->total_transaksi);
        $sheet->setCellValue('A9', 'TOTAL BARANG : ' . $total->total_barang_terjual);

        $sheet->setCellValue('A12', 'No');
        $sheet->setCellValue('B12', 'Nama Barang');
        $sheet->setCellValue('C12', 'Tipe Barang');
        $sheet->setCellValue('D12', 'Jumlah');
        $sheet->setCellValue('E12', 'Harga');
        $sheet->setCellValue('F12', 'Total');

        for ($n = 0; $n < sizeof($q); $n++) {
            $barang = explode("=", $nabarang[$n]);
            $harga = number_format(($barang[1]), 0, ',', '.');
            $total_harga = number_format($barang[1] * $jumbarang[$n], 0, ',', '.');
            $sheet->setCellValue('A' . (13 + $n), ($n + 1));
            $sheet->setCellValue('B' . (13 + $n), $barang[0]);
            $sheet->setCellValue('C' . (13 + $n), $tybarang[$n]);
            $sheet->setCellValue('D' . (13 + $n), $jumbarang[$n]);
            $sheet->setCellValue('E' . (13 + $n), " " . $harga);
            $sheet->setCellValue('F' . (13 + $n), " " . $total_harga);
        }

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(11);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(7);
        $writer = new Xlsx($spreadsheet);
        $writer->save('storage/rekap/cabang/' . $this->changeCabangIDToName($cabang) . '/bulanan/report_bulanan_' . $this->changeCabangIDToName($cabang) . '_' . $bln . '_' . $thn . '.xlsx');

        echo '/home/develo16/public_html/api_pos/storage/rekap/cabang/' . $this->changeCabangIDToName($cabang) . '/bulanan/report_bulanan_' . $this->changeCabangIDToName($cabang) . '_' . $bln . '_' . $thn . '.xlsx';
    }

    public
    function reportMonthAll($request = null, $response = null, $args = null)
    {
        $bln = $_GET['bulan'];
        $thn = $_GET['tahun'];
        if ($bln == null || $thn == null) {
            echo "bulan, atau tahun tidak boleh kosong";
            die;
        }

//                var_dump("ASEEK INI REPORT");
        $rekap_barang = $this->salesModel->getSalesMonthByID($bln, $thn);
//        var_dump($rekap_barang);die;
        for ($i = 0; $i < sizeof($rekap_barang); $i++) {
            for ($j = 0; $j < $rekap_barang[$i]["jumlah"]; $j++) {
                $totbar[] = $rekap_barang[$i]["nama"] . "=" . $rekap_barang[$i]["price"];
            }
        }

        $c = array_count_values($totbar);
        arsort($c);
        foreach ($c as $x => $c) {
            $a = explode('=', $x);
            $type = $this->salesModel->getTypeSales($a[0]);
            $t = $type->description;
            $h = $type->price;
            $q[] = "$x ($t) = $c\n";
            $nabarang[] = $x;
            $tybarang[] = $t;
            $jumbarang[] = $c;
            $harga[] = $h;
        }

        $pendapatan = $this->salesModel->getReportByMonth($bln, $thn);
        $total = $this->salesModel->getTotalReportByMonth($bln, $thn);

        $resultShopIdHasReport = (array)$this->salesModel->getShopHasReportm($bln);
        $resultBranchYetReport = $this->getShopBranchExceptShopIda($resultShopIdHasReport);

        $id = array_reverse($resultShopIdHasReport);
        asort($id);
        foreach ($id as $di => $id) {
            $k[] = $id;
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A2', 'LAPORAN REKAPITULASI BULANAN SEMUA CABANG ');
        $sheet->setCellValue('A4', 'Bulan : ' . $this->changeMonthNumberToName($bln));
        $sheet->setCellValue('A5', 'tahun : ' . $thn);
        $sheet->setCellValue('A7', 'TOTAL PENDAPATAN :' . number_format($pendapatan->total, 0, ',', '.'));
        $sheet->setCellValue('A8', 'TOTAL TRANSAKSI : ' . $pendapatan->total_transaksi);
        $sheet->setCellValue('A9', 'TOTAL BARANG : ' . $total->total_barang_terjual);

        $sheet->setCellValue('A12', 'No');
        $sheet->setCellValue('B12', 'Nama Barang');
        $sheet->setCellValue('C12', 'Tipe Barang');
        $sheet->setCellValue('D12', 'Jumlah');
        $sheet->setCellValue('E12', 'Harga');
        $sheet->setCellValue('F12', 'Total');

        for ($n = 0; $n < sizeof($q); $n++) {
            $barang = explode("=", $nabarang[$n]);
            $harga = number_format(($barang[1]), 0, ',', '.');
            $total_harga = number_format($barang[1] * $jumbarang[$n], 0, ',', '.');

            $sheet->setCellValue('A' . (13 + $n), ($n + 1));
            $sheet->setCellValue('B' . (13 + $n), $barang[0]);
            $sheet->setCellValue('C' . (13 + $n), $tybarang[$n]);
            $sheet->setCellValue('D' . (13 + $n), $jumbarang[$n]);
            $sheet->setCellValue('E' . (13 + $n), " " . $harga);
            $sheet->setCellValue('F' . (13 + $n), " " . $total_harga);

        }

        $cell = 'H';
        for ($m = 0; $m < sizeof($resultBranchYetReport); $m++) {
            $sheet->setCellValue($cell . '12', $resultBranchYetReport[$m]);
            $sheet->getColumnDimension($cell)->setWidth(9.5);

            for ($l = 0; $l < sizeof($q); $l++) {
                $nama_barang = $sheet->getCell('B' . (13 + $l))->getValue();
                $barang_cabang = $this->salesModel->getTotalByCabangm($nama_barang, $bln, $k[$m]["shopId"]);
//            var_dump($k[$m]["shopId"]);die;
                $sheet->setCellValue($cell . (13 + $l), ($barang_cabang->total != null ? $barang_cabang->total : '0'));
            }

            $cell++;
        }


        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(11);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(7);
        $writer = new Xlsx($spreadsheet);
        $writer->save('storage/rekap/all/bulanan/report_bulanan_all_' . $bln . '_' . $thn . '.xlsx');

        return '/home/develo16/public_html/api_pos/storage/rekap/all/bulanan/report_bulanan_all_' . $bln . '_' . $thn . '.xlsx';
    }

    public
    function reportWeek($requset, $response, $args)
    {

//            var_dump("ASEEK INI REPORT WEEK");
        $cabang = $_GET['cabang'];
        $mgu = $_GET['hari'];

        if ($cabang == null || $mgu == null) {
            echo "cabang atau hari tidak boleh kosong";
            die;
        }

        $mguskrng = date('Y-m-d', strtotime($mgu));
        $mgulalu = date('Y-m-d', strtotime('-6 day', strtotime($mgu)));
//        echo "Report untuk cabang ".$cabang." tanggal ".$mgulalu." Sampai ".$mguskrng;

        $rekap_barang = $this->salesModel->getSalesWeekByID($mgulalu, $mguskrng, $cabang);
        for ($i = 0; $i < sizeof($rekap_barang); $i++) {
            for ($j = 0; $j < $rekap_barang[$i]["jumlah"]; $j++) {
                $totbar[] = $rekap_barang[$i]["nama"] . "=" . $rekap_barang[$i]["price"];
            }
        }

        $c = array_count_values($totbar);
        arsort($c);
        foreach ($c as $x => $c) {
            $a = explode('=', $x);
            $type = $this->salesModel->getTypeSales($a[0]);
            $t = $type->description;
            $h = $type->price;
            $q[] = "$x ($t) = $c\n";
            $nabarang[] = $x;
            $tybarang[] = $t;
            $jumbarang[] = $c;
            $harga[] = $h;
        }

        $pendapatan = $this->salesModel->getReportByWeek($mgulalu, $mguskrng, $cabang);
        $total = $this->salesModel->getTotalReportByWeek($mgulalu, $mguskrng, $cabang);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A2', 'LAPORAN REKAPITULASI MINGGUAN UNTUK CABANG ' . strtoupper($this->changeCabangIDToName($cabang)));
        $sheet->setCellValue('A4', 'Tanggal ' . $mgulalu . " Sampai " . $mguskrng);
        $sheet->setCellValue('A6', 'TOTAL PENDAPATAN : ' . number_format($pendapatan->total, 0, ',', '.'));
        $sheet->setCellValue('A7', 'TOTAL TRANSAKSI : ' . $pendapatan->total_transaksi);
        $sheet->setCellValue('A8', 'TOTAL BARANG : ' . $total->total_barang_terjual);

        $sheet->setCellValue('A11', 'No');
        $sheet->setCellValue('B11', 'Nama Barang');
        $sheet->setCellValue('C11', 'Tipe Barang');
        $sheet->setCellValue('D11', 'Jumlah');
        $sheet->setCellValue('E11', 'Harga');
        $sheet->setCellValue('F11', 'Total');

        for ($n = 0; $n < sizeof($q); $n++) {
            $barang = explode("=", $nabarang[$n]);
            $harga = number_format(($barang[1]), 0, ',', '.');
            $total_harga = number_format($barang[1] * $jumbarang[$n], 0, ',', '.');

            $sheet->setCellValue('A' . (12 + $n), ($n + 1));
            $sheet->setCellValue('B' . (12 + $n), $barang[0]);
            $sheet->setCellValue('C' . (12 + $n), $tybarang[$n]);
            $sheet->setCellValue('D' . (12 + $n), $jumbarang[$n]);
            $sheet->setCellValue('E' . (12 + $n), " " . $harga);
            $sheet->setCellValue('F' . (12 + $n), " " . $total_harga);

        }

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(11);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(7);
        $writer = new Xlsx($spreadsheet);
        $writer->save('storage/rekap/cabang/' . $this->changeCabangIDToName($cabang) . '/mingguan/report_mingguan_' . $this->changeCabangIDToName($cabang) . '_' . $mgulalu . '_' . $mguskrng . '.xlsx');
        echo '/home/develo16/public_html/api_pos/storage/rekap/cabang/' . $this->changeCabangIDToName($cabang) . '/mingguan/report_mingguan_' . $this->changeCabangIDToName($cabang) . '_' . $mgulalu . '_' . $mguskrng . '.xlsx';
    }

    public
    function reportWeekAll($request, $response, $args)
    {
//        var_dump("ASEEK INI REPORT WEEK");
        $mgu = $_GET['hari'];

        if ($mgu == null) {
            echo "hari tidak boleh kosong";
            die;
        }

        $mguskrng = date('Y-m-d', strtotime($mgu));
        $mgulalu = date('Y-m-d', strtotime('-6 day', strtotime($mgu)));
//        echo "Report  tanggal ".$mgulalu." Sampai ".$mguskrng;

        $rekap_barang = $this->salesModel->getSalesWeekByID($mgulalu, $mguskrng);
        for ($i = 0; $i < sizeof($rekap_barang); $i++) {
            for ($j = 0; $j < $rekap_barang[$i]["jumlah"]; $j++) {
                $totbar[] = $rekap_barang[$i]["nama"] . "=" . $rekap_barang[$i]["price"];
            }
        }
        $c = array_count_values($totbar);
        arsort($c);
        foreach ($c as $x => $c) {
            $a = explode('=', $x);
            $type = $this->salesModel->getTypeSales($a[0]);
            $t = $type->description;
            $h = $type->price;
            $q[] = "$x ($t) = $c\n";
            $nabarang[] = $x;
            $tybarang[] = $t;
            $jumbarang[] = $c;
            $harga[] = $h;
        }

        $pendapatan = $this->salesModel->getReportByWeek($mgulalu, $mguskrng);
        $total = $this->salesModel->getTotalReportByWeek($mgulalu, $mguskrng);

        $resultShopIdHasReport = (array)$this->salesModel->getShopHasReportw($mgulalu, $mguskrng);
        $resultBranchYetReport = $this->getShopBranchExceptShopIda($resultShopIdHasReport);

        $id = array_reverse($resultShopIdHasReport);
        asort($id);
        foreach ($id as $di => $id) {
            $k[] = $id;
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A2', 'LAPORAN REKAPITULASI MINGGUAN SEMUA CABANG ');
        $sheet->setCellValue('A4', 'Tanggal ' . $mgulalu . " Sampai " . $mguskrng);
        $sheet->setCellValue('A6', 'TOTAL PENDAPATAN :' . number_format($pendapatan->total, 0, ',', '.'));
        $sheet->setCellValue('A7', 'TOTAL TRANSAKSI : ' . $pendapatan->total_transaksi);
        $sheet->setCellValue('A8', 'TOTAL BARANG : ' . $total->total_barang_terjual);

        $sheet->setCellValue('A11', 'No');
        $sheet->setCellValue('B11', 'Nama Barang');
        $sheet->setCellValue('C11', 'Tipe Barang');
        $sheet->setCellValue('D11', 'Jumlah');
        $sheet->setCellValue('E11', 'Harga');
        $sheet->setCellValue('F11', 'Total');

        for ($n = 0; $n < sizeof($q); $n++) {
            $barang = explode("=", $nabarang[$n]);
            $harga = number_format(($barang[1]), 0, ',', '.');
            $total_harga = number_format($barang[1] * $jumbarang[$n], 0, ',', '.');

            $sheet->setCellValue('A' . (12 + $n), ($n + 1));
            $sheet->setCellValue('B' . (12 + $n), $barang[0]);
            $sheet->setCellValue('C' . (12 + $n), $tybarang[$n]);
            $sheet->setCellValue('D' . (12 + $n), $jumbarang[$n]);
            $sheet->setCellValue('E' . (12 + $n), " " . $harga);
            $sheet->setCellValue('F' . (12 + $n), " " . $total_harga);

        }

        $cell = 'H';
        for ($m = 0; $m < sizeof($resultBranchYetReport); $m++) {
            $sheet->setCellValue($cell . '11', $resultBranchYetReport[$m]);
            $sheet->getColumnDimension($cell)->setWidth(9.5);

            for ($l = 0; $l < sizeof($q); $l++) {
                $nama_barang = $sheet->getCell('B' . (12 + $l))->getValue();
                $barang_cabang = $this->salesModel->getTotalByCabangw($nama_barang, $mgulalu, $mguskrng, $k[$m]["shopId"]);
//            var_dump($k[$m]["shopId"]);die;
                $sheet->setCellValue($cell . (12 + $l), ($barang_cabang->total != null ? $barang_cabang->total : '0'));
            }

            $cell++;
        }


        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(11);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(7);
        $writer = new Xlsx($spreadsheet);
        $writer->save('storage/rekap/all/mingguan/report_mingguan_' . $mgulalu . '_' . $mguskrng . '.xlsx');

        echo '/home/develo16/public_html/api_pos/storage/rekap/all/mingguan/report_mingguan_' . $mgulalu . '_' . $mguskrng . '.xlsx';

    }

    public function labaHarian($request, $response, $args)
    {
        error_reporting(E_WARNING);
        $tgl = date("Y-m-d", strtotime($_GET['tanggal']));
        if ($tgl == null) {
            echo "tanggal tidak boleh kosong";
            die;
        }
        $rekap_barang = $this->salesModel->getSalesDayByID($tgl);
        $resultShopIdHasReport = (array)$this->salesModel->getShopHasReport($tgl);

        $spreadsheet = $this->generateExcelLaba($rekap_barang, $resultShopIdHasReport);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'HITUNG LABA HARIAN VAUZA TAMMA HIJAB');
        $sheet->setCellValue('A2', 'TANGGAL : ' . $tgl);
        $writer = new Xlsx($spreadsheet);
        $writer->save('storage/bati/harian/laba_' . $tgl . '.xlsx');

        return '/home/develo16/public_html/api_pos/storage/bati/harian/laba_' . $tgl . '.xlsx';
    }


    public function labaMingguan($request, $response, $args)
    {
        error_reporting(E_WARNING);
        $mgu = $_GET['hari'];

        if ($mgu == null) {
            echo "hari tidak boleh kosong";
            die;
        }

        $mguskrng = date('Y-m-d', strtotime($mgu));
        $mgulalu = date('Y-m-d', strtotime('-9 day', strtotime($mgu)));

        $rekap_barang = $this->salesModel->getSalesWeekByID($mgulalu, $mguskrng);

        $resultShopIdHasReport = (array)$this->salesModel->getShopHasReportw($mgulalu, $mguskrng);
        $resultBranchYetReport = $this->getShopBranchExceptShopIda($resultShopIdHasReport);

        $spreadsheet = $this->generateExcelLaba($rekap_barang, $resultShopIdHasReport);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'HITUNG LABA CUSTOM VAUZA TAMMA HIJAB');
        $sheet->setCellValue('A2', 'TANGGAL : ' . $mgulalu . ' - ' . $mguskrng);

        $writer = new Xlsx($spreadsheet);
        $writer->save('storage/bati/mingguan/laba_' . $mgulalu . '_' . $mguskrng . '.xlsx');
        return '/home/develo16/public_html/api_pos/storage/bati/mingguan/laba_' . $mgulalu . '_' . $mguskrng . '.xlsx';

    }

    public function labaBulanan($request, $response, $args)
    {
        error_reporting(E_WARNING);
        $bln = $_GET['bulan'];
        $thn = $_GET['tahun'];
        if ($bln == null || $thn == null) {
            echo "bulan, atau tahun tidak boleh kosong";
            die;
        }

        $rekap_barang = $this->salesModel->getSalesMonthByID($bln, $thn);
        for ($i = 0; $i < sizeof($rekap_barang); $i++) {
            for ($j = 0; $j < $rekap_barang[$i]["jumlah"]; $j++) {
                $totbar[] = $rekap_barang[$i]["nama"] . "=" . $rekap_barang[$i]["price"];
            }
        }

        $c = array_count_values($totbar);
        arsort($c);
        foreach ($c as $x => $c) {
            $a = explode('=', $x);
            $type = $this->salesModel->getTypeSales($a[0]);
            $t = $type->description;
            $h = $type->price;
            $q[] = "$x ($t) = $c\n";
            $nabarang[] = $x;
            $tybarang[] = $t;
            $jumbarang[] = $c;
            $harga[] = $h;
        }

        $resultShopIdHasReport = (array)$this->salesModel->getShopHasReportm($bln);
        $resultBranchYetReport = $this->getShopBranchExceptShopIda($resultShopIdHasReport);

        $id = array_reverse($resultShopIdHasReport);
        asort($id);
        foreach ($id as $di => $id) {
            $k[] = $id;
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A2', 'HITUNG LABA BULANAN');
        $sheet->setCellValue('A4', 'Bulan : ' . $bln . " Tahun " . $thn);


        $sheet->setCellValue('A10', 'No');
        $sheet->setCellValue('B10', 'Nama Barang');
        $sheet->setCellValue('C10', 'Jenis');
        $sheet->setCellValue('D10', 'Harga Beli');
        $sheet->setCellValue('E10', 'Harga Jual');
        $sheet->setCellValue('F10', 'Jumlah Barang');
        $sheet->setCellValue('G10', 'Total Jual');
        $sheet->setCellValue('H10', 'Laba');

        for ($n = 0; $n < sizeof($q); $n++) {
            $barang = explode("=", $nabarang[$n]);
            $beli = $this->salesModel->getBuy($barang);
            $harga = number_format(($barang[1]), 0, ',', '.');
            $jual = number_format(($barang[1] - 2000), 0, ',', '.');
//            $total_harga = number_format($barang[1]*$jumbarang[$n] , 0, ',', '.');

            $sheet->setCellValue('A' . (11 + $n), ($n + 1));
            $sheet->setCellValue('B' . (11 + $n), $barang[0]);
            $sheet->setCellValue('C' . (11 + $n), $tybarang[$n]);
            $sheet->setCellValue('D' . (11 + $n), $beli->harga);
            $sheet->setCellValue('E' . (11 + $n), $barang[1]);
            $sheet->setCellValue('F' . (11 + $n), $jumbarang[$n]);
            $sheet->setCellValue('G' . (11 + $n), "=E" . (11 + $n) . "*F" . (11 + $n));
            $sheet->setCellValue('H' . (11 + $n), "=(E" . (11 + $n) . "-D" . (11 + $n) . ")*F" . (11 + $n));
        }
//        $sheet->setCellValue('A5','Total Pendapatan : '.$pendapatan->total);
        $sheet->setCellValue('A6', '="Total Penjualan : "&SUM(G11:G' . ($n + 10) . ')');
        $sheet->setCellValue('A7', '="Total Pembelian : "&SUMPRODUCT(D11:D' . ($n + 10) . ',F11:F' . ($n + 10) . ')');
        $sheet->setCellValue('A8', '="Total Laba : "&SUM(H11:H' . ($n + 10) . ')');

//        $cell = 'H';
//        for ($m = 0;$m<sizeof($resultBranchYetReport);$m++){
//            $sheet->setCellValue($cell.'12',$resultBranchYetReport[$m]);
//            $sheet->getColumnDimension($cell)->setWidth(9.5);
//
//            for ($l = 0;$l<sizeof($q);$l++){
//                $nama_barang = $sheet->getCell('B'.(13+$l))->getValue();
//                $barang_cabang = $this->salesModel->getTotalByCabangm($nama_barang,$bln,$k[$m]["shopId"]);
//                $sheet->setCellValue($cell.(13+$l),($barang_cabang->total != null?$barang_cabang->total:'0'));
//            }
//
//            $cell++;
//        }

        $sheet->getColumnDimension('A')->setWidth(5);
        $sheet->getColumnDimension('B')->setWidth(30);
        $sheet->getColumnDimension('C')->setWidth(11);
        $sheet->getColumnDimension('F')->setWidth(13);
        $writer = new Xlsx($spreadsheet);
        $writer->save('storage/bati/bulanan/laba_' . $bln . '_' . $thn . '.xlsx');

//        echo 'https://utter.co.id/vatcash/storage/rekap/all/mingguan/report_mingguan_'.$mgulalu.'_'.$mguskrng.'.xlsx';
        echo "YEE BERHASIL";
    }

    public function getBuyFromCommodityId($commodityId, $hargaAllBeli)
    {
        for ($i = 0; $i < sizeof($hargaAllBeli); $i++) {
            if ($commodityId == $hargaAllBeli[$i]["commodity_id"]) {
                return $hargaAllBeli[$i]["price"];
            }
        }
        return 0;
    }

    function getTotalBarangCabang($namaBarang, $shopId, $dataBarangAll)
    {
        foreach ($dataBarangAll as $x => $c) {
            $a = explode('=', $x);
            if ($a[3] == $shopId && $a[0] == $namaBarang) {
                return $c;
            }
        }
        return 0;
    }

    function getJumlahBarang($namaBarang, $shopId, $dataAll)
    {
        $jumlahBarang = 0;
        foreach ($dataAll as $data) {

            if ($data["nama"] == $namaBarang && $data["shop_id"] == $shopId) {
                $jumlahBarang = $jumlahBarang + $data["jumlah"];
            }
        }
        return $jumlahBarang;
    }

    function getHargaJualBarang($namaBarang, $shopId, $dataAll)
    {
        $hargaBarang = 0;
        foreach ($dataAll as $data) {
            if ($data["nama"] == $namaBarang && $data["shop_id"] == $shopId) {
                return $data["price"];
            }
        }
        return $hargaBarang;
    }

    function getAbjadFromASCII($valueAscii) {
        if ($valueAscii > 90) {
            return "A".chr(($valueAscii%90) + 64);
         }
        return chr($valueAscii);
    }

    function getASCIIFromAbjadCell($abjadCell) {
        $splitAbjad = str_split($abjadCell);
        if (sizeof($splitAbjad) > 1) {
            $ascii = 0;
            for ($a = 0; $a < sizeof($splitAbjad); $a++) {
                if ($a == 0) {
                    $ascii = 90;
                } else {
                    $ascii = $ascii + (ord($splitAbjad[$a])-64);
                }
            }
            return $ascii;
        }
        return ord($abjadCell);
    }

    public function generateExcelLaba($rekap_barang, $resultShopIdHasReport){
        $resultBranchYetReport = $this->getShopBranchExceptShopIda($resultShopIdHasReport);
        $id = array_reverse($resultShopIdHasReport);
        asort($id);
        foreach ($id as $di => $id) {
            $k[] = $id;
        }

        $newTotalBarang = [];
        foreach ($rekap_barang as $barang) {
            $barangObj = $barang;
            $barangObj["jumlah"] = [];
            $barangObj["harga"] = [];
            unset($barangObj["price"]);
            foreach ($k as $shopBranch) {
                $barangObj["jumlah"][$shopBranch["shopId"]] = 0;
                $barangObj["harga"][$shopBranch["shopId"]] = 0;
                $isFound = false;
                foreach ($newTotalBarang as $barangIsFound) {
                    if ($barangIsFound["nama"] == $barang["nama"]) {
                        if (isset($barangIsFound["jumlah"][$shopBranch["shopId"]])) {
                            $isFound = true;
                            break;
                        }
                    }
                }
                if ($isFound == false) {
                    $barangObj["jumlah"][$shopBranch["shopId"]] = $this->getJumlahBarang($barang["nama"], $shopBranch["shopId"], $rekap_barang);
                    $barangObj["harga"][$shopBranch["shopId"]] = $this->getHargaJualBarang($barang["nama"], $shopBranch["shopId"], $rekap_barang);
                }
            }
            $isFound = false;
            foreach ($newTotalBarang as $barangIsFound) {
                if ($barangIsFound["nama"] == $barang["nama"]) {
                    $isFound = true;
                }
            }
            if ($isFound == false) {
                $newTotalBarang[] = $barangObj;
            }
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A10', 'No');
        $sheet->setCellValue('B10', 'Nama Barang');
        $sheet->setCellValue('C10', 'Jenis');

        for ($w = 0; $w < sizeof($newTotalBarang); $w++) {
            $sheet->setCellValue('B' . (11 + $w), $newTotalBarang[$w]["nama"]);
        }

        $hargaBeliAll = $this->salesModel->getBuy();

        $cell = 'I';
        $sheet->setCellValue($cell . '10', 'Harga Beli');
        $sheet->getColumnDimension($cell)->setWidth(18);
        for ($n = 0; $n < sizeof($newTotalBarang); $n++) {
            $hargaBeli = $this->getBuyFromCommodityId($newTotalBarang[$n]["id"], $hargaBeliAll);
            $sheet->setCellValue($cell . (11 + $n), ($hargaBeli));
        }
        $cell++;

        for ($m = 0; $m < sizeof($resultBranchYetReport); $m++) {
            $sheet->setCellValue($cell . '10', 'Harga Jual ' . $resultBranchYetReport[$m]);
            $sheet->getColumnDimension($cell)->setWidth(18);
            for ($n = 0; $n < sizeof($newTotalBarang); $n++) {
                $sheet->setCellValue($cell . (11 + $n), $newTotalBarang[$n]["harga"][$k[$m]["shopId"]]);
            }
            $cell++;
        }
        for ($b = 0; $b < sizeof($resultBranchYetReport); $b++) {
            $sheet->setCellValue($cell . '10', 'Jumlah Terjual ' . $resultBranchYetReport[$b]);
            $sheet->getColumnDimension($cell)->setWidth(20);
            for ($l = 0; $l < sizeof($newTotalBarang); $l++) {
                $sheet->setCellValue($cell . (11 + $l), $newTotalBarang[$l]["jumlah"][$k[$b]["shopId"]]);
            }
            $cell++;
        }

        for ($b = 0; $b < sizeof($resultBranchYetReport); $b++) {
            $sheet->setCellValue($cell . '10', 'Jumlah Pendapatan ' . $resultBranchYetReport[$b]);
            $sheet->getColumnDimension($cell)->setWidth(20);
            for ($l = 0; $l < sizeof($newTotalBarang); $l++) {
                $asciiCell = $this->getASCIIFromAbjadCell($cell);
                $koordinateJumlahTerjual = $this->getAbjadFromASCII($asciiCell - sizeof($resultBranchYetReport));
                $koordinateHargaJual = $this->getAbjadFromASCII($asciiCell - sizeof($resultBranchYetReport) - sizeof($resultBranchYetReport));
                $sheet->setCellValue($cell . (11 + $l), '=' . $koordinateJumlahTerjual . (11 + $l) . '*' . $koordinateHargaJual . (11 + $l));
            }
            $cell++;
        }

        for ($b = 0; $b < sizeof($resultBranchYetReport); $b++) {
            $sheet->setCellValue($cell . '10', 'Laba ' . $resultBranchYetReport[$b]);
            $sheet->getColumnDimension($cell)->setWidth(20);
            for ($l = 0; $l < sizeof($newTotalBarang); $l++) {
                $asciiCell = $this->getASCIIFromAbjadCell($cell);
                $koordinatePenjualanCabang = $this->getAbjadFromASCII($asciiCell - sizeof($resultBranchYetReport) );
                $koordinateJumlahTerjual = $this->getAbjadFromASCII($asciiCell - (sizeof($resultBranchYetReport) * 2) );
                $sheet->setCellValue($cell . (11 + $l), '=' . $koordinatePenjualanCabang . (11 + $l) . '-(' . $koordinateJumlahTerjual . (11 + $l) . '*I' . (11 + $l) . ')');
            }
            $cell++;
        }
        $cell = 'D';
        $sheet->setCellValue($cell . '10', 'Total Barang Terjual');
        $sheet->getColumnDimension($cell)->setWidth(20);
        for ($l = 0; $l < sizeof($newTotalBarang); $l++) {
            $asciiCell = $this->getASCIIFromAbjadCell($cell);
            $koordinateStartTotalTerjual = $this->getAbjadFromASCII($asciiCell + 6 + (sizeof($resultBranchYetReport) * 1));
            $koordinateEndTotalTerjual = $this->getAbjadFromASCII($asciiCell + 5 + (sizeof($resultBranchYetReport) * 2));
            $sheet->setCellValue($cell . (11 + $l), '=SUM(' . $koordinateStartTotalTerjual . (11 + $l) . ':' . $koordinateEndTotalTerjual . (11 + $l) . ')');
        }
        $cell++;

        $sheet->setCellValue($cell . '10', 'Total Penjualan');
        $sheet->getColumnDimension($cell)->setWidth(20);
        for ($l = 0; $l < sizeof($newTotalBarang); $l++) {
            $asciiCell = $this->getASCIIFromAbjadCell($cell);
            $koordinateStartPenjualan = $this->getAbjadFromASCII($asciiCell + 5 + (sizeof($resultBranchYetReport) * 2));
            $koordinateEndPenjualan = $this->getAbjadFromASCII($asciiCell + 4 + (sizeof($resultBranchYetReport) * 3));
            $sheet->setCellValue($cell . (11 + $l), '=SUM(' . $koordinateStartPenjualan . (11 + $l) . ':' . $koordinateEndPenjualan . (11 + $l) . ')');
        }
        $cell++;
        $sheet->setCellValue($cell . '10', 'Total Pembelian');
        $sheet->getColumnDimension($cell)->setWidth(20);
        for ($l = 0; $l < sizeof($newTotalBarang); $l++) {
            $sheet->setCellValue($cell . (11 + $l), '=D' . (11 + $l) . '*I' . (11 + $l) . '');
        }
        $cell++;
        $sheet->setCellValue($cell . '10', 'Total Laba');
        $sheet->getColumnDimension($cell)->setWidth(20);
        for ($l = 0; $l < sizeof($newTotalBarang); $l++) {
            $sheet->setCellValue($cell . (11 + $l), '=E' . (11 + $l) . '-F' . (11 + $l));
        }

        for ($n = 0; $n < sizeof($newTotalBarang); $n++) {
            $sheet->setCellValue('A' . (11 + $n), ($n + 1));
            $sheet->setCellValue('C' . (11 + $n), $newTotalBarang[$n]["jenis"]);
        }

        $sheet->setCellValue('A4', '="Total Penjualan : "&SUM(E11:E' . ($n + 10) . ')');
        $sheet->setCellValue('A5', '="Total Pembelian : "&SUM(F11:F' . ($n + 10) . ')');
        $sheet->setCellValue('A6', '="Total Barang Terjual : "&SUM(D11:D' . ($n + 10) . ')');
        $sheet->setCellValue('A7', '="Total Laba : "&SUM(G11:G' . ($n + 10) . ')');

        $sheet->getColumnDimension('A')->setWidth(5);
        $sheet->getColumnDimension('B')->setWidth(30);
        $sheet->getColumnDimension('C')->setWidth(11);

        return $spreadsheet;
    }
}

?>
