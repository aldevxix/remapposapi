<?php

use Pos\Config\Constants;
use Pos\Helpers\Location;
use Pos\Middlewares\Filter;
use Pos\Models\UserModel;
use Pos\Systems\Request;
use Pos\Systems\Response;

$this->post("/register", User::class . ":register");
$this->post("/login", User::class . ":login");
$this->post("/checkToken", User::class . ":checkToken");

class User implements Constants
{

    public function __construct()
    {
        $this->userModel = new UserModel();
        $this->request = new Request();
        $this->response = new Response();
    }

    public function register($request, $response, $args)
    {
        $this->request->parse($request);

        $error = $this->validationRegister($this->request);

        if (sizeof($error) > 0) {
            return $this->publishErrorRegister($error);
        }

        $hashPassword = hash('sha256', $this->request->get("password"));
        $token = md5(uniqid(rand(), true));
        $dateTime = date("Y-m-d") . " " . date("H:i:s");
        $params = [
            ":photo" => "",
            ":cover" => "",
            ":username" => $this->request->get("username"),
            ":fullname" => $this->request->get("fullname"),
            ":email" => $this->request->get("email"),
            ":kind" => 1,
            ":verified" => 1,
            ":agreement_tos" => 1,
        ];
        $user = ( array )$this->userModel->insertUser($params, $hashPassword, $token, $dateTime);
        return $this->getUserFromToken($token);
    }

    public function validationRegister($request)
    {

        $error = [];
        if (empty($request->get("fullname"))) {
            $error['fullname'] = 'Fullname still empty';
        }
        if (empty($request->get("username"))) {
            $error['username'] = 'Username still empty';
        }
        if (empty($request->get("email"))) {
            $error['email'] = 'Email still empty';
        }
        if (!filter_var($request->get("email"), FILTER_VALIDATE_EMAIL)) {
            $error['email'] = 'Email format is not valid';
        }
        if (empty($request->get("password"))) {
            $error['password'] = 'Password still empty';
        }
        if (strlen($request->get("password")) < 6) {
            $error['password'] = 'Size character at least 6';
        }

        // check is email found in database ?
        if (sizeof($error) == 0) {
            $user = $this->userModel->checkEmail($request->get("email"));
            if (isset($user->email)) {
                $error['email'] = 'Email have been used';
            } else {
                $user2 = $this->userModel->checkUsername($request->get("username"));
                if (isset($user2->username)) {
                    $error['username'] = 'Username have been used';
                }
            }
        }

        return $error;
    }

    public function login($request, $response, $args)
    {
        $this->request->parse($request);
        $hashPassword = hash('sha256', $this->request->get("password"));
        $token = md5(uniqid(rand(), true));
        $user = $this->userModel->login($this->request->get("email"), $hashPassword, $token);
        if (isset($user->userId)) {
            return $this->response->publish($user, "Success Login", self::SUCCESS);
        } else {
            return $this->response->publish(null, "User Not Found", self::NOT_FOUND);
        }
    }

    public function checkToken($request, $response, $args)
    {
        $this->request->parse($request);
        return $this->getUserFromToken($this->request->get("token"));
    }

    public function getUserFromToken($token)
    {
        $user = $this->userModel->checkToken($token);
        if (isset($user->userId)) {
            return $this->response->publish($user, "Success Login", self::SUCCESS);
        } else {
            return $this->response->publish(null, "User Not Found", self::NOT_FOUND);
        }
    }

    public function publishErrorRegister($error)
    {
        return $this->response->publish((object) $error, "Failed Register", self::NOT_FOUND);
    }
}

?>
