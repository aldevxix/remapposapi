<?php
    /**
     * Shop Controllers
     * @author Robet Atiq Maulana Rifqi
     * JUNI 2019
     */

    use Pos\Config\Constants;
    use Pos\Helpers\Location;
    use Pos\Middlewares\Filter;
    use Pos\Models\ShopModel;
    use Pos\Systems\Request;
    use Pos\Systems\Response;

    $this->get( "/{shopId}", Shop::class . ":detail" );
    $this->get( '/user/{userId}', Shop::class . ":getUserShop" );
    $this->get( "/search/{keyword}", Shop::class . ":search" );
    $this->post( "/filter", Shop::class . ":getByPlace" );
    $this->post( "/create", Shop::class . ":create" );
    $this->post( "/edit", Shop::class . ":edit" );
    $this->post( "/close", Shop::class . ":close" );
    $this->post( "/buy", Shop::class . ":buy" );

    class Shop implements Constants {

        public function __construct() {

            // Initialize
            $this->location = new Location();
            $this->shopModel = new ShopModel();
            $this->request = new Request();
            $this->response = new Response();
        }

        /**
         * Insert Shop Location
         */
        private function _insertLocation () {
            if ( 
                $this->request->get("placeId") == "" &&
                $this->request->get("latitude") != "" &&
                $this->request->get("longitude") != ""
            ) {
                
                // Get Place Detail
                $place = $this->location->getPlaceDetail($this->request->get());

                // Insert Location
                $location = $this->shopModel->insertLocation([
                    ":name"         => $place->name,
                    ":longitude"    => $place->longitude,
                    ":latitude"     => $place->latitude,
                    ":city"         => $place->city,
                    ":province"     => $place->province,
                    ":state"        => $place->state
                ]);

                // Set Place Id
                $this->request->set( "placeId", $location->placeId );
            }
        }

        /**
         * Shop Action
         */
        private function _actionShop ( $action, $message) {

            // get place id
            $placeId = $this->request->get("placeId") != "" ? $this->request->get("placeId") : 0;

            // Action Shop
            $shop = $this->shopModel->actionShop([
                ":shopId"   => $this->request->get("shopId"),
                ":userId"   => $this->request->get("userId"),
                ":name"     => $this->request->get("name"),
                ":address"  => $this->request->get("address"),
                ":placeId"  => $placeId,
                ":action"   => $action
            ]);
            
            // Get Code
            $data =  (json_decode(json_encode($shop), True));
            $code = $data[0]["codes"];
            for ($i = 0; $i < sizeof($data); $i++){ 
                unset($data[$i]["codes"]);
            }
           
            

            switch ( $code ) {

                case self::FORBIDEN: return $this->response->publish ( null, "Forbiden access", $code );

                case self::NOT_FOUND: return $this->response->publish ( null, "Shop Not Found", $code );

                case self::SUCCESS: return $this->response->publish ( $data, $message, $code );
            }
        }

        /**
         * Get Shop Detail
         * 
         * @method GET
         * @link shop/{shopId}
         * @return object
         */
        public function detail ( $request, $response, $args ) {

            // Set Shop Id
            $this->request->set("shopId", $args['shopId']);

            // Get Shop Detail
            return $this->_actionShop(self::ACTION_READ, "Success Get Shop");
        }

        /**
         * Get User Shop
         * 
         * @method GET
         * @link shop/user/{userId}
         * @return object
         */
        public function getUserShop ( $request, $response, $args ) {

            // Set User Id
            $this->request->set("userId", $args['userId']);

            // Get Shop Detail
            $shops =  (array) $this->shopModel->getShopsByUser();

            if ( $shops[0] == null ) return $this->response->publish( null, "Shops Not Found", self::NOT_FOUND );

            // Return
            return $this->response->publish( $shops, "Success Get Shop User", self::SUCCESS );
        }

        /**
         * Create Shop
         * 
         * @method POST
         * @link shop/create
         * @return object
         */
        public function create ( $request, $response, $args ) {

            // Parsing Request Params
            $this->request->parse ( $request );

            // Insert Location
            $this->_insertLocation();

            // Create Shop
            return $this->_actionShop(self::ACTION_CREATE, "Success Create Shop");
        }

        /**
         * Edit Shop
         * 
         * @method POST
         * @link shop/edit
         * @return object
         */
        public function edit ( $request, $response, $args ) {

            // Parsing Request Params
            $this->request->parse ( $request );

            // Insert Location
            $this->_insertLocation();

            // Update Data
            return $this->_actionShop(self::ACTION_UPDATE, "Success Update Shop");
        }

        /**
         * Delete Shop
         * 
         * @method POST
         * @link shop/close
         * @return object
         */
        public function close ( $request, $response, $args ) {

            // Parsing request params
            $this->request->parse($request);

            // Closing Shop
            return $this->_actionShop(self::ACTION_DELETE, "Success Delete Shop");
        }

        /**
         * Search Shop By Place
         * 
         * @method GET
         * @link shop/{range}/{keyword}
         * @return object
         */
        public function getByPlace ( $request, $response, $args ) {

            // Parsing request params
            $this->request->parse ( $request );

            // Search Shop
            $shop = ( array ) $this->shopModel->getByPlace([ 
                ":range" => $this->request->get("range"), 
                ":keyword" => $this->request->get("keyword") 
            ]);

            if ( $shop[0] == null ) return $this->response->publish( null, "Shops Not Found", self::NOT_FOUND );

            // Return
            return $this->response->publish( $shop, "Success Search Shop", self::SUCCESS );
        }

        /**
         * Search Shop
         * 
         * @method GET
         * @link shop/search/{keyword}
         * @return object
         */
        public function search ( $request, $response, $args ) {
            
            // Search Shop
            $shop = ( array ) $this->shopModel->search([ ":keyword" => $args['keyword'] ]);

            if ( $shop[0] == null ) return $this->response->publish ( null, "Shops Not Found", self::NOT_FOUND );

            // Return
            return $this->response->publish ( $shop, "Success Filter Shops", self::SUCCESS );
        }
    }
?>