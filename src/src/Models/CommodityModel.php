<?php
/**
 * Commodity Model
 * @author Robet Atiq Maulana Rifqi
 * JUNI 2019
 */

namespace Pos\Models;

use Pos\Models\Adapters\Commodities;
use Pos\Systems\Connection;

class CommodityModel
{

    public function __construct()
    {

        $this->db = new Connection();
    }

    /**
     * main action commodity
     *
     * @param array
     * @return object
     */
    public function actionCommodity($params)
    {
        $this->db->query(
            "CALL `ActionCommodities`(:commodityId,:userId,:date,:time,:shopId,:name,:description,:price,:images,:minAmount,:discount,:warehouse,:display,:quantity,:purpose,:action)",
            $params);
        $result = $this->db->fetchAll();
        return $result;
    }

    /**
     * Get Commodity Detail
     *
     * @param array
     * @return object
     */
    public function getDetail($params)
    {

        $this->db->query("SELECT commodity_id AS commodityId, name, description, price, images FROM `pmr_t_post_commodities` WHERE `commodity_id` = :commodityId", $params);

        return $this->db->fetch();
    }

    /**
     * Get Commodity Detail By Shop
     *
     * @param array
     * @return object
     */
    public function getDetailByShop($params)
    {

        $this->db->query("
            SELECT c.commodity_id AS commodityId, c.name, c.description, c.images, c.price, s.discount_percent AS discountPercent, s.discount_potongan AS discountPotongan, s.stock_product AS stockProduct, s.date_time AS dateTime, s.shop_id AS shopId
            FROM pmr_t_post_commodities c
            INNER JOIN pmr_t_post_commodities_shop s
            ON c.commodity_id = s.commodity_id
            WHERE s.shop_id = :shopId
            AND s.commodity_id = :commodityId", $params);

        return $this->db->fetch();
    }

    /**
     * Get Commodity By Shop
     *
     * @param array
     * @return object
     */
    public function getAllGeneralCommodity()
    {
        $this->db->query("
            SELECT c.commodity_id AS commodityId, c.name, c.description, c.images, c.price, c.images 
            FROM pmr_t_post_commodities c");

        return $this->db->fetchAll();
    }

    /**
     * Get Commodity By Shop
     *
     * @param array
     * @return object
     */
    public function getByShop($params)
    {
        $this->db->query("
            SELECT c.commodity_id AS commodityId, c.name, c.description, c.images, c.price, s.discount_percent AS discountPercent, s.discount_potongan AS discountPotongan, s.stock_product AS stockProduct, s.date_time AS dateTime, s.shop_id AS shopId
            FROM pmr_t_post_commodities c
            INNER JOIN pmr_t_post_commodities_shop s
            ON c.commodity_id = s.commodity_id
            WHERE s.shop_id = :shopId", $params);

        return $this->db->fetchAll();
    }


    /**
     * Search Commodity
     *
     * @param array
     * @return object
     */
    public function search($params)
    {
        $this->db->query("CALL `SearchCommodity`(:keyword, :shopId)", $params);

        return $this->db->fetchAll(new Commodities());
    }

    /**
     * Deleting Compositions
     *
     * @param array
     */
    public function deleteComposition($params)
    {

        $this->db->query("DELETE FROM `pmr_t_post_commodities_compositions` WHERE `commodity_id` = :commodityId", $params);
    }

    /**
     * Insert Compositions
     *
     * @param array
     */
    public function insertComposition($params)
    {

        $this->db->query(
            "INSERT INTO `pmr_t_post_commodities_compositions`(`commodity_id`, `name`, `amount`, `unit`) VALUES (:commodityId, :name, :amount, :unit)",
            $params);
    }

    /**
     * Get Composition
     *
     * @param array
     * @return object
     */
    public function getCompositions($params)
    {

        $this->db->query("SELECT * FROM `pmr_v_commodities_compositions` WHERE `commodityId` = :commodityId", $params);

        return $this->db->fetchAll();
    }

    /**
     * Update stock
     *
     * @param array
     */
    public function updateStock($params)
    {

        $this->db->query("UPDATE `pmr_t_post_commodities_stock` SET `display` = :display WHERE `commodity_id` = :commodityId", $params);
    }

    public function insertCommodity($params) {

        $query = "
            INSERT INTO `pmr_t_posts`(`secondary_id`, `room_id`, `user_id`, `operator_id`, `date`, `time`, `viewed`, `kind`)
            VALUES(0, 0, :userId, 0, :date, :time, 0, 8);

            INSERT INTO `pmr_t_post_commodities`(`post_id`, `name`, `description`, `price`, `images`)
            VALUES ( (SELECT MAX(post_id) FROM `pmr_t_posts`), :name, :description, :price, :images);
        ";
        $this->db->query($query, $params);
        return $this->db->lastInsertId();
    }

    public function insertCommodityShop($params) {
//          ini untuk query setiap shop
//        $stringQuery  = "";
//        for ($i = 1; $i < 374; $i++) {
//            $stringQuery = $stringQuery."INSERT INTO `pmr_t_post_commodities_shop`(`shop_id`, `commodity_id`, `discount_percent`, `discount_potongan`, `stock_product`, `date_time`)
//            VALUES(16, ".$i.", 0, 0, 1000, '2019-08-01 07:00:00');";
//        }
//        $this->db->query($stringQuery);

        $query = "
            INSERT INTO `pmr_t_post_commodities_shop`(`shop_id`, `commodity_id`, `discount_percent`, `discount_potongan`, `stock_product`, `date_time`)
            VALUES(:shopId, :commodityId, :discountPercent, :discountPotongan, :stockProduct, :dateTime);
        ";
        $this->db->query($query, $params);
        return $this->db->lastInsertId();
    }

    public function updateCommodity($params) {

        $query = "
                UPDATE `pmr_t_post_commodities` 
                SET 
                `name` = :name, 
                `description` = :description, 
                `price` = :price, 
                `images` = :images
                WHERE `commodity_id` = :commodityId;          
        ";
        return $this->db->query($query, $params);
    }

    public function updateCommodityByShop($params) {

        $query = "
                UPDATE `pmr_t_post_commodities_shop` 
                SET 
                `stock_product` = :stockProduct, 
                `discount_percent` = :discountPercent, 
                `discount_potongan` = :discountPotongan, 
                `date_time` = :dateTime
                WHERE `commodity_id` = :commodityId
                AND `shop_id` = :shopId;          
        ";
        return $this->db->query($query, $params);
    }

    public function deleteCommodity($params) {

        $query = "
                 DELETE FROM `pmr_t_post_commodities` WHERE `commodity_id` = :commodityId;         
        ";
        return $this->db->query($query, $params);
    }

    public function deleteCommodityByShop($params) {

        $query = "
                 DELETE FROM `pmr_t_post_commodities_shop` WHERE `commodity_id` = :commodityId AND `shop_id` = :shopId;         
        ";
        return $this->db->query($query, $params);
    }

    public function getIdByName($name, $type, $price) {
        $query = "
                 SELECT commodity_id FROM `pmr_t_post_commodities` WHERE LOWER(`name`) = '".$name."' ORDER BY commodity_id DESC LIMIT 1 ;         
        ";
        $this->db->query($query);
        $result = $this->db->fetch();
        if (isset($result->commodity_id) == true) {
            return $result->commodity_id;
        } else {
            $commodityId = $this->insertCommodity([
                ":userId" => 5,
                ":name" => $name,
                ":description" => $type,
                ":price" => $price,
                ":date" => date("Y-m-d"),
                ":time" => date("H:i:s"),
                ":images" => "",
            ]);
            return $commodityId;
        }
    }

    /**
     * Update stock
     *
     * @param array
     */
    public function updateNewStock($stock, $commodityId, $shopId)
    {
        return $this->db->query("UPDATE `pmr_t_post_commodities_shop` SET `stock_product` = ".$stock." WHERE `commodity_id` = ".$commodityId. " AND `shop_id` = $shopId");
    }

    public function insertOrUpdateCommodityShop($commodityId, $shopId, $stok, $discountPercent, $discountPotongan) {
        $query = "
                 SELECT commodity_id, stock_product FROM `pmr_t_post_commodities_shop` WHERE `commodity_id` = ".$commodityId." AND `shop_id` = ".$shopId." ORDER BY commodity_id DESC LIMIT 1 ;         
        ";
        $this->db->query($query);
        $result = $this->db->fetch();
        if (isset($result->commodity_id) == true) {
            if ($result->stock_product != $stok) {
                $result = $this->updateNewStock($stok, $commodityId, $shopId);
            }
            return $result->commodity_id;
        } else {
            $commodityIdShop = $this->insertCommodityShop([
                ":shopId" => $shopId,
                ":commodityId" => $commodityId,
                ":discountPercent" => $discountPercent,
                ":discountPotongan" => $discountPotongan,
                ":dateTime" => date("Y-m-d") . " " . date("H:i:s"),
                ":stockProduct" => $stok,
            ]);
            return $commodityIdShop;
        }
    }
}
