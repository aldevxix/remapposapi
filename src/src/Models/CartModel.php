<?php
    /**
     * Cart Model
     * @author Robet Atiq Maulana Rifqi
     * JUL 2019
     */

    namespace Pos\Models;

    use Pos\Systems\Connection;

    class CartModel {

        public function __construct () {

            $this->db = new Connection();
        }

        /**
         * Get Single Cart
         *
         * @param array
         * @return object
         */
        public function get ( $params ) {

            $this->db->query (
                 "SELECT `cart_id` AS cartId, `user_id` AS userId, `total`, `status` FROM `pmr_t_shop_cart` WHERE `cart_id` = :cartId",
                 $params );
            
            return $this->db->fetch();
        }

        /**
         * Get User Cart
         *
         * @param array
         * @return object
         */
        public function getUserCart ( $params ) {

            $this->db->query ( "CALL `GetUserCart`(:userId, :status)", $params );

            return $this->db->fetchAll();
        }

        /**
         * Main cart action
         *
         * @param array
         * @return object
         */
        public function actionCart ( $params ) {

            $this->db->query ( "CALL `ActionCart`(:cartId, :userId, :total, :status, :action)", $params);

            return $this->db->fetch();
        }

        /**
         * Delete Cart Item
         * 
         * @param array
         */
        public function deleteItem ( $params ) {

            $this->db->query ( "DELETE FROM `pmr_t_shop_cart_detail` WHERE `cart_id` = :cartId", $params );
        }

        /**
         * Add Cart Item
         * 
         * @param array
         */
        public function addItem ( $params ) {

            $this->db->query (
                "INSERT INTO `pmr_t_shop_cart_detail`(`cart_id`,`commodity_id`, `amount`, `sub_total`) VALUES (:cartId, :commodityId,:amount, :subTotal)",
                $params );
        }

        /**
         * Get Cart Items
         *
         * @param array
         * @return object
         */
        public function getItems ( $params ) {

            $this->db->query ( "SELECT * FROM `pmr_v_shop_cart` WHERE `cartId` = :cartId", $params );

            return $this->db->fetchAll();
        }
    }