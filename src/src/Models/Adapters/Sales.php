<?php


namespace Pos\Models\Adapters;

use Pos\Helpers\Image;


class Sales
{

    public function __invoke($data)
    {

        if (is_null($data) || sizeof($data) == 0) {
            return null;
        }
        $image = new Image();

        // Define Default Model
        $model =  [
            "salesId"       => "",
            "commodityId"   => "",
            "name"          => "",
            "images"        => "",
            "amount"        => "",
            "price"         => "",
            "oldPrice"      => "",
            "discount"      => ""
        ];

        // merging data
        $sales = ( object ) array_merge((array) $model, (array) $data);

        // Set images
        $sales->images = $sales->images != "" ? $image->get("commodity", $sales->images) : null;

        return $sales;
    }
}