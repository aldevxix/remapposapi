<?php
    /**
     * Commodity Adapter
     * @author Robet Atiq Maulana Rifqi
     * JUNI 2019
     */

    namespace Pos\Models\Adapters;

    class Commodities {

        // Invoking data
        public function __invoke ( $data ) {

            // Define default model
            $model = [
                "commodityId"   => "",
                "postId"        => "",
                "shopId"        => "",
                "name"          => "",
                "description"   => "",
                "price"         => "",
                "images"        => "",
                "minAmount"     => "",
                "discount"      => "",
                "warehouse"     => "",
                "display"       => "",
                "stock"         => ""
            ];

            // Merging data
            $this->commodity = ( object ) array_merge ( ( array ) $model, ( array ) $data );

//            $this->_setDiscount();
//
//            $this->_setStock();

            // Return 
            return $this->commodity;
        }

        private function _setDiscount () {

            // Get discount
            $discount = [
                "minAmount"     => $this->commodity->minAmount,
                "discount"      => $this->commodity->discount,
                "percentage"    => ($this->commodity->discount / $this->commodity->price * 100) . "%"
            ];

            // Unseting data
            unset ( $this->commodity->minAmount );

            // Setting discount
            $this->commodity->discount = $discount;
        }

        private function _setStock () {

            // Get Stock
            $stock = [
                "display"       => $this->commodity->display,
                "warehouse"     => $this->commodity->warehouse
            ];

            // Unsetting data
            unset ( $this->commodity->display );
            unset ( $this->commodity->warehouse );

            // Setting data
            $this->commodity->stock = $stock;
        }
    }