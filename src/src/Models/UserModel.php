<?php
/**
 * Shop Models
 * @author Robet Atiq Maulana Rifqi
 * JUNI 2019
 */

namespace Pos\Models;

use Pos\Systems\Connection;

class UserModel
{

    public function __construct()
    {
        $this->db = new Connection();
    }

    public function insertUser($params, $passwrod, $token, $datetime)
    {
        $query = "
            INSERT INTO pmr_t_users (photo, cover, username, fullname, email, kind, verified, agreement_tos) 
            VALUES (:photo, :cover, :username, :fullname, :email, :kind, :verified, :agreement_tos);

            INSERT INTO pmr_t_user_secret (user_id, password, token, registered_date, signal_id, v_code) 
            VALUES ((SELECT MAX(pmr_t_users.user_id) FROM `pmr_t_users`), '" . $passwrod . "','" . $token . "','" . $datetime . "'," . "'',''" . ");
        ";
        $this->db->query($query, $params);

        return $this->db->lastInsertId();
    }

    public function login($email, $password, $token)
    {
        $query = "
            SELECT p.user_id AS userId, p.username, p.fullname, p.email, p.photo, p.cover, p.username, p.fullname, p.email, p.kind, p.verified, p.agreement_tos, k.token 
           FROM pmr_t_users p INNER JOIN pmr_t_user_secret k ON p.user_id = k.user_id 
           WHERE p.email = '" . $email . "' AND k.password = '" . $password . "'";
        $this->db->query($query);

        $user = $this->db->fetch();

        if (isset($user->userId)) {
            $query = "UPDATE `pmr_t_user_secret` SET `token` = '" . $token . "' WHERE `user_id` = " . $user->userId;
            $this->db->query($query);
            $user->token = $token;
        }
        return $user;
    }

    public function checkToken($token)
    {
        $query = "SELECT p.user_id AS userId, p.username, p.fullname, p.email, p.photo, p.cover, p.username, p.fullname, p.email, p.kind, p.verified, p.agreement_tos, k.token 
           FROM pmr_t_users p INNER JOIN pmr_t_user_secret k ON p.user_id = k.user_id 
           WHERE k.token = '" . $token . "'";

        $this->db->query($query);

        return $this->db->fetch();
    }

    public function checkEmail($email)
    {
        $query = "SELECT * FROM pmr_t_users p WHERE email = '" . $email . "'";

        $this->db->query($query);

        return $this->db->fetch();
    }

    public function checkUsername($userName)
    {
        $query = "SELECT * FROM pmr_t_users p WHERE username = '" . $userName . "'";

        $this->db->query($query);

        return $this->db->fetch();
    }
}

?>
