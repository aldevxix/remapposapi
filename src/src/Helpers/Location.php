<?php
    /**
     * Location Helper
     * @author Robet Atiq Maulana Rifqi
     * JUNI 2019
     */

    namespace Pos\Helpers;
    
    use Pos\Config\Constants;

    class Location implements Constants {

        /**
         * Get Place Detail
         * 
         * @param object
         * @return object
         */
        public function getPlaceDetail ( $data ) {

            // set Data
            $data = ( object ) [
                "name"      => "",
                "longitude" => $data->longitude,
                "latitude"  => $ata->latitude,
                "city"      => "",
                "address"   => "",
                "province"  => "",
                "state"     => ""
            ];

            // Get Coordinates
            $coordinates = $data->latitude . "," . $data->longitude;

            // Get maps url
            $url = self::MAPS_URL . "?latlng=$coordinates&sensor=" . self::MAPS_SENSOR . "&key=" . self::MAPS_KEY;

            // Get location content
            $content = file_get_contents ( $url );

            // decoding location
            $location = json_decode ( $content );

            // Set Result
            $result = $location->status != "ZERO_RESULTS" ? $location->result[0] : false;

            // Get Location Detail
            if ( $result == false ) $data->address = "No Where";
            else $data = $this->_detail($data, $location);

            // Return
            return $data;
        }

        /**
         * Get Location Detail
         * 
         * @param object
         * @param int|null
         * 
         * @return object
         */
        private function _detail ( $data, $location, $i = 0 ) {

            // Get Components
            $components = $location->address_components;

            // Get Value
            $shortValue = $components->short_name;
            $longValue = $components->long_name;
            $type = $components->types[0];

            // Parsing data
            switch ( $type ) {

                case "street_number": $data->address = "No. $shortValue"; break;

                case "route": $data->address = "$shortValue $data->address"; break;

                case "administrative_area_level_4": $data->district = "$longValue, "; break;

                case "administrative_area_level_3": $data->district .= $longValue; break;

                case "administrative_area_level_2": $data->city = $longValue; break;

                case "administrative_area_level_1": $data->province = $longValue; break;

                case "country": $data->state = $longValue; break;
            }

            // Make Loop
            if ( $i < sizeof ( $data->address_components ) - 1 ) return $this->_detail($data, $location, $i + 1);

            // Return 
            return $data;
        }
    }
?>