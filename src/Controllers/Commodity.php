<?php
/**
 * Commodity Controller
 * @author Robet Atiq Maulana Rifqi
 * JUNI 2019
 */

use Pos\Config\Constants;
use Pos\Helpers\Image;
//    use Pos\Middlewares\Filter;
use Pos\Models\CommodityModel;
use Pos\Models\ShopModel;
use Pos\Systems\Request;
use Pos\Systems\Response;

$this->get("/{commodityId}", Commodity::class . ":detail");
$this->get("/shop/{shopId}", Commodity::class . ":getCommodities");
$this->get("/{commodityId}/shop/{shopId}", Commodity::class . ":getDetailCommoditiesByShop");
$this->post("/general", Commodity::class . ":getAllCommodities");
$this->post("/allShop", Commodity::class . ":getAllCommoditiesShop");
$this->post("/search", Commodity::class . ":search");
$this->post("/add", Commodity::class . ":add");
$this->post("/shop/add", Commodity::class . ":addShopCommodity");
$this->post("/edit", Commodity::class . ":edit");
$this->post("/edit/shop", Commodity::class . ":editShopCommodity");
$this->post("/delete", Commodity::class . ":delete");
$this->post("/delete/shop", Commodity::class . ":deleteShopCommodity");
$this->post("/image/add", Commodity::class . ":addImage");
$this->post("/image/delete", Commodity::class . ":deleteImage");

/**
 * @property Response response
 * @property Image image
 * @property CommodityModel commodityModel
 * @property ShopModel shopModel
 * @property Request request
 */
class Commodity implements Constants
{

    public function __construct()
    {

        // Initialize
        $this->image = new Image();
        $this->commodityModel = new CommodityModel();
        $this->shopModel = new ShopModel();
        $this->request = new Request();
        $this->response = new Response();
    }

    // Main Compositions Action
    private function _actionCompositions($action, $i = 0)
    {

        // Deleting unsused composition
        if ($action == self::ACTION_DELETE || ($action == self::ACTION_UPDATE && $i == 0)) {
            $this->commodityModel->deleteComposition([":commodityId" => $this->request->get("commodityId")]);
        }

        if ($this->request->get("compositionName") != "") {


            if ($action != self::ACTION_DELETE) {

                // Inserting composition
                $this->commodityModel->insertComposition([
                    ":commodityId" => $this->request->get("commodityId"),
                    ":name" => $this->request->get("compositionName")[$i],
                    ":amount" => $this->request->get("compositionAmount")[$i],
                    ":unit" => $this->request->get("compositionUnit")[$i]
                ]);
            }

            // Make loop
            if ($i < sizeof($this->request->get("compositionName")) - 1) return $this->_actionCompositions($action, $i + 1);
        }

        // getting compositions data
        return ( array )$this->commodityModel->getCompositions([":commodityId" => $this->request->get("commodityId")]);
    }

    // Upload image
    private function _upload($i = 0)
    {

        if ($_FILES["image"] != null) {

            // Uploading image
            $upload = $this->image->upload($_FILES['image'], "commodity");

            if ($upload->code != self::SUCCESS) return $upload;

            // Set Images
            $this->request->set("images", $upload->path);
        }

        return ( object )["code" => self::SUCCESS];
    }

    // Main commodities action
    private function _actionCommodity($action)
    {

        // Set default result
        $result = ( object )[];

        // Uploading image
        $upload = $this->_upload();

        if ($upload->code != self::SUCCESS) return $upload;

        $commodity = $this->commodityModel->actionCommodity([
            ":commodityId" => $this->request->get("commodityId") != "" ? $this->request->get("commodityId") : 0,
            ":userId" => $this->request->get("userId"),
            ":date" => date("Y-m-d"),
            ":time" => date("H:i:s"),
            ":shopId" => $this->request->get("shopId") != "" ? $this->request->get("shopId") : 0,
            ":name" => $this->request->get("name"),
            ":description" => $this->request->get("description"),
            ":price" => $this->request->get("price"),
            ":images" => $this->request->get("images"),
            ":minAmount" => $this->request->get("minAmount"),
            ":discount" => $this->request->get("discount"),
            ":warehouse" => $this->request->get("warehouse"),
            ":display" => $this->request->get("display"),
            ":quantity" => $this->request->get("quantity"),
            ":purpose" => $this->request->get("purpose"),
            ":action" => $action
        ]);

        if ($commodity->codes == self::SUCCESS) {

            // Set Commodity Id
            if ($action != self::ACTION_DELETE) $this->request->set("commodityId", $commodity->commodityId);

            // Set Commodity Image
            $commodity->images = $this->image->get("commodity", $commodity->images);

            // Performing composition action
            $commodity->compositions = $this->_actionCompositions($action);
        }

        // Set result code
        $result->code = $commodity->codes;

        // unseting code
        unset ($commodity->codes);

        switch ($result->code) {

            case self::SUCCESS:
                $result->data = $commodity;
                break;

            case self::NOT_FOUND:
                $result->message = "Commodity not found";
                break;

            case self::FORBIDEN:
                $result->message = "Illegal Access!";
                break;
        }

        return $result;
    }

    /**
     * Get Commodity Detail
     *
     * @return object
     * @link commodity/{commodityId}
     * @method GET
     */
    public function detail($request, $response, $args)
    {

        // Get Detail
        $commodity = $this->commodityModel->getDetail([
            ":commodityId" => $args['commodityId']
        ]);

        if ($commodity->commodityId == "") return $this->response->publish(null, "Commodity Not Found", self::NOT_FOUND);


        // Return
        return $this->response->publish($commodity, "Success get Commodity", self::SUCCESS);
    }

    /**
     * Get Commodity By Shop
     *
     * @return object
     * @link commodity/shop/{shopId}
     * @method GET
     */
    public function getCommodities($request, $response, $args)
    {

        // Get Commodity List
        $commodity = ( array )$this->commodityModel->getByShop([":shopId" => $args['shopId']]);
        if ($commodity[0] == null) return $this->response->publish(null, "Commodities Not Found", self::NOT_FOUND);

        // Return
        return $this->response->publish($commodity, "Success Get Commodity", self::SUCCESS);
    }

    /**
     * Get Detail Commodity By Shop
     *
     * @return object
     * @link commodity/shop/{shopId}
     * @method GET
     */
    public function getDetailCommoditiesByShop($request, $response, $args)
    {

        // Get Commodity List
        $commodity = $this->commodityModel->getDetailByShop([
            ":shopId" => $args['shopId'],
            ":commodityId" => $args['commodityId']]);

        if ($commodity->commodityId == null) return $this->response->publish(null, "Commodities Not Found", self::NOT_FOUND);

        // Return
        return $this->response->publish($commodity, "Success Get Detail Commodity", self::SUCCESS);
    }

    /**
     * Get Commodity General
     *
     * @return object
     * @link commodity/shop/{shopId}
     * @method GET
     */
    public function getAllCommodities($request, $response, $args)
    {
        // getting all Commodity
        $commodity = ( array )$this->commodityModel->getAllGeneralCommodity();

        // Return
        return $this->response->publish($commodity , "Success Get Commodity", self::SUCCESS);
    }


    /**
     * Get Commodity By Shop
     *
     * @return object
     * @link commodity/shop/{shopId}
     * @method GET
     */
    public function getAllCommoditiesShop($request, $response, $args)
    {

        // Get Shop Detail
        $shops = (array)$this->shopModel->getShopsByUser();

        for ($i = 0; $i < sizeof($shops); $i++) {

            // getting user Commodity
            $commodity = ( array )$this->commodityModel->getByShop([":shopId" => $shops[$i]->shopId]);

            $shops[$i]->commodity = $commodity;
        }

        // Return
        return $this->response->publish($shops, "Success Get Commodity", self::SUCCESS);
    }

    /**
     * Add Product
     *
     * @return object
     * @link commodity/add
     * @method POST
     */
    public function addShopCommodity($request, $response, $args)
    {

        // Parsing request params
        $this->request->parse($request);

        $commodityShopId = $this->commodityModel->insertCommodityShop([
            ":shopId" => $this->request->get("shopId") != "" ? $this->request->get("shopId") : 0,
            ":commodityId" => $this->request->get("commodityId"),
            ":discountPercent" => $this->request->get("discountPercent"),
            ":discountPotongan" => $this->request->get("discountPotongan"),
            ":dateTime" => date("Y-m-d") . " " . date("H:i:s"),
            ":stockProduct" => $this->request->get("stockProduct"),
        ]);

        $data = [
            "shopId" => $this->request->get("shopId") != "" ? $this->request->get("shopId") : 0,
            "commodityId" => $this->request->get("commodityId"),
            "discountPercent" => $this->request->get("discountPercent"),
            "discountPotongan" => $this->request->get("discountPotongan"),
            "dateTime" => date("Y-m-d") . " " . date("H:i:s"),
            "stockProduct" => $this->request->get("stockProduct"),
        ];
        // Return
        return $this->response->publish($data, "Success Adding Commodity", self::SUCCESS);
    }

    /**
     * Add Product
     *
     * @return object
     * @link commodity/add
     * @method POST
     */
    public function add($request, $response, $args)
    {
        // Parsing request params
        $this->request->parse($request);

        $commodityId = $this->commodityModel->insertCommodity([
            ":userId" => $this->request->get("userId"),
            ":name" => $this->request->get("name"),
            ":description" => $this->request->get("description"),
            ":price" => $this->request->get("price"),
            ":date" => date("Y-m-d"),
            ":time" => date("H:i:s"),
            ":images" => $this->request->get("images"),
        ]);

        $data = [
            "userId" => $this->request->get("userId"),
            "commodityId" =>  $commodityId,
            "name" => $this->request->get("name"),
            "description" => $this->request->get("description"),
            "price" => $this->request->get("price"),
            "images" => $this->request->get("images"),
        ];
        // Return
        return $this->response->publish($data, "Success Adding Commodity", self::SUCCESS);
    }

    /**
     * Edit Product
     *
     * @return object
     * @link commodity/edit
     * @method POST
     */
    public function edit($request, $response, $args)
    {

        // Parsing request params
        $this->request->parse($request);

        $commodityId = $this->commodityModel->updateCommodity([
            ":commodityId" => $this->request->get("commodityId"),
            ":name" => $this->request->get("name"),
            ":description" => $this->request->get("description"),
            ":price" => $this->request->get("price"),
            ":images" => $this->request->get("images"),
        ]);

        $data = [
            "commodityId" => $this->request->get("commodityId"),
            "name" => $this->request->get("name"),
            "description" => $this->request->get("description"),
            "price" => $this->request->get("price"),
            "image" => $this->request->get("images"),
        ];

        // Return
        return $this->response->publish($data, "Success Update Commodity", self::SUCCESS);
    }

    /**
     * Edit Product
     *
     * @return object
     * @link commodity/edit
     * @method POST
     */
    public function editShopCommodity($request, $response, $args)
    {

        // Parsing request params
        $this->request->parse($request);

        $commodityId = $this->commodityModel->updateCommodityByShop([
            ":commodityId" => $this->request->get("commodityId"),
            ":shopId" => $this->request->get("shopId"),
            ":stockProduct" => $this->request->get("stockProduct"),
            ":discountPercent" => $this->request->get("discountPercent"),
            ":discountPotongan" => $this->request->get("discountPotongan"),
            ":dateTime" => date("Y-m-d") . " " . date("H:i:s"),
        ]);

        $data = [
            "commodityId" => $this->request->get("commodityId"),
            "shopId" => $this->request->get("shopId"),
            "stockProduct" => $this->request->get("stockProduct"),
            "discountPercent" => $this->request->get("discountPercent"),
            "discountPotongan" => $this->request->get("discountPotongan"),
            "dateTime" => date("Y-m-d") . " " . date("H:i:s"),
        ];

        // Return
        return $this->response->publish($data, "Success Update Commodity Shop", self::SUCCESS);
    }

    /**
     * Delete Product
     *
     * @return object
     * @link commodity/delete
     * @method POST
     */
    public function delete($request, $response, $args)
    {

        // Parsing request params
        $this->request->parse($request);

        $commodityId = $this->commodityModel->deleteCommodity([
            ":commodityId" => $this->request->get("commodityId")]);
        // Return
        return $this->response->publish(null, "Success Delete Commodity", self::SUCCESS);
    }


    /**
     * Delete Product in Spesific Shop
     *
     * @return object
     * @link commodity/delete
     * @method POST
     */
    public function deleteShopCommodity($request, $response, $args)
    {

        // Parsing request params
        $this->request->parse($request);

        $commodityId = $this->commodityModel->deleteCommodityByShop([
            ":commodityId" => $this->request->get("commodityId"),
            ":shopId" => $this->request->get("shopId")
            ]);
        // Return
        return $this->response->publish(null, "Success Delete Commodity", self::SUCCESS);
    }


    /**
     * Search Product
     *
     * @return object
     * @link commodity/search
     * @method POST
     */
    public function search($request, $response, $args)
    {

        // Parsing request params
        $this->request->parse($request);

        // Searching commodity
        $commodity = ( array )$this->commodityModel->search([
            ":keyword" => $this->request->get("keyword"),
            ":shopId" => $this->request->get("shopId")
        ]);

        if ($commodity[0] == null) return $this->response->publish(null, "Commodity Not Found", self::NOT_FOUND);

        // Return
        return $this->response->publish($commodity, "Success Search Commodity", self::SUCCESS);
    }
}

?>
