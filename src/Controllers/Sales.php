<?php

/**
 * Sales Controller
 * @author Robet Atiq Maulana Rifqi
 * Ags 2019
 */


namespace Pos\Controllers;

use Pos\Config\Constants;
use Pos\Models\CommodityModel;
use Pos\Models\SalesModel;
use Pos\Models\ShopModel;
use Pos\Systems\Request;
use Pos\Systems\Response;

$this->get("/{salesId}", Sales::class . ":detail");
$this->get("/pagination/shop/{shopId}", Sales::class . ":getDataPagination");
$this->post("/user", Sales::class . ":getUserSales");
$this->post("/shop", Sales::class . ":getShopSales");
$this->post("/add", Sales::class . ":add");
$this->post("/edit", Sales::class . ":edit");
$this->post("/remove", Sales::class . ":remove");
$this->post("/remove/item", Sales::class . ":removeItem");
$this->post("/buy", Sales::class . ":buy");

//$this->post("/allShops", Sales::class . ":getAllShopSalesItem");

class Sales implements Constants
{

    /**
     * @var Request
     */
    private $request;

    /**
     * @var Response
     */
    private $response;

    /**
     * @var SalesModel
     */
    private $salesModel;

    /**
     * @var ShopModel
     */
    private $shopModel;

    /**
     * @var CommodityModel
     */
    private $commodityModel;

    /**
     * Sales constructor.
     */
    public function __construct()
    {

        $this->commodityModel = new CommodityModel();
        $this->salesModel = new SalesModel();
        $this->shopModel = new ShopModel();
        $this->request = new Request();
        $this->response = new Response();
    }

    /**
     * Insert Sales Item Detail
     *
     * @param int $i
     * @return object|void
     */
    private function _insertItem($i = 0)
    {

        if ($this->request->get("commodityId") == 0) return;

        // Inserting item
        $this->salesModel->insertSalesItem([
            ":salesId" => $this->request->get("salesId"),
            ":secondaryTransId" => $this->request->get("secondaryId"),
            ":commodityId" => $this->request->get("commodityId")[$i],
            ":secondaryId" => $this->request->get("secondaryIdItem")[$i],
            ":amount" => $this->request->get("amount")[$i],
            ":price" => $this->request->get("price")[$i],
            ":oldPrice" => $this->request->get("oldPrice")[$i],
            ":discountPercent" => $this->request->get("discountPercent")[$i],
            ":discountPotongan" => $this->request->get("discountPotongan")[$i]
        ], $this->request->get("commodityId")[$i], $this->request->get("secondaryIdItem")[$i],  $this->request->get("amount")[$i], $this->request->get("shopId"));

        // Make loop
        if ($i < sizeof($this->request->get("commodityId")) - 1) return $this->_insertItem($i + 1);

        return $this->salesModel->getSalesItem([":salesId" => $this->request->get("salesId")]);
    }

    /**
     * Get User Sales Item
     *
     * @param $sales
     * @param int $i
     * @return mixed
     */
    private function getUserSalesItem($sales, $i = 0)
    {

        // Get sales Item
        $sales[$i]['item'] = ( array )$this->salesModel->getSalesItem([
            ":salesId" => $sales[$i]['salesId']
        ]);

        if ($i < sizeof($sales) - 1) return $this->getUserSalesItem($sales, $i + 1);

        return $sales;
    }

    /**
     * Get Shop Sales Item
     *
     * @param $sales
     * @param int $i
     * @return mixed
     */
    private function getShopSalesItem($sales, $i = 0)
    {

        // Get sales Item
        $sales[$i]['item'] = ( array )$this->salesModel->getSalesItem([
            ":salesId" => $sales[$i]['salesId']
        ]);

        if ($i < sizeof($sales) - 1) return $this->getUserSalesItem($sales, $i + 1);

        return $sales;
    }

    /**
     * Get Detail Sales
     *
     * @method GET
     * @param $request
     * @param $response
     * @param $args
     *
     * @return false|string
     * @link sales/{salesId}
     *
     */
    public function detail($request, $response, $args)
    {

        // Set Params
        $params = [":salesId" => $args['salesId']];

        // Get Sales data
        $sales = $this->salesModel->getSales($params);

        if (!isset($sales->salesId)) return $this->response->publish(null, "Sales not found", self::NOT_FOUND);

        // Get Sales Item
        $sales->item = ( array )$this->salesModel->getSalesItem($params);

        return $this->response->publish($sales, "Success get sales", self::SUCCESS);
    }

    /**
     * Get User Sales
     *
     * @method POST
     * @param $request
     * @param $response
     * @param $args
     *
     * @return false|string
     * @link sales/user
     *
     */
    public function getUserSales($request, $response, $args)
    {

        $this->request->parse($request);

        // Get Shop Detail
        $shops = (array)$this->shopModel->getShopsByUser();

        for ($i = 0; $i < sizeof($shops); $i++) {

            // getting user Sales
            $sales = (array)$this->salesModel->getShopSales([
                ":shopId" => $shops[$i]->shopId]);

            if (sizeof($sales) > 0) {
                $sales = $this->getShopSalesItem($sales);
            }

            $shops[$i]->sales = $sales;
        }


        return $this->response->publish($shops, "Success get user sales", self::SUCCESS);
    }


    /**
     * Get Shop Sales
     *
     * @method POST
     * @param $request
     * @param $response
     * @param $args
     *
     * @return false|string
     * @link sales/user
     *
     */
    public function getShopSales($request, $response, $args)
    {

        $this->request->parse($request);
        $sales = [];
        if ($this->request->get("offset") != null) {
            // getting  Sales by Shop Id, limit, and offset
            $sales = ( array )$this->salesModel->getShopSalesLimit([
                ":shopId" => $this->request->get("shopId")
                ], (int) $this->request->get("offset"), (int) $this->request->get("limit"));
        } else {
            // getting  Sales by Shop Id
            $sales = ( array )$this->salesModel->getShopSales([
                ":shopId" => $this->request->get("shopId")]);
        }



        if (sizeof($sales) == 0) return $this->response->publish(null, "Sales Not Found", self::NOT_FOUND);

        $sales = $this->getShopSalesItem($sales);

        return $this->response->publish($sales, "Success get user sales", self::SUCCESS);
    }

    /**
     * Add Sales
     *
     * @method POST
     * @param $request
     * @param $response
     * @param $args
     *
     * @return false|string
     * @link sales/add
     *
     */
    public function add($request, $response, $args)
    {

        $this->request->parse($request);

        $sales = $this->_insertSales();

        return $this->response->publish($sales, "Success Add Sales", self::SUCCESS);
    }


    /**
     * Editting Sales data
     *
     * @method POST
     * @param $request
     * @param $response
     * @param $args
     *
     * @return false|string
     * @link sales/edit
     *
     */
    public function edit($request, $response, $args)
    {

        $this->request->parse($request);

        // Getting sales data
        $sales = $this->salesModel->getSales([":salesId" => $this->request->get("salesId")]);

        // Removing old item
        $this->salesModel->removeSalesItem([":salesId" => $this->request->get("salesId")]);

        // inserting new item
        $sales->item = (array)$this->_insertItem();

        return $this->response->publish($sales, "Success Edit Sales", self::SUCCESS);
    }

    /**
     * Removing Sales data
     *
     * @method POST
     * @param $request
     * @param $response
     * @param $args
     *
     * @return false|string
     * @link sales/remove
     *
     */
    public function remove($request, $response, $args)
    {

        $this->request->parse($request);

        // Removing sales
        $this->salesModel->removeSalesData([":salesId" => $this->request->get("salesId")]);

        return $this->response->publish(null, "Success Delete Sales", self::SUCCESS);
    }

    /**
     * Removing Sales data
     *
     * @method POST
     * @param $request
     * @param $response
     * @param $args
     *
     * @return false|string
     * @link sales/remove
     *
     */
    public function removeItem($request, $response, $args)
    {

        $this->request->parse($request);

        // Removing sales
        $this->salesModel->removeSalesItemByCommodityId([":salesId" => $this->request->get("salesId"),
            ":commodityId" => $this->request->get("commodityId")
            ]);

        return $this->response->publish(null, "Success Delete Item Sales", self::SUCCESS);
    }

    /**
     * Buy Product
     *
     * @method POST
     * @param $request
     * @param $response
     * @param $args
     *
     * @return false|string
     * @link sales/buy
     *
     */
    public function buy($request, $response, $args)
    {

        $this->request->parse($request);

        // Check Sales Id
        $items = $this->request->get("salesId") != "" ?
            ( array )$this->salesModel->getSalesItem([":salesId" => $this->request->get("salesId")]) :
            ( array )$this->_parse_item();

        $avaibility = $this->_checkCommodityAvaibility($items);

        // Check commodity avaibility
        if (!$avaibility['status']) return $this->response->publish(null, "insufficient stock", self::ERROR);

        // Reducing Stock
        $this->_reduceCommodityStock($items, $avaibility['commodity']);

        // Inserting or Updating sales
        if ($this->request->get("salesId") != "") {

            $this->salesModel->updateStatus([
                ":status" => self::BUY_STATUS,
                ":salesId" => $this->request->get("salesId")
            ]);

            $sales = $this->salesModel->getSales([":salesId" => $this->request->get("salesId")]);

            // Get Sales Item
            $sales->item = ( array )$this->salesModel->getSalesItem([":salesId" => $this->request->get("salesId")]);

        } else {

            // Inserting sales
            $sales = $this->_insertSales();
        }

        return $this->response->publish($sales, "Success Buy Product", self::SUCCESS);
    }

    /**
     * @param array $item
     * @param int $i
     * @return array
     */
    private function _parse_item($item = [], $i = 0)
    {

        // Pushing array
        array_push($item, [
            "commodityId" => $this->request->get("commodityId")[$i],
            "amount" => $this->request->get("amount")[$i]
        ]);

        // Make Loop
        if ($i < sizeof($this->request->get("commodityId")) - 1) return $this->_parse_item($item, $i + 1);

        // Return
        return $item;
    }

    /**
     * @param $items
     * @param array $commodities
     * @param int $i
     * @return array
     */
    private function _checkCommodityAvaibility($items, $commodities = [], $i = 0)
    {

        $item = (array)$items[$i];

        // Get Commodity
        $commodity = $this->commodityModel->getDetail([":commodityId" => $item['commodityId']]);

        // Check avaibility
        if ($commodity->stock['display'] < $item['amount']) return ["status" => false];

        // pushing commodity
        array_push($commodities, $commodity);

        // Make Loop
        if ($i < sizeof($items) - 1) return $this->_checkCommodityAvaibility($items, $commodities, $i + 1);

        return [
            "status" => true,
            "commodity" => $commodities
        ];
    }

    /**
     * @param $items
     * @param $commodities
     * @param int $i
     */
    private function _reduceCommodityStock($items, $commodities, $i = 0)
    {

        // getting data
        $item = (array)$items[$i];
        $commodity = (array)$commodities[$i];

        $commodityId = $commodity['commodityId'];
        $newStock = $commodity['stock']['display'] - $item['amount'];

        // Update Commodity Stock
        $this->commodityModel->updateStock([
            ":display" => $newStock,
            ":commodityId" => $commodityId
        ]);

        // Make Loop
        if ($i < sizeof($items) - 1) $this->_reduceCommodityStock($items, $commodities, $i + 1);
    }

    /**
     * @return array
     */
    private function _insertSales()
    {
        // Insert Sales
        $salesId = $this->salesModel->insertSales([
            ":userId" => $this->request->get("userId"),
            ":secondaryId" => $this->request->get("secondaryId"),
            ":operatorId" => $this->request->get("operatorId"),
            ":shopId" => $this->request->get("shopId"),
            ":customerPaid" => $this->request->get("customerPaid"),
            ":remainingPayment" => $this->request->get("remainingPayment"),
            ":discountPercent" => $this->request->get("discountPercentTrans"),
            ":discountPotongan" => $this->request->get("discountPotonganTrans"),
            ":total" => $this->request->get("total"),
            ":totalAfterDiscount" => $this->request->get("totalAfterDiscount"),
            ":date" => date("Y-m-d"),
            ":time" => date("H:i:s"),
            ":status" => self::CART_STATUS
        ], $this->request->get("secondaryId"));


        // set Sales Id
        $this->request->set("salesId", $salesId);

        // Inserting item
        $item = $this->_insertItem();

        // Get Sales Data
        return [
            "salesId" => $this->request->get("salesId"),
            "userId" => $this->request->get("userId"),
            "operatorId" => $this->request->get("operatorId"),
            "shopId" => $this->request->get("shopId"),
            "secondaryId" => $this->request->get("secondaryId"),
            "remainingPayment" => $this->request->get("remainingPayment"),
            "customerPaid" => $this->request->get("customerPaid"),
            "discountPercent" => $this->request->get("discountPercentTrans"),
            "discountPotongan" => $this->request->get("discountPotonganTrans"),
            "total" => $this->request->get("total"),
            "totalAfterDiscount" => $this->request->get("totalAfterDiscount"),
            "date" => date("Y-m-d"),
            "time" => date("H:i:s"),
            "status" => self::CART_STATUS,
            "item" => (array)$item
        ];
    }

    function getDataPagination($request, $response, $args) {
        // Set Params
        $params = [
            ":shopId" => $args['shopId']
        ];
        $this->request->parse($request);
        $countMax = $this->salesModel->getMaxCountByShop($params);
        return $this->response->publish((object)$countMax, "Success get Max", self::SUCCESS);
    }
}
