<?php

/**
 * Image Helper
 * @author Robet Atiq Maulana Rifqi
 * JUN 2019
 */

namespace Pos\Helpers;

use Pos\Config\Constants;

class Image implements Constants
{

    /**
     * Defining private component
     */
    private $type = ["image/png", "image/jpg", "image/jpeg"];
    private $defaultImages = [
        "path" => '',
        "width" => 500,
        "height" => 500,
        "colorTone" => "#EFEFEF"
    ];

    private $path = "../../storage/";
    private $uri = "storage/";

    /**
     *
     * Uploading Image
     *
     * @param $images
     * @param $path
     * @param int $i
     *
     * @param array $result
     * @return mixed
     */
    public function upload($images, $path, $i = 0, $result = [])
    {

        /**
         * Getting images data
         * Checking multiple image
         */
        $image = ( array ) $images;

        $error = is_array($image['error']) ? $image['error'][$i] : $image['error'];
        $type = is_array($image['type']) ? $image['type'][$i] : $image['type'];
        $imageName = is_array($image['name']) ? $image['name'][$i] : $image['name'];
        $tmp = is_array($image['tmp_name']) ? $image['tmp_name'][$i] : $image['tmp_name'];

        /** Returning error when error is found */
        if ($error) return ( object ) [ "code" => self::ERROR, "message" => "Error uploading image"];

        /** Checking image type */
        if (!in_array($type, $this->type)) return ( object ) [ "code" => self::ERROR, "message" => "Image type not supported"];

        /**
         * Generating image name
         * Generating image dir
         */
        $exploder = explode(".", $imageName);
        $name = hash( "sha256", date ( "YmdHis" ) . $exploder[0] ) . "." . $exploder[1];

        /**
         * Checking image path
         * if path doesn't exist it will create that path
         */
        if ( !file_exists( $this->path )) mkdir($this->path);
        if ( !file_exists($this->path . $path) ) mkdir( $this->path . $path );

        /** Uploading image */
        move_uploaded_file($tmp, $this->path . $path . "/" . $name );

        /** Checking single data */
        if ( !is_array($image['name'])) return ( object ) [ "code" => self::SUCCESS, "path" => $name];

        /** Pushing image name to result */
        $result[$i] = $name;

        /** Make Loop */
        if ( $i < sizeof($image['name']) - 1) return $this->upload($image, $path, $i + 1, $result);

        /** Returning result json */
        return ( object ) [ "code" => self::SUCCESS, "path" => json_encode($result)];
    }

    /**
     * Get Uploaded Image
     *
     * @param $path
     * @param $name
     * @param $isDetail
     * @return mixed
     */
    public function get($path, $name, $isDetail = false)
    {

        // Decoding Image as JSON Objects
        $images = json_decode($name) != null ? json_decode($name) : $name;

        // Arraying image images
        return is_array($images) ? $this->_getFullPath($path, $images, $isDetail) : $this->_getFullPath($path, [$images], $isDetail)[0];
    }

    /**
     * Get Full Image Path
     * @param $path
     * @param $name
     * @param bool $isDetail
     * @param int $i
     * @return mixed
     */
    private function _getFullPath($path, $name, $isDetail = false, $i = 0)
    {
        $dir = $this->path . $path . "/" . $name[$i];

        // Check Image Exists
        if ($name[$i] != "" && file_exists($dir)) {

            // Get Image Dimensions
            list($width, $height) = getimagesize($dir);

            // Set Object
            $name[$i] = $isDetail ? [
                "path" => $this->uri . $path . "/" . $name[$i],
                "width" => $width,
                "height" => $height,
                "colorTone" => "#EFEFEF"
            ] : $this->uri . $path . "/" . $name[$i];

        } else {

            if ( $isDetail ) {
                $name[$i] = ( object )$this->defaultImages;
                $name[$i]->path = $this->uri . $path . "/al.png";
            } else $name[$i] = $this->uri . $path . "/al.png";
        }

        // Make Loop
        if ($i < sizeof($name) - 1) return $name = $this->_getFullPath($path, $name, $isDetail, $i + 1);

        // Return
        return $name;
    }
}